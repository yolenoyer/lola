//! Implements all the native units and quantities (close to the [International System of Units]).
//!
//! The quantities actually handled are:
//!
//!  * length
//!  * area
//!  * volume
//!  * mass
//!  * time
//!  * speed
//!  * force
//!  * energy
//!  * pression
//!  * power
//!  * electric current
//!  * electric charge
//!  * electric potential
//!  * electric resistance
//!  * electric conductance
//!  * electric inductance
//!  * electric capacitance
//!  * magnetic induction
//!  * magnetic flux
//!  * frequence
//!  * amount
//!  * temperature
//!  * angle
//!  * currency
//!  * computer sizes
//!
//! [International System of Units]: https://en.wikipedia.org/wiki/International_System_of_Units

use crate::context::Container;
use crate::{unit, no_unit};

/// Creates all the native units in a container.
pub fn create_units(container: &mut Container) {
    macro_rules! new_quantity {
        (__main $name:expr, $( $units:tt )*) => {{
            let q = container.add_quantity($name).unwrap().clone();
            new_quantity!(__units q, $($units)*);
            q
        }};

        (__units $q:expr, $( ( $($unit:tt)* ) ),* $(,)?) => {
            $( new_quantity!(__unit $q, $($unit)*) );*
        };

        (__unit $q:expr, $name:expr, $weight:expr) => {
            container.add_base_unit($name, float_parsed!($weight), &$q, None)
        };

        (__unit $q:expr, $name:expr, [parsed] $($weight:tt)*) => {
            container.add_base_unit($name, float_parsed!($($weight)*), &$q, None)
        };

        (__unit $q:expr, $name:expr, $weight:expr, $equivalence:expr) => {
            container.add_base_unit($name, float_parsed!($weight), &$q, Some($equivalence))
        };

        ( $( $tt:tt )* ) => {
            new_quantity!(__main $($tt)*)
        };
    }

    macro_rules! weight {
        ("Y")  => { float_parsed!(1_000_000_000_000_000_000_000_000) };
        ("Z")  => { float_parsed!(1_000_000_000_000_000_000_000) };
        ("E")  => { float_parsed!(1_000_000_000_000_000_000) };
        ("P")  => { float_parsed!(1_000_000_000_000_000) };
        ("T")  => { float_parsed!(1_000_000_000_000) };
        ("G")  => { float_parsed!(1_000_000_000) };
        ("M")  => { float_parsed!(1_000_000) };
        ("k")  => { float_parsed!(1_000) };
        ("h")  => { float_parsed!(100) };
        ("da") => { float_parsed!(10) };
        ("d")  => { float_parsed!(0.1) };
        ("c")  => { float_parsed!(0.01) };
        ("m")  => { float_parsed!(0.001) };
        ("µ")  => { float_parsed!(0.000_001) };
        ("u")  => { float_parsed!(0.000_001) };
        ("n")  => { float_parsed!(0.000_000_001) };
        ("p")  => { float_parsed!(0.000_000_000_001) };
        ("f")  => { float_parsed!(0.000_000_000_000_001) };
        ("a")  => { float_parsed!(0.000_000_000_000_000_001) };
        ("z")  => { float_parsed!(0.000_000_000_000_000_000_001) };
        ("y")  => { float_parsed!(0.000_000_000_000_000_000_000_001) };
    }

    macro_rules! add_prefixed_unit {
        (__add_one $q:expr, $bu:expr, $prefix:tt) => {{
            let unit_name = concat!($prefix, $bu);
            container.add_base_unit(unit_name, weight!($prefix), &$q, None);
        }};
        (__add_one_with_equiv $q:expr, $bu:expr, $equiv:expr, $prefix:tt) => {{
            let unit_name = concat!($prefix, $bu);
            container.add_base_unit(unit_name, weight!($prefix), &$q, Some($equiv));
        }};
        ( $q:expr, $bu:expr, [ $( $prefixes:tt ),* ] ) => {
            $(
                add_prefixed_unit!(__add_one $q, $bu, $prefixes);
            )*
        };
        ( $q:expr, $bu:expr, $equiv:expr, [ $( $prefixes:tt ),* ] ) => {
            $(
                add_prefixed_unit!(__add_one_with_equiv $q, $bu, $equiv, $prefixes);
            )*
        };
    }

    macro_rules! add_all_prefixed_units {
        ( $q:expr, $bu:expr ) => {
            add_prefixed_unit!($q, $bu, [
                "Y", "Z", "E", "P", "T", "G", "M", "k", "h", "da",
                "d", "c", "m", "µ", "u", "n", "p", "f", "a", "z", "y"
            ]);
        };
        ( $q:expr, $bu:expr, $equiv:expr ) => {
            add_prefixed_unit!($q, $bu, $equiv, [
                "Y", "Z", "E", "P", "T", "G", "M", "k", "h", "da",
                "d", "c", "m", "µ", "u", "n", "p", "f", "a", "z", "y"
            ]);
        };
    }

    macro_rules! unit {
        ($( $name:tt : $exp:expr ),* ) => {
            container.create_unit(&[ $(
                ( stringify!($name), float_parsed!($exp) )
            ),* ]).unwrap()
        };
    }

    let q = new_quantity!("length",
        ( "m",                       1      ),
        ( "ft",                      0.3048 ),
        ( "in",                      0.0254 ),
        ( "mi",                  1_609.344  ),
        ( "au",        149_597_870_700      ),
        ( "ly",  9_460_730_472_580_800      ),
        ( "pc", 30_856_775_810_000_000      ),
    );
    add_all_prefixed_units!(q, "m");

    new_quantity!("area",
        ( "ha", 10_000, unit!(m:2) ),
    );

    new_quantity!("volume",
        ( "l",  0.001, unit!(m:3) ),
        // TODO: find a more elegant solution for units like liter which have a base weight
        ( "Yl", 1_000_000_000_000_000_000_000, unit!(m:3) ),
        ( "Zl", 1_000_000_000_000_000_000, unit!(m:3) ),
        ( "El", 1_000_000_000_000_000, unit!(m:3) ),
        ( "Pl", 1_000_000_000_000, unit!(m:3) ),
        ( "Tl", 1_000_000_000, unit!(m:3) ),
        ( "Gl", 1_000_000, unit!(m:3) ),
        ( "Ml", 1_000, unit!(m:3) ),
        ( "kl", 1, unit!(m:3) ),
        ( "hl", 0.1, unit!(m:3) ),
        ( "dal", 0.01, unit!(m:3) ),
        ( "dl", 0.000_1, unit!(m:3) ),
        ( "cl", 0.000_01, unit!(m:3) ),
        ( "ml", 0.000_001, unit!(m:3) ),
        ( "µl", 0.000_000_001, unit!(m:3) ),
        ( "ul", 0.000_000_001, unit!(m:3) ),
        ( "nl", 0.000_000_000_001, unit!(m:3) ),
        ( "pl", 0.000_000_000_000_001, unit!(m:3) ),
        ( "fl", 0.000_000_000_000_000_001, unit!(m:3) ),
        ( "al", 0.000_000_000_000_000_000_001, unit!(m:3) ),
        ( "zl", 0.000_000_000_000_000_000_000_001, unit!(m:3) ),
        ( "yl", 0.000_000_000_000_000_000_000_000_001, unit!(m:3) ),
    );

    let q = new_quantity!("mass",
        ( "g",         1 ),
        ( "t", 1_000_000 ),
    );
    add_all_prefixed_units!(q, "g");

    let q = new_quantity!("time",
        ( "s",          1 ),
        ( "min",       60 ),
        ( "h",       3600 ),
        ( "d",      86400 ),
        ( "y",   31557600 ),
    );
    add_all_prefixed_units!(q, "s");

    new_quantity!("speed",
        ( "nd", 1.852, unit!(km:1, h:-1) ),
        ( "c",  299_792_458, unit!(m:1, s:-1) ),
    );

    let q = new_quantity!("force",
        ( "N",  1, unit!(kg:1, m:1, s:-2) ),
    );
    add_all_prefixed_units!(q, "N", unit!(kg:1, m:1, s:-2));

    let q = new_quantity!("energy",
        ( "J",    1,      unit!(kg:1, m:2, s:-2) ),
        ( "cal",  4.1855, unit!(kg:1, m:2, s:-2) ),
        ( "kcal", 4185.5, unit!(kg:1, m:2, s:-2) ),
        ( "meV",  0.000_000_000_000_000_000_000_160_217_663_4, unit!(kg:1, m:2, s:-2) ),
        ( "eV",   0.000_000_000_000_000_000_160_217_663_4,     unit!(kg:1, m:2, s:-2) ),
        ( "keV",  0.000_000_000_000_000_160_217_663_4,         unit!(kg:1, m:2, s:-2) ),
        ( "MeV",  0.000_000_000_000_160_217_663_4,             unit!(kg:1, m:2, s:-2) ),
        ( "GeV",  0.000_000_000_160_217_663_4,                 unit!(kg:1, m:2, s:-2) ),
        ( "TeV",  0.000_000_160_217_663_4,                     unit!(kg:1, m:2, s:-2) ),
    );
    add_all_prefixed_units!(q, "J", unit!(kg:1, m:2, s:-2));

    let q = new_quantity!("pression",
        ( "Pa",        1, unit!(kg:1, m:-1, s:-2) ),
        ( "bar", 100_000, unit!(kg:1, m:-1, s:-2) ),
        ( "mbar",    100, unit!(kg:1, m:-1, s:-2) ),
    );
    add_all_prefixed_units!(q, "Pa", unit!(kg:1, m:-1, s:-2));

    let q = new_quantity!("power",
        ( "W",    1, unit!(kg:1, m:2, s:-3) ),
        ( "hp", 746, unit!(kg:1, m:2, s:-3) ), // Horse power; the given number is indicative
    );
    add_all_prefixed_units!(q, "W", unit!(kg:1, m:2, s:-3));

    let q = new_quantity!("electric-current",
        ( "A", 1 ),
    );
    add_all_prefixed_units!(q, "A");

    let q = new_quantity!("electric-charge",
        ( "C", 1, unit!(A:1, s:1) ),
    );
    add_all_prefixed_units!(q, "C", unit!(A:1, s:1));

    let q = new_quantity!("electric-potential",
        ( "V",  1, unit!(kg:1, m:2, s:-3, A:-1) ),
    );
    add_all_prefixed_units!(q, "V", unit!(kg:1, m:2, s:-3, A:-1));

    let q = new_quantity!("electric-resistance",
        ( "Ω",   1, unit!(kg:1, m:2, s:-3, A:-2) ),
        ( "ohm", 1, unit!(kg:1, m:2, s:-3, A:-2) ),
    );
    add_all_prefixed_units!(q, "Ω", unit!(kg:1, m:2, s:-3, A:-2));
    add_all_prefixed_units!(q, "ohm", unit!(kg:1, m:2, s:-3, A:-2));

    let q = new_quantity!("electric-conductance",
        ( "S",  1, unit!(ohm:-1) ),
    );
    add_all_prefixed_units!(q, "S", unit!(ohm:-1));

    let q = new_quantity!("electric-inductance",
        ( "H", 1, unit!(kg:1, m:2, s:-2, A:-2) ),
    );
    add_all_prefixed_units!(q, "H", unit!(kg:1, m:2, s:-2, A:-2));

    let q = new_quantity!("electric-capacitance",
        ( "F",  1, unit!(s:4, A:2, m:-2, kg:-1) ),
    );
    add_all_prefixed_units!(q, "F", unit!(s:4, A:2, m:-2, kg:-1));

    let q = new_quantity!("magnetic-induction",
        ( "T",  1,       unit!(kg:1, s:-2, A:-1) ),
        ( "G",  0.000_1, unit!(kg:1, s:-2, A:-1) ),
    );
    add_all_prefixed_units!(q, "T", unit!(kg:1, s:-2, A:-1));

    new_quantity!("magnetic-flux",
        ( "Mx", 0.000_000_01, unit!(kg:1, m:2, s:-2, A:-1) ),
        ( "Wb", 1,            unit!(kg:1, m:2, s:-2, A:-1) ),
    );

    new_quantity!("frequence",
        ( "THz", 1_000_000_000_000, unit!(s:-1) ),
        ( "GHz",     1_000_000_000, unit!(s:-1) ),
        ( "MHz",         1_000_000, unit!(s:-1) ),
        ( "kHz",             1_000, unit!(s:-1) ),
        ( "Hz",                  1, unit!(s:-1) ),
        ( "Bq",                  1, unit!(s:-1) ),
        ( "GBq",     1_000_000_000, unit!(s:-1) ),
    );

    new_quantity!("amount",
        ( "mol", 602_214_076_000_000_000_000_000, no_unit!() ),
    );

    // The temperature units below represent only the ratio between the different units, not the
    // "offset". So, converting 0°C to kelvins will not give +273.15°K, but 0°K.
    let q = new_quantity!("temperature",
        ( "K",  1 ),
        ( "°C", 1 ),
        ( "°F", 0.555555555555555555555555555555555555 ),
    );
    add_all_prefixed_units!(q, "K");

    new_quantity!("angle",
        ( "rad", 1                          ),
        ( "°",   0.017453292519943295769236 ),
    );

    new_quantity!("currency",
        ( "€", 1 ),
        ( "$", 0.89 ),
    );

    let q = new_quantity!("computer-size",
        ( "B", 1 ),

        ( "KiB", 1_024                 ),
        ( "kiB", 1_024                 ),
        ( "MiB", 1_048_576             ),
        ( "GiB", 1_073_741_824         ),
        ( "TiB", 1_099_511_627_776     ),
        ( "PiB", 1_125_899_906_842_624 ),

        ( "KB", 1000 ),
    );
    add_prefixed_unit!(q, "B", [
        "Y", "Z", "E", "P", "T", "G", "M", "k"
    ]);
}

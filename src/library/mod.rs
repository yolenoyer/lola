//! Implements all the native units, quantities, functions and constants.

use crate::context::{Container, Scope, ContextRef};

mod functions;
mod variables;
mod units;

use functions::*;
use variables::*;
use self::units::*;

/// Prefills the given context with standard quantities, units, variables, functions.
///
/// See [`ContextRef`] for an example of use.
///
/// [`ContextRef`]: crate::context::ContextRef
pub fn prefill_context(cxt: &mut ContextRef) {
    create_units(cxt.container);
    create_variables(cxt);
    create_math_functions(cxt.scope);
    create_temperature_functions(cxt.scope);
    create_integer_functions(cxt.scope);
    create_string_functions(cxt.scope);
    create_dict_functions(cxt.scope);
    create_misc_functions(cxt.scope);
    create_type_functions(cxt.scope);
    create_config_function(cxt.scope);
    #[cfg(feature = "import_function")]
    create_import_function(cxt.scope);
    #[cfg(feature = "sys_functions")]
    create_fs_functions(cxt.scope);
    #[cfg(test)]
    create_debug_functions(cxt.scope);
}

/// Creates a new context with standard quantities, units, variables, functions.
///
/// See [`ContextRef`] for an example of use.
///
/// [`ContextRef`]: crate::context::ContextRef
pub fn create_prefilled_context() -> (Container, Scope) {
    let mut container = Container::default();
    let mut scope = Scope::new();
    let mut cxt = ContextRef::new(&mut container, &mut scope);
    prefill_context(&mut cxt);
    (container, scope)
}

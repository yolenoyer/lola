//! Implements native constants.
//!
//! * `pi`
//! * `G` (gravitational constant)
//! * `earth_gravity`

use rug::float::Constant;

use crate::context::{ContextRef};
use crate::{number, unit};

/// Creates all the native constants in the scope of the given context.
pub fn create_variables(cxt: &mut ContextRef) {
    macro_rules! define {
        ( __one_var $id:tt : [constant] $constant_name:ident ) => {
            cxt.scope.define_var(
                stringify!($id).to_string(),
                number!(cxt.container, [expr] (float!(Constant::$constant_name)))
            ).unwrap()
        };
        ( __one_var $id:tt : $($value:tt)* ) => {
            cxt.scope.define_var(
                stringify!($id).to_string(),
                number!(cxt.container, $($value)*)
            ).unwrap()
        };
        ( $( ( $($one_var_definition:tt)* ) ),* $(,)? ) => {
            {
                $(
                    define!(__one_var $($one_var_definition)*)
                );*
            }
        };
    }

    define!(
        ( pi: [constant] Pi ),
        ( G: 6.67430E-11 m:3, kg:-1, s:-2 ),
        ( earth_gravity: 9.80665 m/s2  ),
    );
}

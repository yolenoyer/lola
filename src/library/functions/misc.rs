//! Implements miscellaneous runtime functions.
//!
//!  * `print()`
//!  * `is_defined()`
//!  * `sleep()`
//!  * `len()`

use std::thread;

use crate::context::Scope;
use crate::value::{
    Value,
    Format,
};
use crate::eval::{
    arg_list_checker as arg_list,
    arg_checker as arg,
    func_macros::*,
};

/// Creates miscellaneous functions in the given scope.
pub fn create_misc_functions(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    // Print values to stdout:
    add_func!(
        print,
        body!(|args, cxt| {
            if !args.is_empty() {
                let output = args.iter()
                    .map(|v| v.format(cxt.container.format_config(), false))
                    .collect::<Vec<_>>()
                    .join(" ");
                let mut writer = cxt.container.writer().borrow_mut();
                writeln!(writer, "{}", output).unwrap();
            }
            Ok(void!())
        })
    );

    // Check if a variable is defined:
    add_func!(
        is_defined,
        args![
            name: arg::is_string
        ],
        body!(|args, cxt| {
            Ok(boolean!({
                let var_name = args[0].unwrap_str().val_ref();
                cxt.scope.has_var(var_name)
            }))
        })
    );

    // Sleep during the given time:
    add_func!(
        sleep,
        args![
            time: arg::has_quantity(compound_quantity!{ time: 1 })
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, cxt| {
            // The arguments have already been checked, then we know that there is exactly one
            // argument of type number(time).
            let time = &args[0];
            let time_in_seconds = time.convert_to_unit(&unit!(cxt.container, s)).unwrap();
            let time_in_seconds = time_in_seconds.unwrap_num();
            let seconds = time_in_seconds.num().to_f32();
            thread::sleep(std::time::Duration::from_secs_f32(seconds)); // TODO: can panic
            Ok(void!())
        })
    );

    // Function "len()", returns the length of a string or an array
    add_func!(
        len,
        args![
            s: arg::or(arg::is_string, arg::is_array)
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| {
            match &args[0] {
                Value::Str(s) => Ok(int!(s.utf8_len())),
                Value::Array(a) => Ok(int!(a.len())),
                _ => unreachable!(),
            }
        })
    );
}

#[cfg(test)]
/// Creates some debug functions in the given scope.
pub fn create_debug_functions(scope: &mut Scope) {
    use crate::eval::func_macros::*;

    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    add_func!(
        nop,
        body!(|_, _| Ok(void!()) )
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn function_len() {
        with_test_context!(cxt, {
            let result = eval!(cxt, r#"  len("hello")  "#);
            assert_eq!(result, int!(5));
            let result = eval!(cxt, "len([1,2,3])");
            assert_eq!(result, int!(3));
        })
    }

    #[test]
    fn function_is_defined() {
        with_test_context!(cxt, {
            eval!(cxt, "let v = 45");
            assert_eq!(eval!(cxt, r#" is_defined("v") "#), v_true!());
            assert_eq!(eval!(cxt, r#" is_defined("zzz") "#), v_false!());
        })
    }
}

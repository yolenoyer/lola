//! Implements runtime string functions.
//!
//! * `str_part()`

use crate::context::Scope;

use crate::eval::{
    ExprError,
    arg_list_checker as arg_list,
    arg_checker as arg,
    func_macros::*,
};

/// Creates functions dealing with strings in a container.
pub fn create_string_functions(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    // Returns a part of a string:
    add_func!(
        str_part,
        args![
            s:     arg::is_string,
            start: arg::is_int,
            len:   arg::is_int,
        ],
        arg_list_checker!(arg_list::args_count_range(2..=3)),
        body!(|args, _| -> Result<_, ExprError> {
            macro_rules! extract_usize {
                ($value:expr) => { $value.extract_int::<usize>().map_err(ExprError::Operation)? };
            }

            let s = args[0].unwrap_str().val_ref();
            let s_len = s.len();

            let start = extract_usize!(args[1]);
            if start > s_len {
                return Err(err!(Expr:Operation:ValueOutOfBounds));
            }

            let range = match args.get(2) {
                None => {
                    let r: Result<_, ExprError> = Ok(start..s_len);
                    r
                },
                Some(len) => {
                    let len = extract_usize!(len);
                    let end = start + len; // TODO: theorically, this could panic
                    if end > s_len {
                        return Err(err!(Expr:Operation:ValueOutOfBounds));
                    }
                    Ok(start..end)
                },
            }?;

            let value = string!(&s[range]);
            Ok(value)
        })
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn function_str_part() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, r#" str_part("foobar", 0, 1) "#), string!("f"));
            assert_eq!(eval!(cxt, r#" str_part("foobar", 0, 6) "#), string!("foobar"));
            assert_eq!(eval!(cxt, r#" str_part("foobar", 1, 2) "#), string!("oo"));
            assert_eq!(eval!(cxt, r#" str_part("foobar", 0) "#), string!("foobar"));
            assert_eq!(eval!(cxt, r#" str_part("foobar", 3) "#), string!("bar"));
            assert_eq!(
                eval_err!(cxt, r#" str_part("foo", -1)  "#),
                err!(Expr:Operation:ValueOutOfBounds)
            );
            assert_eq!(
                eval_err!(cxt, r#" str_part("foo", 1, 3)  "#),
                err!(Expr:Operation:ValueOutOfBounds)
            );
        })
    }
}

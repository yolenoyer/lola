//! Implements the RT function `config()`.
//!
//!  * `config()`

use std::fmt;

use crate::context::Scope;
use crate::value::{StrOperand, Value};
use crate::eval::{ExprError, func_macros::*};

/// Creates the `config()` function in the given scope.
pub fn create_config_function(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    // Configures the container:
    add_func!(
        config,
        body!(|args, cxt| {
            let mut try_config = |opt_key: Option<&StrOperand>, value: &Value| -> Result<(), ExprError> {
                let key = match opt_key {
                    Some(key) => key.val_ref(),
                    None => {
                        let message = ConfigArgsError::StringExpected(0).to_string();
                        return Err(err!(Expr:Call:ArgList:Custom(message)));
                    }
                };
                cxt.container.configure(key, value.clone())
                    .map_err(|e| err!(Expr:Call:ArgList:Custom(e.to_string())))?;
                Ok(())
            };

            if args.len() == 2 {
                try_config(args[0].get_str(), &args[1])?;
            } else if let Some(Some(dict)) = args.get(0).map(|arg| arg.get_dict()) {
                let dict = dict.inner().borrow();
                for (key, value) in dict.iter() {
                    try_config(key.get_str(), value)?;
                }
            } else {
                return Err(err!(Expr:Call:ArgList:MismatchTypes));
            }

            Ok(void!())
        })
    );
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum ConfigArgsError {
    StringExpected(usize),
}

impl fmt::Display for ConfigArgsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::StringExpected(arg) => write!(f, "Arg {}: string expected", arg),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::util::test_common::*;

    #[test]
    fn test_rt_func_config() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                config({
                    "digits": 4,
                    "underscores": false,
                    "ellipsis": "...",
                });
            "#);
            assert_eq!(
                cxt.container.format_config(),
                &FormatConfig {
                    significant_digits: Some(4),
                    underscores: None,
                    ellipsis: "...".to_string(),
                    multi_line: true,
                    max_width: 60,
                    indent: 2,
                }
            );
        })
    }

    #[test]
    fn test_rt_func_config_digits() {
        with_test_context!(cxt, {
            eval!(cxt, r#"config({ underscores: false, ellipsis: "..." })"#);

            let s = eval!(cxt, r#"config("digits", 6); str(1.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1.23456...");

            let s = eval!(cxt, r#"config("digits", 2); str(1.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1.2...");

            let s = eval!(cxt, r#"config("digits", 1); str(1.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1...");

            eval_err!(cxt, r#"config("digits", 0)"#);
        })
    }

    #[test]
    fn test_rt_func_config_underscores() {
        with_test_context!(cxt, {
            eval!(cxt, r#"config({ underscores: false, digits: false })"#);

            let s = eval!(cxt, "str(1.234567)");
            assert_eq!(s.unwrap_str().val_ref(), "1.234567");

            let s = eval!(cxt, r#"config("underscores", 3); str(1.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1.234_567");

            let s = eval!(cxt, r#"config("underscores", 3); str(34567890.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "34_567_890.234_567");

            let s = eval!(cxt, r#"config("underscores", 1); str(12.34)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1_2.3_4");

            eval_err!(cxt, r#"config("underscores", 0)"#);
        })
    }

    #[test]
    fn test_rt_func_config_ellipsis() {
        with_test_context!(cxt, {
            eval!(cxt, r#"config({ digits: 4, ellipsis: "..." })"#);

            let s = eval!(cxt, r#"str(1.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1.234...");

            let s = eval!(cxt, r#"config("ellipsis", "===="); str(1.234567)"#);
            assert_eq!(s.unwrap_str().val_ref(), "1.234====");
        })
    }
}

//! Implements runtime math functions.
//!
//! Math functions are mostly bindings to the [rug] crate math functions.
//!
//! [rug]: https://docs.rs/rug/1.9.0/rug/

use std::convert::TryFrom;

use rug::Float;

use crate::context::{Container, Scope};
use crate::value::{IntoValue, NumOperand};
use crate::num_operand;
use crate::util::numbers::{get_u32, get_i32};
use crate::eval::{
    function::FunctionArgs,
    arg_list_checker as arg_list,
    func_macros::*,
    ExprError,
};


macro_rules! _num_func {
    ( $name:ident ( $($args_names:tt),* ) : $checker:expr, $body:expr ) => {
        func!(
            $name,
            args_eval!(
                FunctionArgs::try_from(vec![
                    $(stringify!($args_names).to_string()),*
                ]).unwrap()
            ),
            arg_list_checker!(arg_list::and(arg_list::all_numbers, $checker)),
            body!(|args, cxt| {
                let numbers = args.iter()
                    .map(|v| v.get_num().unwrap())
                    .collect::<Vec<_>>();
                Ok($body(numbers, cxt.container)?.into_value())
            })
        )
    }
}

macro_rules! _add_num_func {
    ($scope:ident, $($tt:tt)*) => {
        $scope.add_named_function(_num_func!($($tt)*)).unwrap();
    };
}

macro_rules! _add_float_to_float_func {
    ($scope:ident, $name:ident ($arg1:ident) $func:ident ) => {
        _add_num_func!($scope, $name($arg1):
            arg_list::and(arg_list::check_args_count, arg_list::all_numbers),
            move |numbers: Vec<&NumOperand>, &mut _| -> Result<_, ExprError> {
                Ok(numbers[0].map(|num| {
                    num.clone().$func()
                }))
            }
        );
    }
}

macro_rules! _add_2_floats_to_float_func_no_unit {
    ($scope:ident, $name:ident ($arg1:ident, $arg2:ident) $func:ident ) => {
        _add_num_func!($scope, $name($arg1, $arg2):
            arg_list::and(arg_list::check_args_count, arg_list::all_empty_units),
            move |numbers: Vec<&NumOperand>, &mut _| -> Result<_, ExprError> {
                Ok(numbers[0].map(|num| {
                    num.clone().$func(&numbers[1].num())
                }))
            }
        );
    }
}

// TODO : return false for mismatch types
macro_rules! _add_float_to_bool_func {
    ($scope:ident, $name:ident ($arg1:ident) $func:ident ) => {
        _add_num_func!($scope, $name($arg1):
            arg_list::and(arg_list::check_args_count, arg_list::all_numbers),
            move |numbers: Vec<&NumOperand>, &mut _| -> Result<_, ExprError> {
                Ok(boolean_operand!(numbers[0].num().clone().$func()))
            }
        );
    }
}

macro_rules! _add_float_to_float_func_no_unit {
    ($scope:ident, $name:ident ($arg1:ident) $func:ident ) => {
        _add_num_func!($scope, $name($arg1):
            arg_list::and(arg_list::check_args_count, arg_list::all_empty_units),
            move |numbers: Vec<&NumOperand>, &mut _| -> Result<_, ExprError> {
                let num = numbers[0].num().clone();
                let res = num.clone().$func();
                Ok(NumOperand::from(res))
            }
        );
    }
}

macro_rules! _add_float_to_float_funcs {
    ($scope:ident, $( $name:ident ($arg1:ident) $func:ident ),* $(,)?) => {
        $( _add_float_to_float_func!($scope, $name($arg1) $func) );*
    }
}

macro_rules! _add_float_to_float_funcs_no_unit {
    ($scope:ident, $( $name:ident ($arg1:ident) $func:ident ),* $(,)?) => {
        $( _add_float_to_float_func_no_unit!($scope, $name($arg1) $func) );*
    }
}

macro_rules! _add_2_floats_to_float_funcs_no_unit {
    ($scope:ident, $( $name:ident ($arg1:ident, $arg2:ident) $func:ident ),* $(,)?) => {
        $( _add_2_floats_to_float_func_no_unit!($scope, $name($arg1, $arg2) $func) );*
    }
}

macro_rules! _add_float_to_bool_funcs {
    ($scope:ident, $( $name:ident ($arg1:ident) $func:ident ),* $(,)?) => {
        $( _add_float_to_bool_func!($scope, $name($arg1) $func) );*
    }
}

/// Creates all the math functions in the given scope.
pub fn create_math_functions(scope: &mut Scope) {
    macro_rules! add_num_func { ($($tt:tt)*) => {
        _add_num_func!(scope, $($tt)*) }
    }
    macro_rules! add_float_to_float_funcs_no_unit {
        ($($tt:tt)*) => { _add_float_to_float_funcs_no_unit!(scope, $($tt)*) }
    }
    macro_rules! add_2_floats_to_float_funcs_no_unit {
        ($($tt:tt)*) => { _add_2_floats_to_float_funcs_no_unit!(scope, $($tt)*) }
    }
    macro_rules! add_float_to_float_funcs {
        ($($tt:tt)*) => { _add_float_to_float_funcs!(scope, $($tt)*) }
    }
    macro_rules! add_float_to_bool_funcs {
        ($($tt:tt)*) => { _add_float_to_bool_funcs!(scope, $($tt)*) }
    }

    // Functions available in the Float type, which accept numbers with a unit.
    add_float_to_float_funcs!(
        abs(n)   abs,
        ceil(n)  ceil,
        floor(n) floor,
        round(n) round,
        fract(n) fract,
        trunc(n) trunc,
    );

    // Functions available in the Float type, which require a value without unit.
    add_float_to_float_funcs_no_unit!(
        log10(n)      log10,
        log2(n)       log2,
        ln(n)         ln,
        sin(n)        sin,
        cos(n)        cos,
        tan(n)        tan,
        asin(n)       asin,
        acos(n)       acos,
        atan(n)       atan,
        acosh(n)      acosh,
        cosh(n)       cosh,
        asinh(n)      asinh,
        sinh(n)       sinh,
        atanh(n)      atanh,
        tanh(n)       tanh,
        ai(n)         ai,
        cbrt(n)       cbrt,
        cot(n)        cot,
        coth(n)       coth,
        csc(n)        csc,
        csch(n)       csch,
        digamma(n)    digamma,
        eint(n)       eint,
        erf(n)        erf,
        erfc(n)       erfc,
        exp(n)        exp,
        exp10(n)      exp10,
        exp2(n)       exp2,
        exp_m1(n)     exp_m1,
        gamma(n)      gamma,
        j0(n)         j0,
        j1(n)         j1,
        li2(n)        li2,
        ln_1p(n)      ln_1p,
        ln_gamma(n)   ln_gamma,
        recip(n)      recip,
        recip_sqrt(n) recip_sqrt,
        sec(n)        sec,
        sech(n)       sech,
        signum(n)     signum,
        square(n)     square,
        y0(n)         y0,
        y1(n)         y1,
        zeta(n)       zeta,
    );

    // Functions available in the Float type, with return a boolean value.
    add_float_to_bool_funcs!(
        is_finite(n)        is_finite,
        is_infinite(n)      is_infinite,
        is_integer(n)       is_integer,
        is_nan(n)           is_nan,
        is_normal(n)        is_normal,
    );

    // Functions available in the Float type, with requires two values with no unit.
    add_2_floats_to_float_funcs_no_unit!(
        atan2(a, b)          atan2,
        agm(a, b)            agm,
        gamma_inc(a, b)      gamma_inc,
        hypot(a, b)          hypot,
        positive_diff(a, b)  positive_diff,
        remainder(a, b)      remainder,
    );

    // TODO: handle integers
    // factorial
    add_num_func!(
        factorial(n):
        arg_list::and(arg_list::check_args_count, arg_list::all_empty_units),
        move |numbers: Vec<&NumOperand>, container: &Container| -> Result<_, ExprError> {
            let n = get_u32(numbers[0].num())
                .map_err(|err| err!(Expr:Call:IntConversion(err)))?;
            Ok(num_operand!(container, [expr] (float!(Float::factorial(n)))))
        }
    );

    // root
    add_num_func!(
        root(n, k):
        arg_list::and(arg_list::check_args_count, arg_list::all_empty_units),
        move |numbers: Vec<&NumOperand>, container: &Container| -> Result<_, ExprError> {
            let n = numbers[0].num().clone();
            let k = get_u32(numbers[1].num())
                .map_err(|err| err!(Expr:Call:IntConversion(err)))?;
            Ok(num_operand!(container, [expr] (float!(n.root(k)))))
        }
    );

    // jn
    add_num_func!(
        jn(n, k):
        arg_list::and(arg_list::check_args_count, arg_list::all_empty_units),
        move |numbers: Vec<&NumOperand>, container: &Container| -> Result<_, ExprError> {
            let n = numbers[0].num().clone();
            let k = get_i32(numbers[1].num())
                .map_err(|err| err!(Expr:Call:IntConversion(err)))?;
            Ok(num_operand!(container, [expr] (float!(n.jn(k)))))
        }
    );

    // yn
    add_num_func!(
        yn(n, k):
        arg_list::and(arg_list::check_args_count, arg_list::all_empty_units),
        move |numbers: Vec<&NumOperand>, container: &Container| -> Result<_, ExprError> {
            let n = numbers[0].num().clone();
            let k = get_i32(numbers[1].num())
                .map_err(|err| err!(Expr:Call:IntConversion(err)))?;
            Ok(num_operand!(container, [expr] (float!(n.yn(k)))))
        }
    );

    // sqrt
    add_num_func!(
        sqrt(n):
        arg_list::check_args_count,
        move |numbers: Vec<&NumOperand>, container: &Container| -> Result<_, ExprError> {
            Ok(numbers[0].pow_num(&num_operand!(container, 0.5)).unwrap())
        }
    );

    // min() and max() functions:

    // Most of the code is similar for functions `min()` and `max()`, so we define a macro to avoid
    // code repetition:
    macro_rules! add_maxmin_func {
        ( $name:ident, $int_cmp_method:ident, $num_cmp_method:ident ) => {{
            use arg_list::*;
            let func = func!(
                $name,
                args_eval!(FunctionArgs::try_from(vec![ "...".to_string() ]).unwrap()),
                arg_list_checker!(and(
                    at_least_n_args(1),
                    or(
                        all_integers,
                        and(all_numbers, all_same_quantities)
                    )
                )),
                body!(|args, _cxt| {
                    // In order to know if all arguments are integers, we need to check only the 1st
                    // argument; indeed, if there is at least one non-integer number, then all the
                    // arguments have been converted to numbers, thanks to `all_numbers()` above:
                    let args_are_all_integers = args[0].is_int();

                    let mut args = args.iter();
                    // The arguments checker ensures that there is at least one argument:
                    let first_arg = args.next().unwrap();

                    let max_value = if args_are_all_integers {
                        args.map(|v| v.unwrap_int())
                            .fold(first_arg.unwrap_int(), |folded, int| if int.$int_cmp_method(folded) { int } else { folded })
                            .clone()
                            .into_value()
                    } else {
                        // Any integer has already been converted to a number.
                        args.map(|v| v.unwrap_num())
                            .fold(first_arg.unwrap_num(), |folded, num| if num.$num_cmp_method(folded) { num } else { folded })
                            .clone()
                            .into_value()
                    };


                    Ok(max_value)
                })
            );
            scope.add_named_function(func).unwrap()
        }}
    }

    // Use the macro definition just above to define the `min()` and `max()` functions:
    add_maxmin_func!(max, gt_int, gt_num);
    add_maxmin_func!(min, lt_int, lt_num);
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_function_sqrt() {
        with_test_context!(cxt, {
            let result = eval!(cxt, "sqrt(144.0)");
            assert_eq!(result, number!(cxt.container, 12));
        })
    }

    #[test]
    fn test_function_log10() {
        with_test_context!(cxt, {
            let result = eval!(cxt, "log10(10000.0)");
            assert!(result.equal(&number!(cxt.container, 4)));
        })
    }

    #[test]
    fn test_function_log10_bad_call() {
        with_test_context!(cxt, {
            let err = eval_err!(cxt, "log10(10000m)");
            assert_eq!(err, err!(Expr:Call:ArgList:Custom(
                "Argument(s) must be numbers without a unit"
            )));
        })
    }

    #[test]
    fn test_function_trigo() {
        with_test_context!(cxt, {
            let result = eval!(cxt, "sin(pi * 0.5)");
            assert_eq!(result, number!(cxt.container, 1));
        })
    }

    #[test]
    #[ignore]
    fn test_function_sin_in_degrees() {
        with_test_context!(cxt, {
            let result = eval!(cxt, "sin(90°)");
            assert_eq!(result, number!(cxt.container, 1));
        })
    }

    #[test]
    fn test_function_trigo_bad_call() {
        with_test_context!(cxt, {
            let err = eval_err!(cxt, "sin(1km)");
            assert_eq!(err, err!(Expr:Call:ArgList:Custom(
                "Argument(s) must be numbers without a unit"
            )));
            let err = eval_err!(cxt, r#" sin("hi") "#);
            assert_eq!(err, err!(Expr:Call:ArgList:MismatchTypes));
        })
    }

    #[test]
    fn test_function_arc_trigo() {
        with_test_context!(cxt, {
            macro_rules! cmp {
                ($result:ident, $cmp_func:ident, $num:expr) => (
                    $result.$cmp_func(&number!(cxt.container, $num))
                        .unwrap().unwrap_boolean().is_true()
                )
            }
            let result = eval!(cxt, "asin(1.0)");
            assert!(cmp!(result, cmp_gt, 1.5707963267948965));
            assert!(cmp!(result, cmp_lt, 1.5707963267948967));
        })
    }

    #[test]
    #[ignore]
    fn test_function_arc_trigo_with_int() {
        with_test_context!(cxt, {
            macro_rules! cmp {
                ($result:ident, $cmp_func:ident, $num:expr) => (
                    $result.$cmp_func(&number!(cxt.container, $num))
                        .unwrap().unwrap_boolean().is_true()
                )
            }
            let result = eval!(cxt, "asin(1)");
            assert!(cmp!(result, cmp_gt, 1.5707963267948965));
            assert!(cmp!(result, cmp_lt, 1.5707963267948967));
        })
    }

    #[test]
    fn test_function_arc_trigo_bad_call() {
        with_test_context!(cxt, {
            let err = eval_err!(cxt, "acos(1km)");
            assert_eq!(err, err!(Expr:Call:ArgList:Custom(
                "Argument(s) must be numbers without a unit"
            )));
        })
    }

    #[test]
    fn test_function_float_with_unit() {
        with_test_context!(cxt, {
            let result = eval!(cxt, "trunc(70.3m)");
            assert_eq!(result, number!(cxt.container, 70 m));
            let err = eval_err!(cxt, r#" trunc("hi") "#);
            assert_eq!(err, err!(Expr:Call:ArgList:MismatchTypes));
        })
    }

    #[test]
    fn test_function_max() {
        with_test_context!(cxt, {
            let result = eval!(cxt, "max(4.0, 8.0, 1.0, 2.0)");
            assert_eq!(result, number!(cxt.container, 8));

            let result = eval!(cxt, "max(400m, 800m, 1km, 2mm)");
            assert_eq!(result, number!(cxt.container, 1 km));

            let result = eval!(cxt, "max(1N)");
            assert_eq!(result, number!(cxt.container, 1 N));
        })
    }

    #[test]
    fn test_function_max_bad_call() {
        with_test_context!(cxt, {
            let result = eval_err!(cxt, "max(4.0, 8.0, 1m, 2.0)");
            assert_eq!(result, err!(Expr:Call:ArgList:MismatchUnits));

            let result = eval_err!(cxt, "max()");
            assert_eq!(result, err!(Expr:Call:ArgList:Custom(
                "The function max() must have at least 1 argument(s)"
            )));
        })
    }
}

use std::fs;

use crate::context::Scope;
use crate::eval::{
    ExprError,
    arg_checker as arg,
    func_macros::*,
};

/// Creates filesystem functions in the given scope.
pub fn create_fs_functions(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    add_func!(
        read_file,
        args![
            filename: arg::is_string
        ],
        body!(|args, _cxt| {
            let filename = args[0].unwrap_str().val_ref();
            fs::read_to_string(filename)
                .map(|content| string!(content))
                .map_err(|_| ExprError::IO(format!("Unable to read the file \"{}\"", filename)))
        })
    );

    add_func!(
        write_file,
        args![
            filename: arg::is_string,
            content: arg::is_string,
        ],
        body!(|args, _cxt| {
            let filename = args[0].unwrap_str().val_ref();
            let content = args[1].unwrap_str().val_ref();
            fs::write(filename, content)
                .map(|_| void!())
                .map_err(|_| ExprError::IO(format!("Unable to write the file \"{}\"", filename)))
        })
    );
}

//! Implements runtime functions relative to value types.
//!
//! * `is_number()`
//! * `is_int()`
//! * `is_string()`
//! * `is_boolean()`
//! * `is_function()`
//! * `is_void()`
//! * `is_array()`
//! * `int()`
//! * `num()`
//! * `str()`
//! * `type()`
//! * `whole_type()`

use std::convert::TryInto;

use crate::context::Scope;
use crate::parse::Input;
use crate::value::{CompoundQuantity, Format, IntOperand, IntoValue, NumOperand, Value};
use crate::eval::{
    arg_list_checker as arg_list,
    arg_checker as arg,
    func_macros::*,
    var_type,
};
use crate::parse::sub_parsers::{number, integer};

/// Creates functions dealing with value types in the given scope.
pub fn create_type_functions(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    // Add type test functions (1st item is the runtime function name, 2nd is the function to call
    // in the `Value` instance):
    macro_rules! add_type_test_func {
        ($runtime_name:ident, $rust_func:ident) => {
            add_func!(
                $runtime_name,
                args![ expr ],
                arg_list_checker!(arg_list::check_args_count),
                body!(|values, _| Ok(boolean!(values[0].$rust_func())) )
            );
        };
    }
    add_type_test_func!(is_number,   is_num);
    add_type_test_func!(is_int,      is_int);
    add_type_test_func!(is_string,   is_str);
    add_type_test_func!(is_boolean,  is_boolean);
    add_type_test_func!(is_function, is_func);
    add_type_test_func!(is_void,     is_void);
    add_type_test_func!(is_array,    is_array);
    add_type_test_func!(is_dict,     is_dict);

    // Tries to convert a value to a number:
    add_func!(
        num,
        args![ val ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, cxt| {
            let num = match &args[0] {
                Value::Num(n) => n.clone(),
                Value::Int(i) => i.into(),
                Value::Boolean(b) => {
                    let i: i64 = b.val().into();
                    i.into()
                },
                Value::Str(s) => {
                    let input = Input::new(s.val_ref(), 0);
                    let num = match number(cxt.container)(input) {
                        Ok((_, num)) => num,
                        Err(_) => match integer(input) {
                            Ok((_, i)) => NumOperand::from(i).into_value(),
                            Err(_) => return Err(err!(
                                Expr:Call:NumberConversion(s.val_ref().clone())
                            )),
                        }
                    };
                    return Ok(num);
                },
                _ => return Err(err!(Expr:Call:ArgList:MismatchTypes)),
            };
            Ok(num.into_value())
        })
    );

    // Tries to convert a value to a int:
    add_func!(
        int,
        args![ val ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| {
            let int = match &args[0] {
                Value::Num(n) => n.try_into().map_err(|_| err!(Expr:Call:ValueOutOfBounds))?,
                Value::Int(i) => i.val(),
                Value::Boolean(b) => b.val().into(),
                Value::Str(s) => {
                    let input = Input::new(s.val_ref(), 0);
                    let (_, i) = integer(input)
                        .map_err(|_| err!(Expr:Call:IntConversion:FailToParse))?;
                    return Ok(IntOperand::from(i).into_value());
                },
                _ => return Err(err!(Expr:Call:ArgList:MismatchTypes)),
            };
            Ok(int!(int))
        })
    );

    // Converts a value to a string:
    add_func!(
        "str",
        args![ val ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, cxt| {
            if args[0].is_str() {
                Ok(args[0].clone())
            } else {
                Ok(string!(args[0].format(cxt.container.format_config(), false)))
            }
        })
    );

    // Returns the main type of the given value.
    add_func!(
        "type",
        args![ v ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| {
            let var_type = args[0].get_type();
            let main_type = var_type.main_type();
            Ok(string!(main_type))
        })
    );

    // Returns the whole type of the given value.
    add_func!(
        whole_type,
        args![
            v,
            compact_numbers: arg::is_boolean, // optional argument
        ],
        arg_list_checker!(arg_list::args_count_range(1..=2)),
        body!(|args, _| {
            let var_type = args[0].get_type();
            let compact_numbers = args.get(1)
                .map(|cn| cn.unwrap_boolean().is_true())
                .unwrap_or(false);
            let whole_type = var_type.whole_type(var_type::FormatOptions { compact_numbers });
            Ok(string!(whole_type))
        })
    );

    // Returns the quantity of the given number.
    add_func!(
        quantity,
        args![
            v: arg::is_number,
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| {
            let num = args[0].unwrap_num();
            let cq = CompoundQuantity::from(num.unit());
            Ok(string!(cq.to_string()))
        })
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn function_is_number() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "is_number(45.0)"), v_true!());
            assert_eq!(eval!(cxt, "is_number(true)"), v_false!());
            assert_eq!(
                eval_err!(cxt, "is_number()"),
                err!(Expr:Call:ArgList:WrongArgCount)
            );
        })
    }

    #[test]
    fn function_is_int() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "is_int(7)"), v_true!());
            assert_eq!(eval!(cxt, r#" is_int("hi") "#), v_false!());
        })
    }

    #[test]
    fn function_is_string() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, r#" is_string("hi") "#), v_true!());
            assert_eq!(eval!(cxt, "is_string(true)"), v_false!());
        })
    }

    #[test]
    fn function_is_boolean() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, " is_boolean(true) "), v_true!());
            assert_eq!(eval!(cxt, "is_boolean(45)"), v_false!());
        })
    }

    #[test]
    fn function_is_function() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, " is_function(sin) "), v_true!());
            assert_eq!(eval!(cxt, "is_function(45)"), v_false!());
        })
    }

    #[test]
    fn function_is_array() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, " is_array([1,2,3]) "), v_true!());
            assert_eq!(eval!(cxt, "is_array(45)"), v_false!());
        })
    }

    #[test]
    fn function_is_void() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, " is_void(nop()) "), v_true!());
            assert_eq!(eval!(cxt, "is_void(sin)"), v_false!());
        })
    }

    #[test]
    fn function_int() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "int(11.2)"), int!(11));
            assert_eq!(eval!(cxt, "int(11.8)"), int!(12));
            assert_eq!(eval!(cxt, "int(10km)"), int!(10));
            assert_eq!(
                eval_err!(cxt, "int(10.0^80)"),
                err!(Expr:Call:ValueOutOfBounds)
            );
            assert_eq!(eval!(cxt, "int(10)"), int!(10));
            assert_eq!(eval!(cxt, r#"  int("10")  "#), int!(10));
            assert_eq!(
                eval_err!(cxt, "int([])"),
                err!(Expr:Call:ArgList:MismatchTypes)
            );
        })
    }

    #[test]
    fn function_num() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "num(11)"), number!(cxt.container, 11));
            assert_eq!(eval!(cxt, "num(10.2)"), number!(cxt.container, 10.2));
            assert_eq!(eval!(cxt, "num(10km)"), number!(cxt.container, 10 km));
            assert_eq!(eval!(cxt, r#"  num("10")  "#), number!(cxt.container, 10));
            assert_eq!(eval!(cxt, r#"  num("10 km")  "#), number!(cxt.container, 10 km));
            assert_eq!(
                eval_err!(cxt, r#"  num("aaaa")  "#),
                err!(Expr:Call:NumberConversion("aaaa".to_string()))
            );
            assert_eq!(
                eval_err!(cxt, "num([])"),
                err!(Expr:Call:ArgList:MismatchTypes)
            );
        })
    }

    #[test]
    fn function_str() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "str(11km)"), string!("11 km"));
            assert_eq!(eval!(cxt, "str(\"hello\")"), string!("hello"));
        })
    }

    #[test]
    fn function_type() {
        with_test_context!(cxt, {
            assert_eq!(
                eval_err!(cxt, "type()"),
                err!(Expr:Call:ArgList:WrongArgCount)
            );
            assert_eq!(
                eval_err!(cxt, "type(10, 10)"),
                err!(Expr:Call:ArgList:WrongArgCount)
            );
            assert_eq!(eval!(cxt, "type(11)"), string!("int"));
            assert_eq!(eval!(cxt, "type(11.0)"), string!("number"));
            assert_eq!(eval!(cxt, "type(11km)"), string!("number"));
            assert_eq!(eval!(cxt, "type(2 > 3)"), string!("boolean"));
            assert_eq!(eval!(cxt, "type(print)"), string!("function"));
            assert_eq!(eval!(cxt, "type(nop())"), string!("void"));
        })
    }

    #[test]
    fn function_whole_type() {
        with_test_context!(cxt, {
            assert_eq!(
                eval_err!(cxt, "whole_type()"),
                err!(Expr:Call:ArgList:WrongArgCount)
            );
            assert_eq!(
                eval_err!(cxt, "whole_type(10, 10)"),
                err!(Expr:Call:ArgList:Arg:MismatchTypes)
            );
            assert_eq!(eval!(cxt, "whole_type(11)"), string!("int"));
            assert_eq!(eval!(cxt, "whole_type(2 > 3)"), string!("boolean"));
            assert_eq!(eval!(cxt, "whole_type(print)"), string!("function"));
            assert_eq!(eval!(cxt, "whole_type(nop())"), string!("void"));
            assert_eq!(eval!(cxt, "whole_type(11.0)"), string!("number()"));
            assert_eq!(eval!(cxt, "whole_type(11km)"), string!("number(length)"));
            assert_eq!(eval!(cxt, "whole_type(11km, true)"), string!("{length}"));
        })
    }
}

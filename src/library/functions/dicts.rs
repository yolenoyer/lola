use std::convert::TryFrom;

use crate::context::Scope;
use crate::eval::{
    ExprError,
    arg_checker as arg,
    arg_list_checker as arg_list,
    func_macros::*,
};
use crate::value::{ArrayOperand, HashableValue, IntoValue};

/// Creates functions dealing with dicts in a container.
pub fn create_dict_functions(scope: &mut Scope) {
    macro_rules! add_func {
        ($($tt:tt)*) => { scope.add_named_function(func!($($tt)*)).unwrap() }
    }

    add_func!(
        dict_keys,
        args![
            d: arg::is_dict,
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| -> Result<_, ExprError> {
            let dict = args[0].unwrap_dict().inner().borrow();
            let keys = dict.keys()
                .map(|k| k.to_owned().into_value())
                .collect::<Vec<_>>();
            Ok(ArrayOperand::from(keys).into_value())
        })
    );

    add_func!(
        dict_values,
        args![
            d: arg::is_dict,
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| -> Result<_, ExprError> {
            let dict = args[0].unwrap_dict().inner().borrow();
            let keys = dict.values()
                .cloned()
                .collect::<Vec<_>>();
            Ok(ArrayOperand::from(keys).into_value())
        })
    );

    add_func!(
        dict_items,
        args![
            d: arg::is_dict,
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| -> Result<_, ExprError> {
            let dict = args[0].unwrap_dict();
            let key_values = dict.get_key_value_pairs();

            Ok(ArrayOperand::from(key_values).into_value())
        })
    );

    add_func!(
        dict_remove,
        args![
            d: arg::is_dict,
            key: arg::is_hashable_value,
        ],
        arg_list_checker!(arg_list::check_args_count),
        body!(|args, _| -> Result<_, ExprError> {
            let mut dict = args[0].unwrap_dict().inner().borrow_mut();
            let hashable_value = HashableValue::try_from(args[1].clone()).unwrap();
            let removed = dict.remove(&hashable_value);
            if removed.is_some() {
                Ok(void!())
            } else {
                Err(err!(Expr:Dict:DictKeyNotFound(hashable_value)))
            }
        })
    );
}

#[cfg(test)]
mod tests {
    use crate::value::{HashableValue, StrOperand};

    #[test]
    fn function_dict_keys() {
        with_test_context!(cxt, {
            eval!(cxt, "d = { foo: 1, bar: 2 }");
            let keys = eval!(cxt, "dict_keys(d)");
            assert!(keys == array![string!("foo"), string!("bar")]
                || keys == array![string!("bar"), string!("foo")]);
        });
    }

    #[test]
    fn function_dict_values() {
        with_test_context!(cxt, {
            eval!(cxt, "d = { foo: 1, bar: 2 }");
            let keys = eval!(cxt, "dict_values(d)");
            assert!(keys == array![int!(1), int!(2)]
                || keys == array![int!(2), int!(1)]);
        });
    }

    #[test]
    fn function_dict_items() {
        with_test_context!(cxt, {
            eval!(cxt, "d = { foo: 1, bar: 2 }");
            let keys = eval!(cxt, "dict_items(d)");
            assert!(
                keys == array![
                    array![string!("foo"), int!(1)],
                    array![string!("bar"), int!(2)],
                ] ||
                keys == array![
                    array![string!("bar"), int!(2)],
                    array![string!("foo"), int!(1)],
                ]
            );
        });
    }

    #[test]
    fn function_dict_remove() {
        with_test_context!(cxt, {
            eval!(cxt, r#"d = { foo: 1, bar: 2 }; dict_remove(d, "foo") "#);
            assert_eq!(eval!(cxt, "d"), dict!{ string!("bar") => int!(2) });
            assert_eq!(
                eval_err!(cxt, r#" dict_remove(d, "foo") "#),
                err!(Expr:Dict:DictKeyNotFound(HashableValue::Str(StrOperand::from("foo"))))
            );
        });
    }
}

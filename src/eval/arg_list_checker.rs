//! This module contains functions which can be used to check the list of arguments sent to a
//! function.

use std::fmt;
use std::ops;

use crate::value::{Value, NumOperand, CompoundQuantity};
use super::ArgError;
use super::{Function, cast_int_to_number};

use super::get_number;

/// Each function which has this prototype can be used as a global argument checker.
pub trait ArgListChecker: Fn(&mut Vec<Value>, &Function) -> ArgCheckResult {}

impl<F> ArgListChecker for F
where F: Fn(&mut Vec<Value>, &Function) -> ArgCheckResult
{}

/// The result of the check of a function argument list.
pub type ArgCheckResult = Result<(), ArgListError>;

/// Errors that can occur when checking function arguments and return type.
#[derive(Debug, PartialEq)]
pub enum ArgListError {
    Arg(ArgError),
    MismatchTypes,
    MismatchUnits,
    WrongArgCount,
    Custom(String),
}

impl fmt::Display for ArgListError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Arg(arg_err)  => write!(f, "{}", arg_err),
            Self::MismatchTypes => write!(f, "Mismatch types"),
            Self::MismatchUnits => write!(f, "Mismatch units"),
            Self::WrongArgCount => write!(f, "Wrong number of arguments"),
            Self::Custom(s)     => write!(f, "{}", s),
        }
    }
}

/// Helper which returns a [`ArgListError`] custom error.
macro_rules! custom_error {
    ($($tt:tt)*) => ( Err(err!(ArgList:Custom(format!($($tt)*)))) )
}

// ARGUMENTS CHECKERS

/// Checks if there is the same number of arguments in the given list compared to the function
/// definition.
pub fn check_args_count(values: &mut Vec<Value>, func: &Function) -> ArgCheckResult {
    if values.len() != func.nb_args() {
        Err(err!(ArgList:WrongArgCount))
    } else {
        Ok(())
    }
}

/// Checks for a minimun argument count.
pub fn at_least_n_args(n: usize) -> impl ArgListChecker {
    move |values: &mut Vec<Value>, func: &Function| {
        if values.len() >= n {
            Ok(())
        } else {
            custom_error!("The function {}() must have at least {} argument(s)", func.name(), n)
        }
    }
}

/// Checks if the arguments count is within the given range.
pub fn args_count_range<R: ops::RangeBounds<usize>>(range: R) -> impl ArgListChecker {
    move |values: &mut Vec<Value>, _: &Function| {
        if range.contains(&values.len()) {
            Ok(())
        } else {
            Err(err!(ArgList:WrongArgCount))
        }
    }
}

/// Checks if all values are numbers. Integers are automatically converted to numbers.
pub fn all_numbers(values: &mut Vec<Value>, _: &Function) -> ArgCheckResult {
    if values.iter_mut().all(|value| {
        match value {
            Value::Num(_) => true,
            Value::Int(_) => {
                cast_int_to_number(value);
                true
            },
            _ => false,
        }
    }) {
        Ok(())
    } else {
        Err(err!(ArgList:MismatchTypes))
    }
}

/// Checks if all values are integers.
pub fn all_integers(values: &mut Vec<Value>, _: &Function) -> ArgCheckResult {
    if values.iter().all(|value| value.is_int()) {
        Ok(())
    } else {
        Err(err!(ArgList:MismatchTypes))
    }
}

/// Checks if all values are numbers and have empty units. Integers are automatically converted to
/// numbers.
pub fn all_empty_units(values: &mut Vec<Value>, _: &Function) -> ArgCheckResult {
    let values = get_numbers(values)?;

    if values.iter().all(|value| value.unit().is_empty()) {
        Ok(())
    } else {
        custom_error!("Argument(s) must be numbers without a unit")
    }
}

/// Checks if all values are numbers have the same quantities. Integers are automatically converted
/// to numbers.
pub fn all_same_quantities(values: &mut Vec<Value>, _: &Function) -> ArgCheckResult {
    if values.is_empty() {
        return Ok(());
    }

    let values = get_numbers(values)?;

    let compound_q = CompoundQuantity::from(values[0].unit());
    let has_same_quantities = values.iter().skip(1).all(|value| {
        compound_q == CompoundQuantity::from(value.unit())
    });

    if has_same_quantities {
        Ok(())
    } else {
        Err(err!(ArgList:MismatchUnits))
    }
}

// /// Creates the following argument checker: for each value, run a given arg checker function. If
// /// one of the predicates return an error, then the arguments are considered as invalid and the
// /// found error is returned.
// pub fn argument_predicates(preds: Vec<Option<checker::BoxedArgChecker>>) -> impl ArgListChecker {
//     move |values: &mut Vec<Value>, func: &Function| -> ArgCheckResult {
//         let nb_args = func.nb_args();
//         if nb_args != values.len() || nb_args != preds.len() {
//             return Err(err!(ArgList:WrongArgCount));
//         }
//
//         let found_error = izip!(values.iter_mut(), func.args().iter(), preds.iter())
//             .filter(|(_, _, pred)| pred.is_some())
//             .map(|(value, arg, pred)| pred.as_ref().unwrap()(value, ArgKind::Arg(arg), func))
//             .find(|result| result.is_err());
//
//         match found_error {
//             Some(err_result) => err_result,
//             None => Ok(()),
//         }
//     }
// }

/// Checks if all values are numbers, and returns the list of numbers, otherwise returns an error.
fn get_numbers(values: &mut Vec<Value>) -> Result<Vec<&NumOperand>, ArgListError> {
    values.iter_mut()
        .map(|v| get_number(v).map_err(ArgListError::Arg))
        .collect::<Result<Vec<_>, _>>()
}

// ARGUMENT CHECKERS COMBINATORS

/// Combines two checkers by a "and" relation.
pub fn and(a: impl ArgListChecker, b: impl ArgListChecker) -> impl ArgListChecker {
    move |values: &mut Vec<Value>, func: &Function| {
        a(values, func)?;
        b(values, func)?;
        Ok(())
    }
}

/// Combines two checkers by a "or" relation.
pub fn or(a: impl ArgListChecker, b: impl ArgListChecker) -> impl ArgListChecker {
    move |values: &mut Vec<Value>, func: &Function| {
        a(values, func).or_else(|_| b(values, func))
    }
}

/// Shortcut combinator: checks the argument count then checks something else.
pub fn argument_count_and(a: impl ArgListChecker) -> impl ArgListChecker {
    and(check_args_count, a)
}

//! Defines the [`ExprError`] struct.

use std::fmt;

use crate::value::{DictError, OperationError, OperationResult, Value};
use crate::eval::function::{
    FunctionError,
    CallError,
};
use crate::parse::{
    Span,
    UError,
};
use crate::context::Container;

/// Errors which can occur when evaluating an expression.
#[derive(Debug, PartialEq)]
pub enum ExprError {
    /// This error will be catched and handled by a parent loop block. If no loop is found, it will
    /// raise a real error.
    Break(Value),
    /// This error will be catched and handled by a parent loop block. If no loop is found, it will
    /// raise a real error.
    Continue(Value),
    /// This error will be catched and handled by a parent scoped block. If no scoped block is
    /// found, it will raise a real error.
    Leave(Value),
    /// This error will be catched and handled by a parent function body. If no function body is
    /// found, it will raise a real error.
    Return(Value),
    /// This error will be catched and handled by a parent `catch` block. If no `catch` block is
    /// found, it will raise a real error.
    Throw(Value),

    /// Trying to get the value of an inexistant variable.
    UndefinedVariable(String),
    /// Mismatch types.
    MismatchTypes,
    /// Trying to call a non-callable value.
    NotCallable,
    /// Variable already exists.
    VarAlreadyExists(String),
    /// Unassignable expression.
    Unassignable,
    /// Unable to perfom a destructuring assignment.
    UnableToDestructure,
    /// An error occured when defining a function.
    Function(FunctionError),
    /// An error occured when performing an operation a some values.
    Operation(OperationError),
    /// Error when trying to create a dict.
    Dict(DictError),
    /// An error occured when trying to call function (e.g.: mismatch function arguments).
    Call(CallError),
    /// An error occured when trying to access the filesystem.
    IO(String),
    /// A `break` statement was encountered outside a loop, inside a function body.
    BreakFunc,
    /// A `continue` statement was encountered outside a loop, inside a function body.
    ContinueFunc,
    /// Parse error when importing a file.
    Parse(String, nom::Err<UError>),

    /// A wrapper which holds a real error, with information about the location of the
    /// error into the source input code. This variant is the main mechanism which allows errors to
    /// display a line number and other source location information. The [`ExprResultSpanUtil`]
    /// trait helps to work with these "spanned" errors more easily.
    Spanned(Span, Box<ExprError>),
}

impl fmt::Display for ExprError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::UndefinedVariable(name) => write!(f, "Undefined variable: '{}'", name),
            Self::MismatchTypes           => write!(f, "Mismatch types"),
            Self::NotCallable             => write!(f, "The value is not callable"),
            Self::VarAlreadyExists(name)  => write!(f, "The variable '{}' already exists", name),
            Self::Unassignable            => write!(f, "Unassignable expression"),
            Self::UnableToDestructure     => write!(f, "Unable to perfom a destructuring assignment"),
            Self::Function(err)           => write!(f, "{}", err),
            Self::Operation(err)          => write!(f, "{}", err),
            Self::Dict(err)               => write!(f, "{}", err),
            Self::Call(err)               => write!(f, "{}", err),
            Self::IO(err)                 => write!(f, "Input/Output error: {}", err),
            Self::Break(_) |
            Self::BreakFunc               => write!(f, "Break statement outside of a loop"),
            Self::Leave(_)                => write!(f, "Leave statement outside of a block"),
            Self::Return(_)               => write!(f, "Return statement outside of a function"),
            Self::Continue(_) |
            Self::ContinueFunc            => write!(f, "Continue statement outside of a loop"),
            Self::Throw(v)                => write!(f, "Exception: {}", v),
            Self::Spanned(span, err)      => write!(f, "{}: {}", span, err),
            Self::Parse(file, err)        => write!(f, "Parse error in imported file '{}': {}", file, err),
        }
    }
}

impl ExprError {
    /// If this error is [`Self::Spanned`], then consumes the error and unwraps the inner error.
    pub fn unspan(self) -> ExprError {
        match self {
            Self::Spanned(_, err) => *err,
            _ => self,
        }
    }

    /// Returns a reference to `self`, unless this error is [`Self::Spanned`]; in this case returns a
    /// reference to the inner error.
    ///
    /// This is useful to test is an error is of a particular kind, without taking into account
    /// [`Self::Spanned`] which is not a an error, but a wrapper to a real error.
    pub fn unspan_ref(&self) -> &ExprError {
        match self {
            Self::Spanned(_, err) => err,
            _ => self,
        }
    }

    /// Allows to display the error with the source filename if present. The container is needed to
    /// retrieve the name.
    pub fn display_with_source(&self, container: &Container) -> String {
        if let Self::Spanned(span, _err) = self {
            if let Some(index) = span.parsed_input_index {
                let source = container.get_parsed_input_source(index);
                format!("{}, {}", source, self)
            } else {
                format!("{}", self)
            }
        } else {
            format!("{}", self)
        }
    }
}

impl From<OperationError> for ExprError {
    fn from(err: OperationError) -> ExprError {
        Self::Operation(err)
    }
}

/// The result of the evaluation of an expression.
pub type ExprResult = Result<Value, ExprError>;

/// Allows to easily extract a value error from an [`ExprResult`], by taking into account that the
/// error can be wrapped into a [`ExprError::Spanned`].
///
/// # Panics
///
/// Panics if the given expression result is not an error with the given variant.
///
/// [`ExprResult`]: crate::eval::ExprResult
/// [`ExprError::Spanned`]: crate::eval::ExprError
#[macro_export]
#[doc(hidden)]
macro_rules! unwrap_expr_error_value {
    ( $expr_result:expr, $variant:ident ) => {
        match $expr_result.map_unspan_err() {
            Err(ExprError::$variant(value)) => value,
            _ => unreachable!(),
        }
    };
}

/// Many expression errors are wrapped in a [`ExprError::Spanned`], in order to store the source code
/// position where an error occured.
///
/// The following trait gives capabilities to [`ExprResult`] in order to handle more easily these
/// kinds of error.
pub trait ExprResultSpanUtil {
    /// Consumes the result; if the result is an error and this error is [`ExprError::Spanned`], then
    /// replace this error by the inner error wrapped into it.
    fn map_unspan_err(self) -> ExprResult;

    /// If the result is an error, then returns a reference to the error, by taking into account that
    /// the error can be wrapped into a [`ExprError::Spanned`].
    fn get_unspan_err(&self) -> Option<&ExprError>;

    /// If the result is an error and this error is [`ExprError::Spanned`], then returns a copy of
    /// the inner [`Span`] (source code location of the real inner error).
    fn get_err_span(&self) -> Option<Span>;
}

impl ExprResultSpanUtil for ExprResult {
    fn map_unspan_err(self) -> ExprResult {
        self.map_err(|expr_error| expr_error.unspan())
    }

    fn get_unspan_err(&self) -> Option<&ExprError> {
        match self {
            Ok(_) => None,
            Err(err) => Some(err.unspan_ref()),
        }
    }

    fn get_err_span(&self) -> Option<Span> {
        if let Err(ExprError::Spanned(span, _inner_err)) = self {
            Some(span.clone())
        } else {
            None
        }
    }
}

/// Internal trait used to convert OperationResult into ExprResult.
pub trait IntoExprResult {
    fn into_expr_result(self) -> ExprResult;
}

impl IntoExprResult for OperationResult {
    fn into_expr_result(self) -> ExprResult {
        match self {
            Ok(n) => Ok(n),
            Err(err) => Err(err!(Expr:Operation(err))),
        }
    }
}

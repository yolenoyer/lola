use crate::{
    context::ContextRef,
    value::{
        ArrayOperand,
        DictHashMap,
        DictOperand,
        HashableValue,
        OperationResult,
        Value,
    },
};

use super::{ExprError, ExprSpan};

/// Something that can be assigned, like:
///  - a variable,
///  - an array with an index,
///  - a dict with a key,
///  - a destructuring pattern.
#[derive(Debug, Clone, PartialEq)]
pub struct Assignable {
    pub mode: AssignMode,
    pub target: AssignTarget,
}

impl Assignable {
    pub fn new(mode: AssignMode, target: AssignTarget) -> Self {
        Self { mode, target }
    }

    pub fn set(&self, cxt: &mut ContextRef, value: Value) -> Result<(), ExprError> {
        self.get_value_mut(cxt, Some(value), &|lhs: &mut Value, rhs| {
            *lhs = rhs.unwrap();
            Ok(void!())
        })?.unwrap(); // `unwrap()` is safe because the above closure always return `Ok`
        Ok(())
    }

    /// Calls the given closure by giving to it a mutable reference to the assignable value.
    pub fn get_value_mut(
        &self,
        cxt: &mut ContextRef,
        opt_rhs: Option<Value>,
        f: &impl Fn(&mut Value, Option<Value>) -> OperationResult
    ) -> Result<OperationResult, ExprError>
    {
        if self.mode == AssignMode::Let {
            match &self.target {
                AssignTarget::Variable(var_name) => {
                    // TODO: avoid creating this void value which is immediatly overwritten:
                    cxt.scope.define_var(var_name.clone(), void!())
                        .map_err(|_| ExprError::VarAlreadyExists(var_name.clone()))?;
                }
                AssignTarget::Pattern(_pattern) => (),
                AssignTarget::IndexedExpr(..) => {
                    return Err(err!(Expr:Unassignable));
                },
            }
        }

        let res = match &self.target {
            AssignTarget::Variable(var_name) => {
                cxt.scope.get_var_mut(var_name, |lhs| f(lhs, opt_rhs))
                    .map_err(|_| err!(Expr:UndefinedVariable(var_name.clone())))?
            },
            AssignTarget::Pattern(pattern) => {
                match pattern {
                    AssignPattern::Array(assignables) => {
                        let mut new_array = vec![];

                        if let Some(rhs) = opt_rhs {
                            if let Some(array) = rhs.get_array() {
                                let values = array.values().borrow().clone();
                                for (assignable, rhs) in assignables.iter().zip(values.into_iter()) {
                                    match assignable.get_value_mut(cxt, Some(rhs), f)? {
                                        Ok(rhs) => new_array.push(rhs),
                                        Err(err) => return Ok(Err(err)),
                                    }
                                }
                            } else {
                                return Err(err!(Expr:UnableToDestructure));
                            }
                        } else {
                            for assignable in assignables {
                                match assignable.get_value_mut(cxt, None, f)? {
                                    Ok(rhs) => new_array.push(rhs),
                                    Err(err) => return Ok(Err(err)),
                                }
                            }
                        }

                        Ok(Value::Array(ArrayOperand::from(new_array)))
                    },
                    AssignPattern::Dict(dict_assignables) => {
                        let mut new_dict = DictHashMap::default();

                        let rhs = match opt_rhs {
                            Some(rhs) => rhs,
                            None => return Err(err!(Expr:UnableToDestructure)),
                        };

                        let rhs_dict = match rhs.get_dict() {
                            Some(rhs_dict) => rhs_dict,
                            None => return Err(err!(Expr:UnableToDestructure)),
                        };
                        let rhs_dict = rhs_dict.inner().borrow();

                        for dict_assignable in dict_assignables {
                            let dict_value = match rhs_dict.get(&dict_assignable.key) {
                                Some(dict_value) => dict_value,
                                None => return Err(err!(Expr:Dict:DictKeyNotFound(dict_assignable.key.clone()))),
                            };

                            match dict_assignable.assignable.get_value_mut(cxt, Some(dict_value.clone()), f)? {
                                Ok(rhs) => {
                                    new_dict.insert(dict_assignable.key.clone(), rhs);
                                },
                                Err(err) => return Ok(Err(err)),
                            }
                        }

                        Ok(Value::Dict(DictOperand::from(new_dict)))
                    },
                }
            },
            AssignTarget::IndexedExpr(expr, index) => {
                let array = expr.eval(cxt)?;
                let index = index.eval(cxt)?;
                let operand = array.as_operand_value();
                match opt_rhs {
                    Some(rhs) => {
                        operand.get_indexed_mut(&index, &move |lhs| f(lhs, Some(rhs.clone())))?
                    }
                    None => {
                        operand.get_indexed_mut(&index, &|lhs| f(lhs, None))?
                    }
                }
            },
        };

        Ok(res)
    }

    /// Initializes a new variable in the current scope (the variable must not exist). Normally, new
    /// variables should be reserved to the `Assignable::LetVariable` variant, but for internal
    /// purposes it is also allowed for the `Assignable::SetVariable` variant in this particular
    /// function (see `Expr::For` in the [`Expr::eval()`] function).
    pub fn define_as_void(&self, cxt: &mut ContextRef) -> Result<(), ExprError> {
        match &self.target {
            AssignTarget::Variable(var_name) => {
                cxt.scope.define_var(var_name.clone(), void!())
                    .map_err(|_| ExprError::VarAlreadyExists(var_name.clone()))?;
            },
            AssignTarget::Pattern(pattern) => {
                match pattern {
                    AssignPattern::Array(assignables) => {
                        for assignable in assignables {
                            assignable.define_as_void(cxt)?;
                        }
                    },
                    AssignPattern::Dict(dict_assignables) => {
                        for dict_assignable in dict_assignables {
                            dict_assignable.assignable.define_as_void(cxt)?;
                        }
                    },
                }
            },
            AssignTarget::IndexedExpr(..) => (), // Should not happen, but skip silently
        }

        Ok(())
    }
}

/// Assignment mode.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AssignMode {
    /// Define a variable.
    Let,
    /// Set something which already exists.
    Set,
}

/// Assignment target.
#[derive(Debug, Clone, PartialEq)]
pub enum AssignTarget {
    /// A variable to be modified.
    Variable(String),
    /// A destructuring pattern.
    Pattern(AssignPattern),
    /// An element from an array or a dict, to be created or modified (creation is only possible
    /// with a dict element).
    IndexedExpr(Box<ExprSpan>, Box<ExprSpan>),
}

/// A destructuring pattern.
#[derive(Debug, Clone, PartialEq)]
pub enum AssignPattern {
    Array(Vec<Assignable>),
    Dict(Vec<DictAssignable>),
}

/// An assignable value into a dict destructuring pattern.
#[derive(Debug, Clone, PartialEq)]
pub struct DictAssignable {
    pub assignable: Assignable,
    pub key: HashableValue,
}

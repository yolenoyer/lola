//! Defines the [`ExprSpan`] struct.

use std::fmt;

use crate::context::ContextRef;
use crate::parse::Span;
use crate::eval::{Expr, ExprError, ExprResult};

/// A wrapper for [`Expr`] expressions, which holds a [`Span`] instance which gives information
/// about the source code location of the inner error.
#[derive(Clone)]
pub struct ExprSpan {
    pub expr: Expr,
    pub span: Span,
}

impl ExprSpan {
    /// Creates a new span expression, with no source location information.
    pub fn new(expr: Expr) -> Self {
        Self {
            expr,
            span: Span::default(),
        }
    }

    /// Creates a new expression with a defined span.
    pub fn with_span(expr: Expr, span: Span) -> Self {
        Self {
            expr,
            span,
        }
    }

    /// Wrapper for `self.expr.eval()`. If the expression evaluation returns an error, then checks
    /// if this error is spanned. If not, then wraps the real returned error into a
    /// [`ExprError::Spanned`] instance by using `self.span`.
    pub fn eval(&self, cxt: &mut ContextRef) -> ExprResult {
        self.expr.eval(cxt)
            .map_err(|err| {
                if let ExprError::Spanned(span, err) = err {
                    ExprError::Spanned(span, err)
                } else {
                    ExprError::Spanned(self.span.clone(), Box::new(err))
                }
            })
    }

    /// Defines the span.
    pub fn set_span(&mut self, span: Span) {
        self.span = span;
    }
}

impl PartialEq<ExprSpan> for ExprSpan {
    fn eq(&self, other: &ExprSpan) -> bool {
        self.span == other.span
    }
}

impl fmt::Debug for ExprSpan {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if f.alternate() {
            write!(f, "{:?}: {:?}", self.span, self.expr)
        } else {
            write!(f, "{:#?}: {:#?}", self.span, self.expr)
        }
    }
}


pub use crate::{
    func,
    args,
    args_eval,
    body,
    body_eval,
    arg_list_checker,
    opt_return_checker,
};

/// Creates a new runtime function, with the help of the macros [`args!`], [`args_eval!`],
/// [`body!`], [`body_eval!`] and [`arg_list_checker!`].
///
/// ```
/// # use std::convert::TryFrom;
/// # use lola::{func, args, arg_list_checker, body};
/// # use lola::eval::{
/// #     arg_checker as arg,
/// #     arg_list_checker as arg_list,
/// #     function::FunctionArgs,
/// # };
/// # use lola::int;
/// # use lola::value::IntoValue;
/// #
/// let func = func!(
///     double_int,  // Name of the function
///     args![ val: arg::is_int ],
///     arg_list_checker!(arg_list::check_args_count),
///     body!(|args, _| {
///         // Thanks to the argument checker, we can safely  unwrap the argument to an
///         // `IntOperand`:
///         let lhs = args[0].unwrap_int();
///         Ok(int!(lhs.val() * 2))
///     })
/// );
/// ```
#[macro_export]
macro_rules! func {
    ( $name:ident, $($setters:expr),* $(,)? ) => {{
        func!(__impl stringify!($name), $($setters),*)
    }};
    ( $name:expr, $($setters:expr),* $(,)? ) => {{
        func!(__impl $name, $($setters),*)
    }};
    (__impl $name:expr, $($setters:expr),* $(,)? ) => {{
        let mut builder = $crate::eval::FunctionBuilder::default();
        builder.name($name);
        $( $setters(&mut builder); )*
        builder.build().unwrap()
    }};
}

/// This macro is designed to be used inside the [`func!`] macro.
///
/// Creates a list of arguments definitions. This must be a comma-separated list of elements with
/// the given syntax:
///
/// ```ignore
/// NAME: Option<ARG_CHECKER>
/// ```
///
/// Where:
///
///  * `NAME` is an identifier which will be stringified;
///  * `ARG_CHECKER` is a function having the [`ArgChecker`] prototype.
///
/// See the module [`arg_checker`] for a list of the available argument checkers.
///
/// [`ArgChecker`]: crate::eval::ArgChecker
/// [`arg_checker`]: crate::eval::arg_checker
///
/// ```
/// # use std::convert::TryFrom;
/// # use lola::args;
/// # use lola::eval::{
/// #     arg_checker as arg,
/// #     function::FunctionArgs,
/// # };
/// let args = args![
///     // This argument is named `n`, and must be an integer:
///     n: arg::is_int,
///     // This argument is named `str_or_int`, and must be an integer or a string:
///     str_or_int: arg::or(arg::is_int, arg::is_string),
///     // This argument is named `any`, and has no constraint:
///     any,
/// ];
/// ```
#[macro_export]
macro_rules! args {
    ( $($name:ident $(: $checker:expr)?),* $(,)? ) => {{
        use $crate::eval::function::{FunctionArgs, FunctionBuilder};
        use std::convert::TryFrom;
        |builder: &mut FunctionBuilder| {
            builder.args(FunctionArgs::try_from(vec![
                $( args!(__arg stringify!($name).to_string() $(, $checker)?) ),*
            ]).unwrap());
        }
    }};
    (__arg $name:expr) => {{
        ($name, None)
    }};
    (__arg $name:expr, $checker:expr) => {{
        use $crate::eval::ArgChecker;
        use std::rc::Rc;
        (
            $name,
            {
                let c: Rc<dyn ArgChecker> = Rc::new($checker);
                Some(c)
            }
        )
    }};
}

/// This macro is designed to be used inside the [`func!`] macro.
///
/// Creates a list of arguments definitions. This is a raw, more powerful version of [`args!`]. The
/// argument has to be a [`FunctionArgs`].
///
/// [`FunctionArgs`]: crate::eval::function::FunctionArgs
#[macro_export]
macro_rules! args_eval {
    ( $function_args:expr ) => (
        |builder: &mut $crate::eval::FunctionBuilder| {
            builder.args($function_args);
        }
    );
}

/// This macro is designed to be used inside the [`func!`] macro.
///
/// Creates the body of a native function. The given closure must be a [`NativeFunction`].
///
/// [`NativeFunction`]: crate::eval::function::NativeFunction
#[macro_export]
macro_rules! body {
    ($closure:expr) => {
        |builder: &mut $crate::eval::FunctionBuilder| {
            builder.native_body(std::boxed::Box::new($closure));
        }
    };
}

/// This macro is designed to be used inside the [`func!`] macro.
///
/// Creates the body of a function. This is a raw, more powerful version of [`body!`]. The argument
/// has to be a [`FunctionBody`].
///
/// [`FunctionBody`]: crate::eval::function::FunctionBody
#[macro_export]
macro_rules! body_eval {
    ($function_body:expr) => {
        |builder: &mut $crate::eval::FunctionBuilder| {
            builder.body($function_body);
        }
    };
}

/// This macro is designed to be used inside the [`func!`] macro.
///
/// Adds a global arguments checker. The given closure must be a [`ArgListChecker`].
///
/// See the module [`arg_list_checker`] for a list of the available global arguments checkers.
///
/// [`ArgListChecker`]: crate::eval::ArgListChecker
/// [`arg_list_checker`]: crate::eval::arg_list_checker
#[macro_export]
macro_rules! arg_list_checker {
    ($closure:expr) => {
        |builder: &mut $crate::eval::FunctionBuilder| {
            builder.arg_list_checker(std::rc::Rc::new($closure));
        }
    };
}

/// This macro is designed to be used inside the [`func!`] macro.
///
/// Adds a return value checker. The given closure must be a [`ArgChecker`].
///
/// See the module [`arg_checker`] for a list of the available global arguments checkers.
///
/// [`ArgChecker`]: crate::eval::ArgChecker
/// [`arg_checker`]: crate::eval::arg_checker
#[macro_export]
macro_rules! opt_return_checker {
    ($opt_closure:expr) => {
        |builder: &mut $crate::eval::FunctionBuilder| {
            if let Some(checker) = $opt_closure {
                builder.return_checker(checker);
            }
        }
    };
}

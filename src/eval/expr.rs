//! Defines the [`Expr`] struct.
//!
//! The [`Expr`] struct is the heart of the code evaluation process: it holds a single language
//! statement or operation, and can be nested to form a tree-like representation (AST) of a portion
//! of code.

use std::convert::TryFrom;
use std::rc::Rc;

use crate::context::ContextRef;
use crate::util::NoDebug;
use crate::value::{
    ArrayOperand,
    DictOperand,
    FuncOperand,
    IntoValue,
    NumOperand,
    OperationResult,
    Unit,
    Value,
    ValueMergeStrategy,
};
use crate::eval::{
    Assignable,
    ExprSpan,
    ExprError,
    ExprResult,
    IntoExprResult,
    ExprResultSpanUtil,
    function::{
        FunctionArg,
        FunctionArgs,
        FunctionBody,
    },
    func_macros::*,
    arg_list_checker::check_args_count,
};

use super::ArgChecker;

/// Represents an expression to evaluate.
#[derive(Debug, Clone)]
pub enum Expr {
    // Literals
    Val(Value),

    // Basic binary operations
    Pow(Box<ExprSpan>, Box<ExprSpan>),
    Mul(Box<ExprSpan>, Box<ExprSpan>),
    Div(Box<ExprSpan>, Box<ExprSpan>),
    Add(Box<ExprSpan>, Box<ExprSpan>),
    Sub(Box<ExprSpan>, Box<ExprSpan>),
    Mod(Box<ExprSpan>, Box<ExprSpan>),

    // Comparison operations
    Equ(Box<ExprSpan>, Box<ExprSpan>),
    Nequ(Box<ExprSpan>, Box<ExprSpan>),
    CmpLt(Box<ExprSpan>, Box<ExprSpan>),
    CmpGt(Box<ExprSpan>, Box<ExprSpan>),
    CmpLte(Box<ExprSpan>, Box<ExprSpan>),
    CmpGte(Box<ExprSpan>, Box<ExprSpan>),

    // Logical operations
    Not(Box<ExprSpan>),
    And(Box<ExprSpan>, Box<ExprSpan>),
    Or(Box<ExprSpan>, Box<ExprSpan>),

    // Unit conversion
    Conv(Box<ExprSpan>, Unit),
    RawConv(Box<ExprSpan>, Unit),

    // Arrays, dicts
    Array(Vec<ExprSpan>),
    Dict(Vec<(ExprSpan, ExprSpan)>),
    Index(Box<ExprSpan>, Box<ExprSpan>),

    // Variables
    Get(String),

    // Variable assignment
    Set(Assignable, Box<ExprSpan>),
    SetMul(Assignable, Box<ExprSpan>),
    SetDiv(Assignable, Box<ExprSpan>),
    SetAdd(Assignable, Box<ExprSpan>),
    SetSub(Assignable, Box<ExprSpan>),
    PostfixIncr(Assignable),
    PostfixDecr(Assignable),
    PrefixIncr(Assignable),
    PrefixDecr(Assignable),

    // Functions
    FuncCall(Box<ExprSpan>, Vec<ExprSpan>),
    FuncDef(Option<String>, Vec<FunctionArg>, NoDebug<Option<Rc<dyn ArgChecker>>>, Box<ExprSpan>),

    // Blocks
    ScopedBlock(Vec<ExprSpan>),
    Block(Vec<ExprSpan>),
    LeaveBlock(Vec<ExprSpan>),

    // Conditions
    If(Box<ExprSpan>, Box<ExprSpan>, Option<Box<ExprSpan>>),

    // Loops
    Loop(Box<ExprSpan>),
    While(Box<ExprSpan>, Box<ExprSpan>),
    For(Option<Assignable>, Box<ExprSpan>, Box<ExprSpan>),
    Break(Option<Box<ExprSpan>>),
    Return(Option<Box<ExprSpan>>),
    Leave(Option<Box<ExprSpan>>),
    Continue(Option<Box<ExprSpan>>),

    // Exceptions
    Throw(Option<Box<ExprSpan>>),
    TryCatch(Box<ExprSpan>, Option<(Option<String>, Box<ExprSpan>)>),
}

impl Expr {
    /// Evaluates the expression in the given context.
    pub fn eval(&self, cxt: &mut ContextRef) -> ExprResult {
        macro_rules! eval {
            ($expr:expr) => ( $expr.eval(cxt)? )
        }
        macro_rules! eval_optional {
            ($opt_expr_span:expr) => ( Self::eval_optional_expr_span(cxt, $opt_expr_span)? )
        }

        // Strategy for number multiplication and division:
        const STRATEGY: ValueMergeStrategy = ValueMergeStrategy::MergeQuantities;

        match self {
            Self::Val(v) => {
                Ok(v.clone())
            },
            Self::Pow(lhs, rhs)
                | Self::Mul(lhs, rhs) | Self::Div(lhs, rhs)
                | Self::Add(lhs, rhs) | Self::Sub(lhs, rhs)
                | Self::Mod(lhs, rhs)
                | Self::Equ(lhs, rhs) | Self::Nequ(lhs, rhs)
                | Self::CmpLt(lhs, rhs) | Self::CmpGt(lhs, rhs)
                | Self::CmpLte(lhs, rhs) | Self::CmpGte(lhs, rhs)
            => {
                let (mut lhs, rhs) = ( eval!(lhs), eval!(rhs) );

                // The following test allows to perform operations like "4 + 5.0" (integer lhs,
                // number rhs). The opposite case, "4.0 + 5" (number lhs, integer rhs) is handled
                // directly in the NumOperand struct.
                if lhs.is_int() && rhs.is_num() {
                    lhs = NumOperand::from(lhs.unwrap_int()).into_value();
                }

                match self {
                    Self::Pow(_, _)    => lhs.pow(&rhs),
                    Self::Mul(_, _)    => lhs.mul(&rhs, STRATEGY),
                    Self::Div(_, _)    => lhs.div(&rhs, STRATEGY),
                    Self::Add(_, _)    => lhs.add(&rhs),
                    Self::Sub(_, _)    => lhs.sub(&rhs),
                    Self::Mod(_, _)    => lhs.modulo(&rhs),
                    Self::Equ(_, _)    => lhs.equ(&rhs),
                    Self::Nequ(_, _)   => lhs.nequ(&rhs),
                    Self::CmpLt(_, _)  => lhs.cmp_lt(&rhs),
                    Self::CmpGt(_, _)  => lhs.cmp_gt(&rhs),
                    Self::CmpLte(_, _) => lhs.cmp_lte(&rhs),
                    Self::CmpGte(_, _) => lhs.cmp_gte(&rhs),
                    Self::Or(_, _)     => lhs.or(&rhs),
                    _ => unreachable!(),
                }.into_expr_result()
            },
            Self::And(lhs, rhs) => {
                let lhs = eval!(lhs);
                let lhs_operand = lhs.get_boolean().ok_or(err!(Expr:Operation:MismatchTypes))?;
                if lhs_operand.is_false() {
                    Ok(v_false!())
                } else {
                    lhs.and(&eval!(rhs)).into_expr_result()
                }
            },
            Self::Or(lhs, rhs) => {
                let lhs = eval!(lhs);
                let lhs_operand = lhs.get_boolean().ok_or(err!(Expr:Operation:MismatchTypes))?;
                if lhs_operand.is_true() {
                    Ok(v_true!())
                } else {
                    lhs.or(&eval!(rhs)).into_expr_result()
                }
            },
            Self::Not(v) => {
                eval!(v).not().into_expr_result()
            },
            Self::Conv(expr_span, unit) => {
                eval!(expr_span).convert_to_unit(unit).into_expr_result()
            },
            Self::RawConv(expr_span, unit) => {
                eval!(expr_span).raw_convert_to_unit(unit).into_expr_result()
            },
            Self::Array(exprs) => {
                let values = exprs.iter()
                    .map(|expr_span| expr_span.eval(cxt))
                    .collect::<Result<Vec<_>, _>>()?;
                Ok(ArrayOperand::from(values).into_value())
            },
            Self::Dict(key_value_pairs) => {
                let mut dict = DictOperand::new();
                for (key_expr_span, value_expr_span) in key_value_pairs {
                    let (key, value) = (eval!(key_expr_span), eval!(value_expr_span));
                    dict.try_insert(key, value)?;
                }
                Ok(dict.into_value())
            },
            Self::Index(val, index) => {
                eval!(val).index(&eval!(index)).into_expr_result()
            },
            Self::Get(var_name) => {
                match cxt.scope.var(var_name) {
                    Some(value) => Ok(value.borrow().clone()),
                    None => Err(err!(Expr:UndefinedVariable(var_name.clone()))),
                }
            },
            Self::Set(assignable, expr_span) => {
                let value = eval!(expr_span);
                assignable.set(cxt, value.clone())?;
                Ok(value)
            },
            Self::SetMul(assignable, rhs)
                | Self::SetDiv(assignable, rhs)
                | Self::SetSub(assignable, rhs)
                | Self::SetAdd(assignable, rhs) =>
            {
                let rhs_val = eval!(rhs);
                let new_val = assignable.get_value_mut(
                    cxt,
                    Some(rhs_val),
                    &|lhs: &mut Value, rhs| -> OperationResult {
                        let rhs = rhs.unwrap();
                        let new_val = match self {
                            Self::SetMul(_, _) => lhs.set_mul(&rhs, STRATEGY),
                            Self::SetDiv(_, _) => lhs.set_div(&rhs, STRATEGY),
                            Self::SetSub(_, _) => lhs.set_sub(&rhs),
                            Self::SetAdd(_, _) => lhs.set_add(&rhs),
                            _ => unreachable!(),
                        }?;
                        Ok(new_val)
                    }
                )??;
                Ok(new_val)
            },
            Self::PostfixIncr(assignable) => {
                Self::incr_decr_operation(cxt, assignable, |v| v.postfix_incr())
            },
            Self::PostfixDecr(assignable) => {
                Self::incr_decr_operation(cxt, assignable, |v| v.postfix_decr())
            },
            Self::PrefixIncr(assignable) => {
                Self::incr_decr_operation(cxt, assignable, |v| v.prefix_incr())
            },
            Self::PrefixDecr(assignable) => {
                Self::incr_decr_operation(cxt, assignable, |v| v.prefix_decr())
            },
            Self::FuncDef(opt_name, args, opt_ret_check, expr_span) => {
                let name = match opt_name {
                    Some(name) => name,
                    None => "[anonymous-function]",
                };
                let args = FunctionArgs::try_from(args).map_err(ExprError::Function)?;
                let func = func!(
                    (name),
                    args_eval!(args),
                    arg_list_checker!(check_args_count),
                    opt_return_checker!(opt_ret_check.0.clone()), // TODO
                    body_eval!(FunctionBody::UserDefined(
                        expr_span.as_ref().clone(),
                        cxt.scope.clone()
                    )),
                );
                let func_value = FuncOperand::new(func).into_value();
                if let Some(name) = opt_name {
                    cxt.scope.define_var(name.clone(), func_value.clone())
                        .map_err(|_| err!(Expr:VarAlreadyExists(name.clone())))?;
                };
                Ok(func_value)
            },
            Self::FuncCall(func_expr, expr_args) => {
                let value = eval!(func_expr);
                let func_operand = value.get_func().ok_or(err!(Expr:NotCallable))?;
                let func = func_operand.val_ref();

                let evaluated_args = expr_args.iter()
                    .map(|expr_span| expr_span.eval(cxt))
                    .collect::<Result<Vec<_>, _>>()?;

                func.call(cxt, evaluated_args, &func_expr.span)
                    .unwrap_or_else(|err| Err(err!(Expr:Call(err))))
            },
            Self::ScopedBlock(exprs) => {
                cxt.scope.push();

                let eval_result = Self::eval_block_sequence(cxt, exprs);

                let result = match eval_result.get_unspan_err() {
                    Some(&ExprError::Leave(_)) => Ok(unwrap_expr_error_value!(eval_result, Leave)),
                    _ => eval_result,
                };

                cxt.scope.pop();
                result
            },
            Self::Block(exprs) | Self::LeaveBlock(exprs) => {
                Self::eval_block_sequence(cxt, exprs)
            },
            Self::If(cond, if_block, else_block) => {
                let cond_value = eval!(cond);
                let cond = cond_value.get_boolean().ok_or(err!(Expr:Operation:MismatchTypes))?;

                if cond.is_true() {
                    if_block.eval(cxt)
                } else if let Some(else_block) = else_block {
                    else_block.eval(cxt)
                } else {
                    Ok(void!())
                }
            },
            Self::Loop(block) => {
                loop {
                    let result = block.eval(cxt);

                    match result.get_unspan_err() {
                        Some(&ExprError::Break(_)) => break Ok(unwrap_expr_error_value!(result, Break)),
                        Some(&ExprError::Continue(_)) => (),
                        Some(_) => break result,
                        None => (),
                    }
                }
            },
            Self::While(cond, block) => {
                let mut last_block_eval = void!();
                loop {
                    let cond_value = eval!(cond);
                    let cond = cond_value.get_boolean().ok_or(err!(Expr:Operation:MismatchTypes))?;

                    if cond.is_false() {
                        break Ok(last_block_eval);
                    }

                    let result = block.eval(cxt);

                    last_block_eval = match result.get_unspan_err() {
                        Some(&ExprError::Break(_)) => break Ok(unwrap_expr_error_value!(result, Break)),
                        Some(&ExprError::Continue(_)) => unwrap_expr_error_value!(result, Continue),
                        Some(_) => break result,
                        None => result.unwrap(),
                    }
                }
            },
            Self::For(opt_assignable, iter_expr, block) => {
                let iter = eval!(iter_expr).iterate()?;

                if let Some(assignable) = &opt_assignable {
                    // TODO: avoid double scope when the given block is already a scoped block.
                    cxt.scope.push();
                    assignable.define_as_void(cxt)?;
                }

                let mut last_block_eval = void!();
                let mut final_val = None;

                for val in iter.0 {
                    if let Some(assignable) = &opt_assignable {
                        assignable.set(cxt, val)?;
                    }

                    let result = block.eval(cxt);

                    last_block_eval = match result.get_unspan_err() {
                        Some(&ExprError::Break(_)) => {
                            final_val = Some(Ok(unwrap_expr_error_value!(result, Break)));
                            break;
                        },
                        Some(&ExprError::Continue(_)) => {
                            unwrap_expr_error_value!(result, Continue)
                        },
                        Some(_) => {
                            final_val = Some(result);
                            break;
                        },
                        None => {
                            result.unwrap()
                        },
                    }
                }

                if opt_assignable.is_some() {
                    cxt.scope.pop();
                }

                final_val.unwrap_or(Ok(last_block_eval))
            },
            Self::Break(opt_expr_span) => {
                Err(ExprError::Break(eval_optional!(opt_expr_span)))
            },
            Self::Return(opt_expr_span) => {
                Err(ExprError::Return(eval_optional!(opt_expr_span)))
            },
            Self::Leave(opt_expr_span) => {
                Err(ExprError::Leave(eval_optional!(opt_expr_span)))
            },
            Self::Continue(opt_expr_span) => {
                Err(ExprError::Continue(eval_optional!(opt_expr_span)))
            },
            Self::Throw(opt_expr_span) => {
                Err(ExprError::Throw(eval_optional!(opt_expr_span)))
            },
            Self::TryCatch(try_expr, opt_catch) => {
                let result = try_expr.eval(cxt);

                match result.get_unspan_err() {
                    Some(&ExprError::Throw(_)) => {
                        match opt_catch {
                            Some((opt_throw_varname, catch_expr)) => {
                                cxt.scope.push();
                                if let Some(throw_varname) = opt_throw_varname {
                                    let thrown_value = unwrap_expr_error_value!(result, Throw);
                                    cxt.scope.define_var(throw_varname.clone(), thrown_value).unwrap();
                                }
                                let res = catch_expr.eval(cxt);
                                cxt.scope.pop();
                                res
                            },
                            None => Ok(void!()),
                        }
                    },
                    _ => result,
                }
            },
        }
    }

    /// Helper which evaluates an optional expression and returns a void value if the option is
    /// none. Used in `eval()` for flow control expressions (`break`, `return`...).
    fn eval_optional_expr_span(cxt: &mut ContextRef, opt_expr_span: &Option<Box<ExprSpan>>) -> ExprResult {
        match opt_expr_span {
            Some(expr_span) => expr_span.eval(cxt),
            None => Ok(void!()),
        }
    }

    /// Evaluates a sequence of expressions (avoid code repetition in `Self::eval()`, for variants
    /// `Expr::ScopedBlock` and `Expr::Block`).
    fn eval_block_sequence(cxt: &mut ContextRef, exprs: &[ExprSpan]) -> ExprResult {
        exprs.iter().try_fold(void!(), |_acc, expr_span| expr_span.eval(cxt))
    }

    /// Used by the four increment/decrement unary operations in `Self::eval()`.
    fn incr_decr_operation(
        cxt: &mut ContextRef,
        assignable: &Assignable,
        oper: impl Fn(&mut Value) -> OperationResult
    ) -> ExprResult
    {
        assignable.get_value_mut(cxt, None, &|val, _| oper(val))?
            .map_err(ExprError::Operation)
    }

    pub fn needs_semicolon(&self) -> bool {
        fn is_block(expr_span: &ExprSpan) -> bool {
            matches!(expr_span.expr, Expr::ScopedBlock(_) | Expr::Block(_) | Expr::LeaveBlock(_))
        }

        match self {
            Expr::ScopedBlock(_) | Expr::Block(_) | Expr::LeaveBlock(_) => false,
            Expr::If(_, cond, opt_else) => {
                let last_code = opt_else.as_ref().unwrap_or(cond);
                !is_block(last_code)
            }
            Expr::FuncDef(_, _, _, code) | Expr::Loop(code)
            | Expr::While(_, code) | Expr::For(_, _, code) => {
                !is_block(code)
            }
            Expr::TryCatch(try_code, opt_catch) => {
                let last_code = opt_catch
                    .as_ref()
                    .map(|(_, catch_code)| catch_code)
                    .unwrap_or(try_code);
                !is_block(last_code)
            }
            _ => true
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::util::test_common::*;

    macro_rules! t_expr_number {
        ( $( $tt:tt )* ) => (
            Box::new(expr_span!(Val(number!($( $tt )*))))
        )
    }
    macro_rules! t_expr_string {
        ( $s:expr ) => (
            Box::new(expr_span!(Val(string!($s))))
        )
    }
    macro_rules! t_expr_bool {
        ( $v:expr ) => (
            Box::new(expr_span!(Val(boolean!($v))))
        )
    }

    macro_rules! t_oper {
        ($cxt:expr, $oper:ident, ($($val1:tt)*), ($($val2:tt)*)) => {{
            let expr_span = expr_span!($oper(
                t_expr_number!($cxt.container, $($val1)*),
                t_expr_number!($cxt.container, $($val2)*),
            ));
            expr_span.eval(&mut $cxt).unwrap()
        }
    }}

    macro_rules! t_oper_wrapped {
        ($cxt:expr, $oper:ident, ($($val1:tt)*), ($($val2:tt)*)) => {{
            let expr_span = expr_span!($oper(
                t_expr_number!($cxt.container, $($val1)*),
                t_expr_number!($cxt.container, $($val2)*),
            ));
            expr_span.eval(&mut $cxt)
        }
    }}

    #[test]
    fn test_mul() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Mul, (2 km), (4 km));
            assert_eq!(val, number!(cxt.container, 8 km2));
        })
    }

    #[test]
    fn test_div() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Div, (6 km), (2 s));
            assert_eq!(val, number!(cxt.container, 3 km/s));
        })
    }

    #[test]
    fn test_add() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Add, (2 km), (4 km));
            assert_eq!(val, number!(cxt.container, 6 km));
        })
    }

    #[test]
    fn test_mismatch_add() {
        with_test_context!(cxt, {
            let err = t_oper_wrapped!(cxt, Add, (2 km), (4 s));
            let err = unspan_err!(err);
            assert_eq!(err, Err(err!(Expr:Operation:MismatchUnits)));
        })
    }

    #[test]
    fn test_sub() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Sub, (2 km), (4 km));
            assert_eq!(val, number!(cxt.container, -2 km));
        })
    }

    #[test]
    fn test_mismatch_sub() {
        with_test_context!(cxt, {
            let err = t_oper_wrapped!(cxt, Sub, (2 km), (4 s));
            let err = unspan_err!(err);
            assert_eq!(err, Err(err!(Expr:Operation:MismatchUnits)));
        })
    }

    #[test]
    fn test_pow() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Pow, (2 km), (4));
            assert_eq!(val, number!(cxt.container, 16 km:4));
        })
    }

    #[test]
    fn test_mismatch_pow() {
        with_test_context!(cxt, {
            let err = t_oper_wrapped!(cxt, Pow, (2 km), (4 km));
            let err = unspan_err!(err);
            assert_eq!(err, Err(err!(Expr:Operation:MismatchUnits)));
        })
    }

    #[test]
    fn test_modulo() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Mod, (21 km), (4000 m));
            assert_eq!(val, number!(cxt.container, 1 km));
        })
    }

    #[test]
    fn test_float() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                n = 1.0;
                res = (n += 2.5);
            "#);
            assert_eq!(eval!(cxt, "n"), number!(cxt.container, 3.5));
            assert_eq!(eval!(cxt, "res"), number!(cxt.container, 3.5));

            let res = eval_wrapped!(cxt, "2km + 3h");
            assert!(res.is_err());

            let res = eval_wrapped!(cxt, "n = 2km; n += 3h;");
            assert!(res.is_err());
        })
    }

    #[test]
    fn test_equ() {
        with_test_context!(cxt, {
            let result = t_oper!(cxt, Equ, (2 km), (4 km));
            assert_eq!(result, boolean!(false));

            let result = t_oper!(cxt, Equ, (2 km), (2000 m));
            assert_eq!(result, boolean!(true));

            let expr_span = expr_span!(Equ(t_expr_bool!(true), t_expr_bool!(true)));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, boolean!(true));

            let expr_span = expr_span!(Equ(t_expr_string!("foo"), t_expr_string!("foo")));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, boolean!(true));

            let expr_span = expr_span!(Equ(t_expr_string!("foo"), t_expr_string!("bar")));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, boolean!(false));
        })
    }

    #[test]
    fn test_equ_errors() {
        with_test_context!(cxt, {
            let val = t_oper!(cxt, Equ, (2 s), (4 km));
            assert_eq!(val, v_false!());

            let expr_span = expr_span!(Equ(
                t_expr_number!(cxt.container, 2 km),
                t_expr_string!("foo"),
            ));
            let val = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(val, v_false!());
        })
    }

    #[test]
    fn test_nequ() {
        with_test_context!(cxt, {
            // TODO: give context directly to macros
            let result = t_oper!(cxt, Nequ, (2 km), (4 km));
            assert_eq!(result, boolean!(true));

            let expr_span = expr_span!(Nequ(t_expr_string!("foo"), t_expr_string!("bar")));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, boolean!(true));
        })
    }

    #[test]
    fn test_value_conversion() {
        with_test_context!(cxt, {
            let expr_span = expr_span!(Conv(
                t_expr_number!(cxt.container, 2 km),
                unit!(cxt.container, m)
            ));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, number!(cxt.container, 2000 m));

            let expr_span = expr_span!(Conv(
                t_expr_number!(cxt.container, 180 km/h),
                unit!(cxt.container, m/s)
            ));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert!(result.equal(&number!(cxt.container, 50 m/s)));
        })
    }

    #[test]
    fn test_use_variable() {
        with_test_context!(cxt, {
            let expr_span = expr_span!(Set(
                Assignable::new(AssignMode::Let, AssignTarget::Variable("foo".to_string())),
                t_expr_number!(cxt.container, 10 km)
            ));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, number!(cxt.container, 10 km));
            assert!(cxt.scope.has_var("foo"));
            assert_eq!(var_value!(cxt.scope, "foo"), number!(cxt.container, 10 km));
        })
    }

    #[test]
    fn test_undefined_variable() {
        with_test_context!(cxt, {
            let expr_span = expr_span!(Get("foo".to_string()));
            let result = expr_span.eval(&mut cxt);
            let err = unspan_err!(result);
            assert_eq!(err, Err(err!(Expr:UndefinedVariable(s!("foo")))));
        })
    }

    #[test]
    fn test_overwrite_variable() {
        with_test_context!(cxt, {
            let expr_span = expr_span!(Set(
                Assignable::new(AssignMode::Let, AssignTarget::Variable("foo".to_string())),
                t_expr_number!(cxt.container, 10 km)
            ));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, number!(cxt.container, 10 km));
            assert!(cxt.scope.has_var("foo"));
            assert_eq!(var_value!(cxt.scope, "foo"), number!(cxt.container, 10 km));
        });
        with_test_context!(cxt, {
            let expr_span = expr_span!(Set(
                    Assignable::new(AssignMode::Let, AssignTarget::Variable("foo".to_string())),
                t_expr_number!(cxt.container, 20 km)
            ));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, number!(cxt.container, 20 km));
            assert!(cxt.scope.has_var("foo"));
            assert_eq!(var_value!(cxt.scope, "foo"), number!(cxt.container, 20 km));
        })
    }

    #[test]
    fn test_assign_inexistent_global_var() {
        with_test_context!(cxt, {
            let expr_span = expr_span!(Set(
                Assignable::new(AssignMode::Set, AssignTarget::Variable("n".to_string())),
                t_expr_number!(cxt.container, 78)
            ));
            let result = expr_span.eval(&mut cxt);
            assert_eq!(result, Ok(number!(cxt.container, 78)));
            assert!(cxt.scope.has_var("n"));
        });
    }

    #[test]
    fn test_assign_inexistent_local_var() {
        with_test_context!(cxt, {
            let result = eval_err!(cxt, r#"
                fn foo() {
                    newvar = 78;
                };
                foo()
            "#);
            assert_eq!(result, err!(Expr:UndefinedVariable(s!("newvar"))));
        });
    }

    #[test]
    fn test_assign_oper_variable() {
        with_test_context!(cxt, {
            eval!(cxt, "let foo = 10km");
            let expr_span = expr_span!(SetMul(
                Assignable::new(AssignMode::Set, AssignTarget::Variable("foo".to_string())),
                t_expr_number!(cxt.container, 2 km)
            ));
            let result = expr_span.eval(&mut cxt).unwrap();
            assert_eq!(result, number!(cxt.container, 20 km2));
            assert_eq!(var_value!(cxt.scope, "foo"), number!(cxt.container, 20 km2));
        });
    }

    #[test]
    fn test_assign_to_pattern() {
        with_test_context!(cxt, {
            macro_rules! test {
                ($expr:expr, $val_a:expr, $val_b:expr) => {
                    eval!(cxt, $expr);
                    assert_eq!(eval!(cxt, "a"), int!($val_a));
                    assert_eq!(eval!(cxt, "b"), int!($val_b));
                }
            }

            test!("let [a, b] = [10, 20]", 10, 20);
            test!("[a, b] = [40, 50]", 40, 50);
            test!("[a, b] = [b, a]", 50, 40);
            test!("[a, b] *= [10, 20]", 500, 800);
            test!("[a, b]++", 501, 801);
            test!("++[a, b]", 502, 802);

            eval!(cxt, r#"
                let d = { a: 1, b: 2, c: 3};
                let [keys, sum] = ["", 0];
                for [k, v] in d {
                    [keys, sum] += [k, v];
                };
            "#);
            let keys = eval!(cxt, "keys");
            let keys = keys.unwrap_str().val_ref();
            assert!(keys.contains('a') && keys.contains('b') && keys.contains('c'));
            assert_eq!(eval!(cxt, "sum"), int!(6));

            test!("[a, [b, c]] = [1, [2, 3]]", 1, 2);
            assert_eq!(eval!(cxt, "c"), int!(3));

            let r = eval_err!(cxt, "[a, b] = 6");
            assert_eq!(r, err!(Expr:UnableToDestructure));
        });
    }

    #[test]
    fn test_assign_to_dict_pattern() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let person = {
                    name: "Jules",
                    lastname: "BÉVOT",
                    hobbies: [ "sport", "reading" ],
                };

                let [ { name, lastname, hobbies as [hobby1, hobby2, hobby3] } ] = [ person ];
            "#);

            assert_eq!(eval!(cxt, "name"), string!("Jules"));
            assert_eq!(eval!(cxt, "lastname"), string!("BÉVOT"));
            assert_eq!(eval!(cxt, "hobby1"), string!("sport"));
            assert_eq!(eval!(cxt, "hobby2"), string!("reading"));
            assert_eq!(eval_err!(cxt, "hobby3"), err!(Expr:UndefinedVariable(s!("hobby3"))));

            assert_eq!(
                eval_err!(cxt, "let { foo } = person"),
                err!(Expr:Dict:DictKeyNotFound(HashableValue::from("foo")))
            );

            assert_eq!(
                eval_err!(cxt, "let { hobbies, hobbies } = person"),
                err!(Expr:VarAlreadyExists(s!("hobbies")))
            );
        });
    }

    // TODO: int or bool keys in destructuring patterns (anything that is a hashable value)
    #[ignore]
    #[test]
    fn test_assign_to_dict_pattern2() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let t = {
                    11: 11km,
                    17: 17km,
                };

                let { 11 as eleven, 17 as seventeen } = t;
            "#);

            assert_eq!(eval!(cxt, "eleven"), number!(cxt.container, 11 km));
            assert_eq!(eval!(cxt, "seventeen"), number!(cxt.container, 17 km));
        });
    }

    #[test]
    fn test_void_value() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "nop()");
            assert_eq!(r, void!());

            let r = eval_err!(cxt, "nop() + nop()");
            assert_eq!(r, err!(Expr:Operation:Unavailable));
        })
    }

    #[test]
    fn test_func_def_with_same_arg_names() {
        with_test_context!(cxt, {
            let r = eval_err!(cxt, "fn(y, y) y");
            assert_eq!(r, err!(Expr:Function:ArgNamesAreNotUnique));
        })
    }

    #[test]
    fn test_if_expression() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let f = fn(n) {
                    if n == 3 {
                        "foo"
                    } else {
                        "bar"
                    }
                }
            "#);
            let r = eval!(cxt, "f(3)");
            assert_eq!(r, string!("foo"));
            let r = eval!(cxt, "f(5)");
            assert_eq!(r, string!("bar"));
        })
    }

    #[test]
    fn test_bad_if_expression() {
        with_test_context!(cxt, {
            let err = eval_err!(cxt, "if 3 + 4 {}");
            assert_eq!(err, err!(Expr:Operation:MismatchTypes));
        })
    }

    #[test]
    fn test_if_without_curly_brackets() {
        with_test_context!(cxt, {
            let v = eval!(cxt, "if (1 + 1 == 2) 78");
            assert_eq!(v, int!(78));
        })
    }

    #[test]
    fn test_loop_expression() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let f = fn(n) {
                    let initial_n = n;
                    loop {
                        n = n - 1;
                        if n == 0 { break initial_n }
                    }
                }
            "#);
            let r = eval!(cxt, "f(3)");
            assert_eq!(r, int!(3));
        })
    }

    #[test]
    fn test_loop_without_curly_brackets() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let n = 3;
                loop if (n-- == 0) break 78
            "#);
            assert_eq!(v, int!(78));
        })
    }

    #[test]
    fn test_while_loop() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let n = 3;
                let v = 1;
                while n-- != 0 { v *= 2 };
                v
            "#);
            assert_eq!(v, int!(8));
        })
    }

    #[test]
    fn test_continue_statement() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let n = 5; let s = "";
                while n-- != 0 {
                    s += n;
                    if (n == 0) continue;
                    s += ",";
                };
                s
            "#);
            assert_eq!(v, string!("4,3,2,1,0"));
        })
    }

    #[test]
    fn test_while_loop_return() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let n = 3;
                let v = while n-- != 0 {
                    if n == 2 { break "hi" }
                };
                v
            "#);
            assert_eq!(v, string!("hi"));
        });

        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let n = 3;
                let v = while n-- != 0 {
                    if n == 7 { break "hi" }
                };
                v
            "#);
            assert_eq!(v, void!());
        })
    }

    #[test]
    fn test_for_loop() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let sum = 0;
                for n in [1, 2, 3] {
                    sum += n
                }
            "#);
            assert_eq!(v, int!(6));

            let v = eval!(cxt, r#"
                sum = 0;
                for n in [1, 2, 3] {
                    sum += n;
                    if n == 2 {
                        break sum
                    }
                }
            "#);
            assert_eq!(v, int!(3));

            let v = eval!(cxt, r#"
                sum = 0;
                let n = 78;
                for n in [1, 2, 3] { sum += n };
                [ n, sum ]
            "#);
            assert_eq!(v, array![
                int!(78),
                int!(6),
            ]);

            let v = eval!(cxt, r#"
                sum = 0;
                for n in 10 { sum += n }
            "#);
            assert_eq!(v, int!(45));
        })
    }

    #[test]
    fn test_anonymous_for_loop() {
        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let count = 0;
                for [1, 2, 3] {
                    ++count
                }
            "#);
            assert_eq!(v, int!(3));
        });

        with_test_context!(cxt, {
            let v = eval!(cxt, r#"
                let count = 0;
                for 3 { ++count }
            "#);
            assert_eq!(v, int!(3));
        });
    }

    #[test]
    fn test_leave_statement() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let n = {
                    leave 40;
                    20
                };
                let f = fn(n) {
                    let v = {
                        if (n < 0) { leave 78; }
                        n * 2
                    };
                    v
                };
                let f2 = fn(n) {
                    let v = {
                        if (n < 0) { leave 78 }
                        n * 2
                    };
                    v
                };
                let f3 = fn(n) {
                    let v = {
                        if (n < 0) {
                            print("test");
                            leave 78;
                        }
                        n * 2
                    };
                    v
                };
            "#);

            assert_eq!(eval!(cxt, "n"), int!(40));
            assert_eq!(eval!(cxt, "f(4)"), int!(8));
            assert_eq!(eval!(cxt, "f2(4)"), int!(8));
            assert_eq!(eval!(cxt, "f3(4)"), int!(8));
            assert_eq!(eval!(cxt, "f(-4)"), int!(78));
            assert_eq!(eval!(cxt, "f2(-4)"), int!(78));
            assert_eq!(eval!(cxt, "f3(-4)"), int!(78));

            assert!(eval_wrapped!(cxt, "leave 10;").is_err());
            assert!(eval_wrapped!(cxt, "{ leave 10; }").is_err());
            assert_eq!(eval!(cxt, "{{ leave 10; }}"), int!(10));
        })
    }

    #[test]
    fn test_throw_statement() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let f = fn(n: int) {
                    if (n < 0) throw "negative";
                    n * 2
                }
            "#);
            assert_eq!(eval!(cxt, "f(4)"), int!(8));
            assert_eq!(
                eval_err!(cxt, "f(-4)"),
                err!(Expr:Throw(string!("negative")))
            );
        })
    }

    #[test]
    fn test_try_catch_statement() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let f = fn(n) {
                    if (n < 0) throw "negative";
                    n * 2
                };
                let g = fn(n) {
                    try {
                        f(n)
                    }
                    catch e {
                        "sorry: " + e
                    }
                }
            "#);
            assert_eq!(eval!(cxt, "g(4)"), int!(8));
            assert_eq!(eval!(cxt, "g(-4)"), string!("sorry: negative"));

            // Test condensed version:
            eval!(cxt, r#"  g = fn(n) try f(n) catch e "sorry: " + e  "#);
            assert_eq!(eval!(cxt, "g(4)"), int!(8));
            assert_eq!(eval!(cxt, "g(-4)"), string!("sorry: negative"));

            // Test without catch variable:
            eval!(cxt, r#"
                g = fn(n) {
                    try f(n)
                    catch "error"
                }
            "#);
            assert_eq!(eval!(cxt, "g(-4)"),string!("error"));

            // Test without catch block:
            eval!(cxt, r#"
                g = fn(n) {
                    try f(n)
                }
            "#);
            assert_eq!(eval!(cxt, "g(4)"), int!(8));
            assert_eq!(eval!(cxt, "g(-4)"), void!());
        })
    }

    #[test]
    fn test_arrays() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let a = [ 1, 2, "three" ]
            "#);
            assert_eq!(eval!(cxt, "a[1]"), int!(2));
            assert_eq!(eval_err!(cxt, "a[-4]"), err!(Expr:Operation:IndexOutOfRange));
        })
    }

    #[test]
    fn test_array_assign() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                a = [ 1, 2, "three" ];
                a[1] = "two";
            "#);
            assert_eq!(eval!(cxt, "a"), array![int!(1), string!("two"), string!("three")]);

            eval!(cxt, r#"
                a = [ 1, [ 10, 20, 30 ], "three" ];
                a[1][2] = "foo";
            "#);
            assert_eq!(eval!(cxt, "a"), array![
                int!(1),
                array![int!(10), int!(20), string!("foo")],
                string!("three"),
            ]);
        })
    }

    #[test]
    fn test_array_add_element() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                a = [ 1, 2 ];
                a = a + 3;
            "#);
            assert_eq!(eval!(cxt, "a"), array![int!(1), int!(2), int!(3)]);
        });

        with_test_context!(cxt, {
            eval!(cxt, r#"
                a = [ 1, 2 ];
                a += 3;
            "#);
            assert_eq!(eval!(cxt, "a"), array![int!(1), int!(2), int!(3)]);
        });
    }

    #[test]
    fn test_array_expr_assign() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                fn longest(a: array, b: array) if len(a) > len(b) a else b;
                a = [1, 2];
                b = [10, 20, 30, 40];
                longest(a, b)[1] = "foo";
            "#);
            assert_eq!(eval!(cxt, "a"), array![int!(1), int!(2)]);
            assert_eq!(
                eval!(cxt, "b"),
                array![int!(10), string!("foo"), int!(30), int!(40)]
            );
        });

        with_test_context!(cxt, {
            eval!(cxt, r#"
                a = [1, 2];
                b = [10, 20, 30, 40];
                (if len(a) > len(b) a else b)[1] = "foo";
            "#);
            assert_eq!(eval!(cxt, "a"), array![int!(1), int!(2)]);
            assert_eq!(
                eval!(cxt, "b"),
                array![int!(10), string!("foo"), int!(30), int!(40)]
            );
        });
    }

    #[test]
    fn test_nested_arrays() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                let a = [ 1, 2, [ "two", "three" ] ]
            "#);
            assert_eq!(eval!(cxt, "a[2][1]"), string!("three"));
        })
    }

    #[test]
    fn test_nested_array_assign() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                a = [ 1, 2, 3 ];
                b = [ 10, 20, a, 40 ];
                a[1] = "foo";
            "#);
            assert_eq!(
                eval!(cxt, "b"),
                array![
                    int!(10),
                    int!(20),
                    array![int!(1), string!("foo"), int!(3)],
                    int!(40),
                ]
            );
        })
    }

    #[test]
    fn test_dicts() {
        with_test_context!(cxt, {
            eval!(cxt, r#" d = { "foo": 10, "bar": [1, 2, 3] }; "#);
            assert_eq!(eval!(cxt, "d"), dict!{
                string!("foo") => int!(10),
                string!("bar") => array![int!(1), int!(2), int!(3)],
            });
            assert_eq!(eval!(cxt, r#" d["foo"] "#), int!(10));
            assert_eq!(
                eval_err!(cxt, r#" d["z"] "#),
                err!(Expr:Operation:Dict:DictKeyNotFound(HashableValue::Str(StrOperand::from("z"))))
            );
            assert_eq!(eval_err!(cxt, r#" d[12.3] "#), err!(Expr:Operation:Dict:InvalidDictIndex));
        });
    }

    #[test]
    fn test_nested_dicts() {
        with_test_context!(cxt, {
            eval!(cxt, r#" d = { "foo": 10, "bar": { 10: "ten", 20: "twenty" } }; "#);
            assert_eq!(eval!(cxt, "d"), dict!{
                string!("foo") => int!(10),
                string!("bar") => dict!{
                    int!(10) => string!("ten"),
                    int!(20) => string!("twenty"),
                },
            });
        });
    }

    #[test]
    fn test_dicts_assign() {
        with_test_context!(cxt, {
            eval!(cxt, r#" d = { "foo": 10, "bar": [1, 2, 3] }; "#);
            eval!(cxt, r#" d["foo"] = 10; d[12] = 120; "#);
            assert_eq!(eval!(cxt, r#" d["foo"] "#), int!(10));
            assert_eq!(eval!(cxt, r#" d[12] "#), int!(120));
        });
    }

    #[test]
    fn test_dicts_key_as_varname() {
        with_test_context!(cxt, {
            eval!(cxt, r#" foo = 10; d = { foo, "bar": [1, 2, 3] }; "#);
            assert_eq!(eval!(cxt, r#" d.foo "#), int!(10));
        });
    }

    #[test]
    fn test_not_operator() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "!true"), v_false!());
            assert_eq!(eval!(cxt, "!false"), v_true!());
            eval!(cxt, "fn foo() true;");
            assert_eq!(eval!(cxt, "!foo()"), v_false!());
            assert_eq!(eval!(cxt, "!!foo()"), v_true!());
            assert_eq!(eval!(cxt, "!!!foo()"), v_false!());
            assert_eq!(eval!(cxt, "!!!!foo()"), v_true!());
        });
    }
}

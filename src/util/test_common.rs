//! Useful helper macros used in tests.

// Import everything:
pub use crate::value::*;
pub use crate::context::*;
pub use crate::eval::*;
pub use crate::parse::*;
pub use crate::util::*;
pub use crate::library::*;

#[macro_export]
macro_rules! with_test_context {
    ( $cxt:ident, $( $tt:tt )* ) => {{
        let (mut container, mut scope) = $crate::library::create_prefilled_context();
        #[allow(unused_mut, unused_variables)]
        let mut $cxt = $crate::context::ContextRef::new(&mut container, &mut scope);
        $( $tt )*
    }}
}

#[macro_export]
macro_rules! with_test_container {
    ( $container:ident, $( $tt:tt )* ) => {{
        with_test_context!(cxt, {
            #[allow(unused_mut, unused_variables)]
            let mut $container = cxt.container;
            $( $tt )*
        })
    }}
}

#[macro_export]
macro_rules! with_test_scope {
    ( $scope:ident, $( $tt:tt )* ) => {{
        with_test_context!(cxt, {
            #[allow(unused_mut, unused_variables)]
            let mut $scope = cxt.scope;
            $( $tt )*
        })
    }}
}

/// Parses the given string (only used for tests)
#[macro_export]
macro_rules! parse {
    ( $container:expr, $expr:expr ) => {{
        use $crate::parse::parse_expr;
        parse_expr($expr, &$container, None).unwrap()
    }}
}

/// Parses the given string without unwrapping (only used for tests)
#[macro_export]
macro_rules! parse_wrapped {
    ( $container:expr, $expr:expr ) => ( parse_expr($expr, &$container, None) )
}

/// By assuming that the given expression will raise an error, evaluates this expression and
/// unwraps the returned error. If the error is wrapped into `ExprError::Spanned(...)`, then
/// unwraps the inner error too.
#[macro_export]
macro_rules! eval_err {
    ( $cxt:ident, $expr: expr ) => {{
        let r = eval_wrapped!($cxt, $expr);
        assert!(r.is_err());
        r.unwrap_err().unspan()
    }}
}

/// Unwraps and (possibly) unspan the given error.
#[macro_export]
macro_rules! unspan_err {
    ( $err:expr ) => {{
        let err = $err.unwrap_err().unspan();
        Result::<Value, _>::Err(err)
    }}
}

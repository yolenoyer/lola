//! Functions and macros around [`rug::Float`] numbers.

use std::{fmt, num::FpCategory};

use rug::Float;

use crate::value::FormatConfig;

#[macro_export]
#[doc(hidden)]
macro_rules! float_parsed {
    ($($n:tt)*) => (
        float!(
            rug::Float::parse(stringify!($($n)*)).unwrap()
        )
    );
}

#[macro_export]
#[doc(hidden)]
macro_rules! float {
    () => (Float::new(crate::numbers::FLOAT_PREC));
    ($n:expr) => (
        rug::Float::with_val_round(
            crate::numbers::FLOAT_PREC,
            $n,
            rug::float::Round::Up
        ).0
    );
}

/// The float precision in bits for all float numbers.
/// If [`FLOAT_PREC`] is modified, then [`FLOAT_STR_PREC`] has to be modified consequently.
pub const FLOAT_PREC: u32 = 128;

/// The corresponding decimal precision on display. It is equal to something like that:
///
/// `FLOAT_STR_PREC = log10(2 ^ FLOAT_PREC) + 1`
///
/// The `+ 1` above has not been very well tested.
pub const FLOAT_STR_PREC: usize = 40;

/// In order to have a friendly display, floats which have been converted to strings are cut on the
/// right by a few digits; this allows to remove the float conversion error, and then to be able
/// to trim trailing zeroes.
/// This value indicates how much the string has to be cut before trimming zeroes.
pub const FLOAT_STR_TRIM_RIGHT: usize = 6;

/// When comparing float numbers, gives the delta of approximation for tolerance.
pub const FLOAT_APPROX_DELTA: f64 = 0.00000000000000000000000000001;

/// Tests if two numbers are approximatively equal.
///
/// This function checks if the difference between the two values is less than the constant
/// [`FLOAT_APPROX_DELTA`].
pub fn approx_eq(n1: &Float, n2: &Float) -> bool {
    let delta = Float::with_val(FLOAT_PREC, n1 - n2).abs();
    delta < FLOAT_APPROX_DELTA
}

/// Formats a float number, by taking into account the given [`FormatConfig`] if present.
///
/// If no config is given, use a default format:
///  * Full precision (actually 40 decimal digits);
///  * No Rust-like underscores.
pub fn format_number(n: &Float, format_config: Option<&FormatConfig>) -> String {
    const EXP_MODE_LIMIT: i32 = 9;

    const DEBUG: bool = false;
    macro_rules! debug {
        ( $($tt:tt)* ) => {
            if DEBUG { println!($($tt)*); }
        }
    }

    match n.classify() {
        FpCategory::Nan => return "(NaN)".to_string(),
        FpCategory::Infinite => return "(inf)".to_string(),
        FpCategory::Zero => return "0".to_string(),
        FpCategory::Subnormal => unreachable!(),
        FpCategory::Normal => (),
    }

    let (sign, mut s, exp) = n.to_sign_string_exp_round(10, None, rug::float::Round::Down);
    let exp = exp.unwrap();

    let first_ending_non_zero = s.rfind('0').map(|i| i + 1).unwrap_or(0);
    let nb_ending_non_zeroes = s.len() - first_ending_non_zero;
    if nb_ending_non_zeroes > 0 && nb_ending_non_zeroes < FLOAT_STR_TRIM_RIGHT {
        s.replace_range(first_ending_non_zero.., &"0".repeat(nb_ending_non_zeroes));
        debug!("Replace ending non zeroes: {:?}", s);
    }

    debug!("float = {:?}", n);
    debug!("sign = {:?}, s = {:?}, exp = {:?}", sign, s, exp);

    let nb_significant_digits = s.rfind(|c| c != '0')
        .map(|i| i + 1)
        .unwrap_or_else(|| s.len()) as i32;
    debug!("Nb significant digits: {:?}", nb_significant_digits);

    let mut nb_int_digits = exp.max(0);
    let mut nb_dec_digits = (nb_significant_digits - exp).max(0);
    debug!("Nb digits: int = {}, dec = {}", nb_int_digits, nb_dec_digits);

    let exp_mode = exp > EXP_MODE_LIMIT || exp <= -EXP_MODE_LIMIT;
    debug!("Exponent mode: {:?}", exp_mode);

    if exp_mode {
        nb_int_digits = 1;
        nb_dec_digits = nb_significant_digits - 1;
        debug!("Corrected nb digits (exp mode): int = {}, dec = {}", nb_int_digits, nb_dec_digits);
    }

    debug!("Check for truncation");
    let mut ellipsis = "";
    if let Some(config) = format_config {
        if let Some(conf_significant_digits) = config.significant_digits {
            if nb_dec_digits > 1 && nb_significant_digits > conf_significant_digits {
                let nb_trunc_dec_digits = (conf_significant_digits - nb_int_digits).max(0);
                if nb_dec_digits > 1 && nb_trunc_dec_digits < nb_dec_digits {
                    ellipsis = &config.ellipsis;
                    nb_dec_digits = nb_trunc_dec_digits;
                    debug!("Corrected dec digits: {}", nb_dec_digits);
                }
            }
        }
    }

    let mut nb_leading_dec_zeroes = 0;
    let mut nb_dec_non_zeroes = nb_dec_digits;
    if !exp_mode && nb_int_digits == 0 {
        nb_leading_dec_zeroes = (-exp).max(0).min(nb_dec_digits);
        nb_dec_non_zeroes = nb_dec_digits - nb_leading_dec_zeroes;
    }

    let underscores = format_config.and_then(|config| config.underscores);

    let nb_int_digits = nb_int_digits as usize;
    let nb_dec_digits = nb_dec_digits as usize;
    let nb_leading_dec_zeroes = nb_leading_dec_zeroes as usize;
    let nb_dec_non_zeroes = nb_dec_non_zeroes as usize;

    let int = if nb_int_digits == 0 {
        "0"
    } else {
        &s[0..nb_int_digits]
    };

    let (leading_dec_zeroes, dec) = if exp_mode {
        (
            "".to_string(),
            s[nb_int_digits..nb_int_digits + nb_dec_digits].to_string(),
        )
    } else {
        (
            "0".repeat(nb_leading_dec_zeroes),
            s[nb_int_digits..nb_int_digits + nb_dec_non_zeroes].to_string(),
        )
    };

    let mut output = String::new();

    if sign {
        output.push('-');
    }

    if let Some(block_size) = underscores {
        output.push_str(&add_underscores_from_right(int, block_size));
    } else {
        output.push_str(int);
    }

    if !dec.is_empty() {
        output.push('.');
        let dec = format!("{}{}", leading_dec_zeroes, dec);
        if let Some(block_size) = underscores {
            output.push_str(&add_underscores_from_left(&dec, block_size));
        } else {
            output.push_str(&dec);
        }
    }

    output.push_str(ellipsis);

    if exp_mode && exp != 0 {
        output.push('e');
        output.push_str(&(exp - 1).to_string());
    }

    output
}

/// Transforms a string like "12345678" to "12_345_678". Assumes that the string is ASCII only, no
/// UTF-8 character.
fn add_underscores_from_right(input: &str, block_size: usize) -> String {
    if block_size == 0 {
        return input.to_string();
    }

    let len = input.len();

    let nb_complete_blocks = (len as isize - 1).max(0) as usize / block_size;
    let first_block_len = len - nb_complete_blocks * block_size;

    let capacity = first_block_len + nb_complete_blocks * (block_size + 1);
    let mut output = String::with_capacity(capacity);

    output.push_str(&input[..first_block_len]);

    for i in 0..nb_complete_blocks {
        output.push('_');
        let start = first_block_len + i * block_size;
        output.push_str(&input[start..start + block_size]);
    }

    output
}

/// Transforms a string like "12345678" to "123_456_78". Assumes that the string is ASCII only, no
/// UTF-8 character.
fn add_underscores_from_left(input: &str, block_size: usize) -> String {
    if block_size == 0 {
        return input.to_string();
    }

    let len = input.len();
    let nb_complete_blocks = (len as isize - 1).max(0) as usize / block_size;
    let last_block_len = len - nb_complete_blocks * block_size;

    let capacity = last_block_len + nb_complete_blocks * (block_size + 1);
    let mut output = String::with_capacity(capacity);

    for i in 0..nb_complete_blocks {
        output.push_str(&input[i * block_size..(i + 1) * block_size]);
        output.push('_');
    }

    output.push_str(&input[nb_complete_blocks * block_size..]);

    output
}

/// Erros which can occurs when trying to convert a [`Float`] to a native rust integer.
///
/// [`Float`]: rug::Float
#[derive(Debug, PartialEq)]
pub enum IntConversionError {
    NotAPositiveNumber,
    NotAnInteger,
    NotANormalNumber,
    FailToParse,
}

impl fmt::Display for IntConversionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            IntConversionError::NotAPositiveNumber => "Not a positive integer",
            IntConversionError::NotAnInteger       => "Not an integer",
            IntConversionError::NotANormalNumber   => "Not a normal number (or zero)",
            IntConversionError::FailToParse        => "Failed to parse an integer",
        };
        write!(f, "{}", msg)
    }
}

/// Tries to convert a float to an unsigned integer.
pub fn get_u32(num: &Float) -> Result<u32, IntConversionError> {
    let n = get_i32(num)?;
    if n < 0 {
        Err(IntConversionError::NotAPositiveNumber)
    } else {
        Ok(n as u32)
    }
}

/// Tries to convert a float to a signed integer.
pub fn get_i32(num: &Float) -> Result<i32, IntConversionError> {
    if !num.is_integer() {
        return Err(IntConversionError::NotAnInteger);
    }
    if !num.is_zero() && !num.is_normal() {
        return Err(IntConversionError::NotANormalNumber);
    }
    let n = num.to_i32_saturating().ok_or(IntConversionError::NotAnInteger)?;
    Ok(n)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn format_a_number() {
        let f = float_parsed!(3);
        assert_eq!(format_number(&f, None), "3");
        let f = float_parsed!(2.25);
        assert_eq!(format_number(&f, None), "2.25");
        let f = float_parsed!(2.2);
        assert_eq!(format_number(&f, None), "2.2");
    }
}

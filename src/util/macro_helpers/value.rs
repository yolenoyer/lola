//! Helper macros which can create [`Value`] and [`Operand`] instances on the fly.
//!
//! [`Value`]: crate::value::Value
//! [`Operand`]: crate::value::Operand

/// Creates a custom [`Unit`] instance.
/// Some unit have aliases, but otherwise the syntax is:
///
/// `unit!(container, [ UNIT_NAME1:EXPONENT1, UNIT_NAME2:EXPONENT2, ... ])`
///
/// E.g: `unit!(1 m:1, s:-1)`
///
/// Thanks to the aliases, the same unit can be expressed more easily: `unit!(container, 1 m/s)`.
///
/// [`Unit`]: crate::value::Unit
#[macro_export]
#[doc(hidden)]
macro_rules! unit {
    // Aliases:
    ( $container:expr, km )   => { unit!($container, km:1) };
    ( $container:expr, km2 )  => { unit!($container, km:2) };
    ( $container:expr, m )    => { unit!($container, m:1) };
    ( $container:expr, m2 )   => { unit!($container, m:2) };
    ( $container:expr, h )    => { unit!($container, h:1) };
    ( $container:expr, s )    => { unit!($container, s:1) };
    ( $container:expr, km/h ) => { unit!($container, km:1, h:-1) };
    ( $container:expr, km/s ) => { unit!($container, km:1, s:-1) };
    ( $container:expr, m.s )  => { unit!($container, m:1,  s:1) };
    ( $container:expr, km.s ) => { unit!($container, km:1, s:1) };
    ( $container:expr, m/s )  => { unit!($container, m:1,  s:-1) };
    ( $container:expr, m/s2 ) => { unit!($container, m:1,  s:-2) };
    ( $container:expr, N )    => { unit!($container, N:1) };

    ( $container:expr, $( $name:ident : $exp:expr ),* $(,)? ) => {
        $container.create_unit(&[ $(
            (
                stringify!($name),
                float_parsed!($exp)
            )
        ),* ]).unwrap()
    };

    ( $container:expr, $( ($name:expr): $exp:expr ),* $(,)? ) => {
        $container.create_unit(&[ $(
            (
                $name,
                float_parsed!($exp)
            )
        ),* ]).unwrap()
    };
}

/// Creates a void [`Unit`] instance.
///
/// [`Unit`]: crate::value::Unit
#[macro_export]
#[doc(hidden)]
macro_rules! no_unit {
    () => {
        $crate::value::Unit::default()
    }
}

/// Creates a [`Unit`] with [`Float`] exponents.
///
/// E.g: `float_unit!(container, km:float_parsed!(2.3))`
///
/// [`Unit`]: crate::value::Unit
/// [`Float`]: rug::Float
#[macro_export]
#[doc(hidden)]
macro_rules! float_unit {
    ( $container:expr, $( $name:tt : $exp:expr ),* ) => {
        $container.create_unit(&[ $(
            (
                stringify!($name),
                $exp
            )
        ),* ]).unwrap()
    };
}

/// Creates a [`CompoundQuantity`] instance.
///
/// [`CompoundQuantity`]: crate::value::CompoundQuantity
#[macro_export]
#[doc(hidden)]
macro_rules! compound_quantity {
    ( $( $quantity:ident : $exp:expr ),* $(,)? ) => {
        $crate::value::CompoundQuantity::from(maplit::hashmap!{ $(
            String::from(stringify!($quantity)) => float!($exp)
        ),* })
    };
}

/// Creates an empty [`CompoundQuantity`] instance.
///
/// [`CompoundQuantity`]: crate::value::CompoundQuantity
#[macro_export]
#[doc(hidden)]
macro_rules! no_compound_quantity {
    () => {
        $crate::value::CompoundQuantity::default()
    }
}

/// Unwraps the given [`BaseUnit`] from the given base unit name. It will panic if the given base
/// unit name does not exist. Mainly useful for tests.
///
/// [`BaseUnit`]: crate::value::BaseUnit
#[macro_export]
#[doc(hidden)]
macro_rules! base_unit {
    ( $container:expr, $base_unit_name:expr ) => {
        $container.strong_base_unit($base_unit_name).unwrap().clone()
    }
}

/// Unwraps the given [`Quantity`] from the given quantity name. It will panic if the given quantity
/// name does not exist. Mainly useful for tests.
///
/// [`Quantity`]: crate::value::Quantity
#[macro_export]
#[doc(hidden)]
macro_rules! quantity {
    ( $container:expr, $quantity_name:expr ) => {
        $container.strong_quantity($quantity_name).unwrap().clone()
    }
}

/// Creates a void operand.
#[macro_export]
#[doc(hidden)]
macro_rules! void_operand {
    () => ( $crate::value::VoidOperand::new() )
}

/// Creates a number operand.
///
/// E.g: `num_operand!(container, 1 km)` will create a [`NumOperand`] instance.
///
/// [`NumOperand`]: crate::value::NumOperand
#[macro_export]
#[doc(hidden)]
macro_rules! num_operand {
    ( $container:expr, [expr] ( $($val:tt)* ) $( $tt:tt )* ) => {{
        use $crate::unit;
        $crate::value::NumOperand::new($($val)*, &unit!($container, $( $tt )*))
    }};
    ( $container:expr, ( $val:expr ) $( $tt:tt )* ) => {{
        use $crate::unit;
        $crate::value::NumOperand::new(float_parsed!($val), &unit!($container, $( $tt )*))
    }};
    ( $container:expr, [ $val:expr ] $( $tt:tt )* ) => {{
        use $crate::unit;
        $crate::value::NumOperand::new(float_parsed!($val), &unit!($container, $( $tt )*))
    }};
    ( $container:expr, -$val:tt $( $tt:tt )* ) => {
        $crate::num_operand!($container, [-$val] $( $tt )*)
    };
    ( $container:expr, $val:tt $( $tt:tt )* ) => {
        $crate::num_operand!($container, [$val] $( $tt )*)
    };
}

/// Creates an int operand.
#[macro_export]
#[doc(hidden)]
macro_rules! int_operand {
    ( $int_value:expr ) => ( $crate::value::IntOperand::from($int_value) )
}

/// Creates a boolean operand.
#[macro_export]
#[doc(hidden)]
macro_rules! boolean_operand {
    ( $bool_value:expr ) => ( $crate::value::BooleanOperand::from($bool_value) )
}

/// Creates a string operand.
#[macro_export]
#[doc(hidden)]
macro_rules! str_operand {
    ( $s:expr ) => ( $crate::value::StrOperand::from($s) )
}

/// Creates a void operand.
#[macro_export]
macro_rules! void {
    () => {{
        use $crate::value::IntoValue;
        $crate::void_operand!().into_value()
    }}
}

/// Creates a number value, e.g: `number!(container, 10 km/s)`
#[macro_export]
macro_rules! number {
    ( $($tt:tt)* ) => {{
        use $crate::value::IntoValue;
        $crate::num_operand!($($tt)*).into_value()
    }}
}

#[macro_export]
macro_rules! int {
    ( $i:expr ) => {{
        use $crate::value::IntoValue;
        $crate::int_operand!($i).into_value()
    }}
}

/// Creates a boolean value from a Rust boolean, e.g: `boolean!(n == 10)`.
#[macro_export]
macro_rules! boolean {
    ( $b:expr ) => {{
        use $crate::value::IntoValue;
        $crate::boolean_operand!($b).into_value()
    }}
}

/// Creates a `true` boolean value.
#[macro_export]
#[doc(hidden)]
macro_rules! v_true {
    () => ( boolean!(true) )
}

/// Creates a `false` boolean value.
#[macro_export]
#[doc(hidden)]
macro_rules! v_false {
    () => ( boolean!(false) )
}

/// Creates a string value, e.g: `string!("hello")`.
#[macro_export]
macro_rules! string {
    ( $s:expr ) => {{
        use $crate::value::IntoValue;
        $crate::str_operand!($s).into_value()
    }}
}

/// Creates an array value.
#[macro_export]
macro_rules! array {
    ( $($values:expr),* $(,)? ) => {{
        use $crate::value::IntoValue;
        $crate::array_operand![
            $($values),*
        ].into_value()
    }}
}

/// Creates an array operand.
#[macro_export]
#[doc(hidden)]
macro_rules! array_operand {
    ( $($values:expr),* $(,)? ) => {
        $crate::value::ArrayOperand::from(vec![
            $($values),*
        ])
    }
}

/// Creates a dict value.
#[macro_export]
macro_rules! dict {
    ( $($key:expr => $value:expr),* $(,)? ) => {{
        use $crate::value::IntoValue;
        $crate::dict_operand![$( $key => $value ),*].into_value()
    }}
}

/// Creates a dict operand.
#[macro_export]
macro_rules! dict_operand {
    ( $($key:expr => $value:expr),* $(,)? ) => {{
        #[allow(unused_mut)]
        let mut dict = $crate::value::DictOperand::new();
        $( dict.try_insert($key, $value).unwrap(); )*
        dict
    }}
}

/// Creates an [`ExprSpan`].
///
/// [`ExprSpan`]: crate::eval::ExprSpan
#[macro_export]
#[doc(hidden)]
macro_rules! expr_span {
    ($($expr:tt)*) => {
        $crate::eval::ExprSpan::new($crate::eval::Expr::$($expr)*)
    };
}

/// Creates a void value.
#[macro_export]
#[doc(hidden)]
macro_rules! expr_void {
    () => ( expr_span!(Val(void!())) )
}

/// Gets the cloned value of a variable.
#[macro_export]
#[doc(hidden)]
macro_rules! var_value {
    ( $scope:expr, $name:expr ) => ( $scope.var($name).unwrap().borrow().clone() )
}

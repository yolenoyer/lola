//! Utilitary functions and macros.

use std::fmt;

#[cfg(test)]
#[macro_use]
pub mod test_common;

#[macro_use]
pub mod numbers;
#[macro_use]
mod macro_helpers;
pub mod writer;

#[doc(inline)]
pub use numbers::format_number;
#[doc(inline)]
pub use writer::*;

#[derive(Clone)]
#[doc(hidden)]
pub struct NoDebug<T>(pub T);

impl<T> fmt::Debug for NoDebug<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "")
    }
}

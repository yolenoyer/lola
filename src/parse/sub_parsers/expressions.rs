//! Parsers for expressions: heart of the parser.

use super::nom::{
    tag,
    multispace0,
    pair,
    tuple,
    preceded,
    alt,
    many0,
    map,
    opt,
    consumed,
};
use nom::Err;

use crate::context::Container;
use crate::parse::sub_parsers::{assignable_let, dict, with_span};
use crate::eval::{AssignMode, AssignTarget, Assignable, Expr, ExprSpan};
use crate::parse::Span;

use super::{assignable, dot_oper, is_keyword};
use super::{UParser, UResult, UError, Input};
use super::{function_definition,call_arguments};
use super::flow_control_expression;
use super::{parentheses_block, curly_brackets_block};
use super::{incr_decr_prefix_operators};
use super::{spaced, literal, identifier, word};
use super::{value_conversion_operator, Conversion};
use super::{array, index};
use super::to_expr_span_map;

/// Stores a parse result for a priority 0 operation.
#[doc(hidden)]
pub enum Prior0 {
    Index(ExprSpan, Span),
    Call(Vec<ExprSpan>, Span),
    Dot(ExprSpan),
    PostfixDecr(Span),
    PostfixIncr(Span),
}

/// Parses a whole expression.
pub fn expression(container: &Container) -> impl UParser<ExprSpan> {
    spaced(oper_prior_9(container))
}

/// Parses a single value as an expression.
pub fn literal_expr(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(multispace0, literal(container)),
        Expr::Val
    )
}

/// Parses an identifier as an expression.
pub fn variable_expr(input: Input<'_>) -> UResult<ExprSpan> {
    to_expr_span_map(
        identifier,
        |id| Expr::Get(id.to_string())
    )(input)
}

/// Parses a "leaf" expression: a single value, a variable name, or a parenthese expression.
pub fn single_expr<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    preceded(
        multispace0,
        alt((
            closure!(flow_control_expression(container)),
            closure!(function_definition(container)),
            closure!(incr_decr_prefix_operators(container)),
            literal_expr(container),
            variable_expr,
            closure!(dict(container)),
            closure!(parentheses_block(container)),
            closure!(curly_brackets_block(container)),
            closure!(array(container)),
        )),
    )
}

/// Parses a list of operations with priority 0 (index, function calls, postfix operators "++" and
/// "--").
pub fn oper_prior_0<'a, P>(container: &'a Container, mut single_expr_parser: P) -> impl UParser<'a, ExprSpan>
where
    P: UParser<'a, ExprSpan>,
{
    move |input| {
        let (remain, (first, opers)) = pair(
            consumed(|input| single_expr_parser(input)),
            many0(alt((
                map(call_arguments(container), |(exprs, args_span)| Prior0::Call(exprs, args_span)),
                map(index(container), |(expr_span, index_span)| Prior0::Index(expr_span, index_span)),
                map(dot_oper, |(_dot, ident)| Prior0::Dot(ident)),
                map(spaced(with_span(tag("++"))), |(span, _tag)| Prior0::PostfixIncr(span)),
                map(spaced(with_span(tag("--"))), |(span, _tag)| Prior0::PostfixDecr(span)),
            ))),
        )(input)?;
        let final_expr_span = build_prior_0_expr(container, first, opers)
            .map_err(Err::Failure)?;
        Ok((remain, final_expr_span))
    }
}

/// Parses a list of operations with priority 1 (negation).
fn oper_prior_1<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            many0(spaced(tag("!"))),
            closure!(oper_prior_0(container, single_expr(container))),
        ),
        |(opers, expr_span)| build_prior_1_expr(opers, expr_span)
    )
}

/// Parses a list of operations with priority 2 (power).
fn oper_prior_2<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_1(container)),
            many0(pair(
                spaced(tag("^")),
                closure!(oper_prior_1(container)),
            )),
        ),
        |(v, opers)| build_equal_prior_span_expr(v, opers)
    )
}

/// Parses a list of operations with priority 3 (multiplication, division).
fn oper_prior_3<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_2(container)),
            many0(pair(
                spaced(alt((
                    tag("*"),
                    tag("/"),
                ))),
                closure!(oper_prior_2(container)),
            )),
        ),
        |(v, opers)| build_equal_prior_span_expr(v, opers)
    )
}

/// Parses a list of operations with priority 4 (addition, subtraction).
fn oper_prior_4<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_3(container)),
            many0(pair(
                spaced(alt((
                    tag("+"),
                    tag("-"),
                    tag("%"),
                ))),
                closure!(oper_prior_3(container)),
            )),
        ),
        |(first, opers)| build_equal_prior_span_expr(first, opers)
    )
}

/// Parses a list of operations with priority 5 (value conversion).
fn oper_prior_5<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_4(container)),
            many0(value_conversion_operator(container)),
        ),
        |(first, opers)| build_conversion_expr(first, opers)
    )
}

/// Parses a list of operations with priority 6 (comparisons between values).
fn oper_prior_6<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_5(container)),
            many0(pair(
                spaced(alt((
                    tag("=="),
                    tag("!="),
                    tag("<="),
                    tag(">="),
                    tag("<"),
                    tag(">"),
                ))),
                closure!(oper_prior_5(container)),
            )),
        ),
        |(first, opers)| build_equal_prior_span_expr(first, opers)
    )
}

/// Parses a list of operations with priority 7 (logical and).
fn oper_prior_7<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_6(container)),
            many0(pair(
                spaced(tag("&&")),
                closure!(oper_prior_6(container)),
            )),
        ),
        |(first, opers)| build_equal_prior_span_expr(first, opers)
    )
}

/// Parses a list of operations with priority 8 (logical or).
pub fn oper_prior_8<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    map(
        pair(
            closure!(oper_prior_7(container)),
            many0(pair(
                spaced(tag("||")),
                closure!(oper_prior_7(container)),
            )),
        ),
        |(first, opers)| build_equal_prior_span_expr(first, opers)
    )
}

/// Parses a list of operations with priority 9 (assignment).
fn oper_prior_9<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    move |input: Input<'a>| {
        let result = alt((
            to_expr_span_map(
                tuple((
                    spaced(word("let")),
                    assignable_let(container, AssignMode::Let),
                    opt(pair(
                        spaced(tag("=")),
                        closure!(oper_prior_8(container)),
                    )),
                )),
                |(_let, assignable, opt_value)| {
                    let expr_span = match opt_value {
                        Some((_op, expr_span)) => expr_span,
                        None => expr_void!(),
                    };
                    Expr::Set(assignable, Box::new(expr_span))
                }
            ),
            expr_or_assign_expr(container),
        ))(input);

        // Check if the variable name is valid:
        if let Ok((_, expr_span)) = &result {
            match &expr_span.expr {
                Expr::Set(assignable, _)
                | Expr::SetMul(assignable, _)
                | Expr::SetDiv(assignable, _)
                | Expr::SetAdd(assignable, _)
                | Expr::SetSub(assignable, _) => {
                    if let AssignTarget::Variable(var_name) = &assignable.target {
                        if is_keyword(var_name) {
                            let err = UError::ForbiddenVarName(var_name.clone(), expr_span.span.clone());
                            return Err(nom::Err::Failure(err));
                        }
                    }
                },
                _ => (),
            }
        }

        result
    }
}

/// Parses either an expression, or an assignment expression if it is followed by an assignment
/// operator and an rhs expression.
fn expr_or_assign_expr(container: &Container) -> impl UParser<'_, ExprSpan> {
    move |input| -> UResult<ExprSpan> {
        let (remain, expr_span) = oper_prior_8(container)(input)?;
        let assign_tag = spaced(alt((
            tag("="),
            tag("*="),
            tag("/="),
            tag("+="),
            tag("-="),
        )))(remain);
        if let Ok((remain, assign_tag)) = assign_tag {
            let (_, assignable) = assignable(container, AssignMode::Set)(input)
                .map_err(|_| nom::Err::Failure(UError::CannotAssign(expr_span.span)))?;
            let (remain, rhs) = oper_prior_8(container)(remain)?;
            let expr = build_assign_operator_expr((assignable, assign_tag, rhs));
            let mut expr_span = ExprSpan::new(expr);
            expr_span.set_span(Span::from_inputs(remain, remain));
            Ok((remain, expr_span))
        } else {
            Ok((remain, expr_span))
        }
    }
}

/// Helper to create a list of operations of the same priority. E.g, this is used when an input
/// like `4 + 5 - 8 + 3` is encountered. It will be called as well for `4 + (7 * 8) + 9`, but
/// the parenthese group will already have been parsed into an expression.
fn build_equal_prior_span_expr(first: ExprSpan, opers: Vec<(Input<'_>, ExprSpan)>) -> ExprSpan {
    opers.into_iter().fold(first, |acc, val| build_operator_span_expr(val, acc))
}

/// Creates an expression based on which operator is given.
fn build_operator_span_expr((op, e2): (Input<'_>, ExprSpan), e1: ExprSpan) -> ExprSpan {
    let span = Span::from_interval(&e1.span, &e2.span);

    let (e1, e2) = (Box::new(e1), Box::new(e2));
    let expr = match op.text {
        "^"  => Expr::Pow(e1, e2),
        "*"  => Expr::Mul(e1, e2),
        "/"  => Expr::Div(e1, e2),
        "+"  => Expr::Add(e1, e2),
        "-"  => Expr::Sub(e1, e2),
        "%"  => Expr::Mod(e1, e2),
        "==" => Expr::Equ(e1, e2),
        "!=" => Expr::Nequ(e1, e2),
        "<"  => Expr::CmpLt(e1, e2),
        ">"  => Expr::CmpGt(e1, e2),
        "<=" => Expr::CmpLte(e1, e2),
        ">=" => Expr::CmpGte(e1, e2),
        "&&" => Expr::And(e1, e2),
        "||" => Expr::Or(e1, e2),
        _ => unreachable!(),
    };

    ExprSpan::with_span(expr, span)
}

/// Creates an expression based on which assignment operator is given.
fn build_assign_operator_expr((assignable, op, rhs): (Assignable, Input<'_>, ExprSpan)) -> Expr {
    let rhs = Box::new(rhs);
    match op.text {
        "="  => Expr::Set(assignable, rhs),
        "*=" => Expr::SetMul(assignable, rhs),
        "/=" => Expr::SetDiv(assignable, rhs),
        "+=" => Expr::SetAdd(assignable, rhs),
        "-=" => Expr::SetSub(assignable, rhs),
        _ => unreachable!(),
    }
}

/// Helper to create a list of operations of priority 0.
pub fn build_prior_0_expr<'a>(
    container: &'a Container,
    (first_consumed, first): (Input<'a>, ExprSpan),
    opers: Vec<Prior0>
) -> Result<ExprSpan, UError>
{
    opers.into_iter().try_fold(
        first,
        |acc, oper| {
             match oper {
                Prior0::Index(index, index_span) => {
                    let span = Span::from_interval(&acc.span, &index_span);
                    let expr_span = ExprSpan::with_span(
                        Expr::Index(Box::new(acc), Box::new(index)),
                        span
                    );
                    Ok(expr_span)
                },
                Prior0::Dot(ident_expr_span) => {
                    let span = Span::from_interval(&acc.span, &ident_expr_span.span);
                    let expr = Expr::Index(Box::new(acc), Box::new(ident_expr_span));
                    Ok(ExprSpan::with_span(expr, span))
                },
                Prior0::Call(args, args_span) => {
                    let span = Span::from_interval(&acc.span, &args_span);
                    let expr_span = ExprSpan::with_span(
                        Expr::FuncCall(
                            Box::new(acc),
                            args.into_iter().collect::<Vec<_>>()
                        ),
                        span
                    );
                    Ok(expr_span)
                },
                Prior0::PostfixDecr(oper_span) => {
                    let span = Span::from_interval(&acc.span, &oper_span);
                    match assignable(container, AssignMode::Set)(first_consumed) {
                        Ok((_, assignable)) => {
                            Ok(ExprSpan::with_span(Expr::PostfixDecr(assignable), span))
                        },
                        Err(_) => Err(UError::CannotAssign(span))
                    }
                },
                Prior0::PostfixIncr(oper_span) => {
                    let span = Span::from_interval(&acc.span, &oper_span);
                    match assignable(container, AssignMode::Set)(first_consumed) {
                        Ok((_, assignable)) => {
                            Ok(ExprSpan::with_span(Expr::PostfixIncr(assignable), span))
                        },
                        Err(_) => Err(UError::CannotAssign(span))
                    }
                },
            }
        }
    )
}

/// Helper to create a list of conversion operations.
fn build_conversion_expr(first: ExprSpan, opers: Vec<(Conversion, Span)>) -> ExprSpan {
    opers.into_iter().fold(
        first,
        |acc, (conv, conv_span)| {
            let expr = match conv {
                Conversion::Normal(unit) => Expr::Conv(Box::new(acc), unit),
                Conversion::Raw(unit) => Expr::RawConv(Box::new(acc), unit),
            };
            ExprSpan::with_span(expr, conv_span)
        }
    )
}

/// Creates an `ExprSpan` for priority 1 expression (negation).
fn build_prior_1_expr(opers: Vec<Input<'_>>, expr_span: ExprSpan) -> ExprSpan {
    let nb_opers = opers.len();
    match nb_opers {
        0 => expr_span,
        _ if nb_opers % 2 == 1 => {
            let oper_span = Span::from(opers[0]);
            let span = Span::from_interval(&oper_span, &expr_span.span);
            ExprSpan::with_span(Expr::Not(Box::new(expr_span)), span)
        }
        _ if nb_opers % 2 == 0 => {
            let oper_span0 = Span::from(opers[0]);
            let span0 = Span::from_interval(&oper_span0, &expr_span.span);
            let oper_span1 = Span::from(opers[1]);
            let span1 = Span::from_interval(&oper_span1, &expr_span.span);
            let expr_span1 = ExprSpan::with_span(Expr::Not(Box::new(expr_span)), span1);
            ExprSpan::with_span(Expr::Not(Box::new(expr_span1)), span0)
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    macro_rules! test_expr {
        ( $expr:expr, $( $expected:tt )* ) => {
            with_test_context!(cxt, {
                let value = eval!(cxt, $expr);
                assert!(value.equal(&number!(cxt.container, $( $expected )*)));
            });
        }
    }

    macro_rules! test_int_expr {
        ( $expr:expr, $( $expected:tt )* ) => {
            with_test_context!(cxt, {
                let value = eval!(cxt, $expr);
                assert!(value.equal(&int!($( $expected )*)));
            });
        }
    }

    macro_rules! test_err_expr {
        ( $expr:expr, $err:ident ) => {
            with_test_context!(cxt, {
                let expr_result = eval_err!(cxt, $expr);
                assert_eq!(expr_result, err!(Expr:Operation:$err));
            });
        }
    }

    #[test]
    fn test_single_value() {
        test_expr!("200m", 200 m);
        test_expr!(" 40 km/s  ", 40 km/s);
    }

    #[test]
    fn test_add_one_value() {
        test_expr!("200m + 10m", 210 m);
        test_expr!("40 km/s  + 1000m/s", 41 km/s);
        test_err_expr!("40m + 10s", MismatchUnits);
    }

    #[test]
    fn test_add_multiple_values() {
        test_expr!("200m + 10m + 1km", 1210 m);
        test_err_expr!("40m + 10s + 1m", MismatchUnits);
    }

    #[test]
    fn test_add_and_sub_values() {
        test_expr!("200m + 10m - 1km", -790 m);
        test_err_expr!("40m - 10s", MismatchUnits);
    }

    #[test]
    fn test_mul_one_value() {
        test_expr!("200m * 10", 2000 m);
        test_expr!("10m * 10s", 100 m.s);
    }

    #[test]
    fn test_mul_multiple_values() {
        test_expr!("10m * 10m * 10s", 1000 m:2, s:1);
    }

    #[test]
    fn test_pow_one_value() {
        test_expr!("200m ^ 2", 40000 m2);
    }

    #[test]
    fn test_pow_multiple_values() {
        test_expr!("10m ^ 2 ^2", 10000 m:4);
    }

    #[test]
    fn test_mul_and_div_values() {
        test_expr!("100km / 2h * 10", 500 km/h);
    }

    #[test]
    fn test_precedence() {
        test_int_expr!("2 * 3 + 4 * 5", 26);
        test_expr!("2km / 10h + 3km / 3600s / 10", 0.5 km/h);
        test_int_expr!("2 ^ 3 * 3 - 7 ^ 2", -25);
    }

    #[test]
    fn test_paren() {
        test_int_expr!("2 * (3 + 4)", 14);
        test_expr!("3 * (3 m + 4 * 3 km)", 36009 m);
    }

    #[test]
    fn test_nested_paren() {
        test_expr!("2 * (3 + 4 + 120 / (2 + 5 - 1))", 54);
    }

    #[test]
    fn test_conversion() {
        test_expr!("2km ~m", 2000 m);
        test_expr!("80km/h + 100km/h ~m/s", 50 m/s);
    }

    #[test]
    fn test_raw_conversion() {
        test_expr!("2km ~!", 2);
        test_expr!("80km/h + 100km/h ~!N", 180 N);
        test_expr!("80km/h + 100m/h ~!N", 80.1 N);
    }

    #[test]
    fn test_multiple_conversion() {
        test_expr!("2~!km~m", 2000 m);
        test_expr!("80km/h ~!N ~!", 80);
        test_expr!("2km ~mm~km", 2 km);
    }

    #[test]
    fn test_bad_conversion() {
        test_err_expr!("80km/h + 100km/h ~s", MismatchUnits);
    }

    #[test]
    fn test_assign_variable() {
        with_test_context!(cxt, {
            eval!(cxt, "let foo = 1 + 1");
            assert_eq!(var_value!(cxt.scope, "foo"), int!(2));
        })
    }

    #[test]
    fn test_get_variable() {
        with_test_context!(cxt, {
            eval!(cxt, "  let foo = 1 + 1");
            let r = eval!(cxt, " foo + 1");
            assert_eq!(r, int!(3));
        })
    }

    #[test]
    fn test_func_call() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "sqrt(16km2)");
            assert_eq!(r, number!(cxt.container, 4 km));
        })
    }

    #[test]
    fn test_func_call_error() {
        with_test_context!(cxt, {
            let r = eval_err!(cxt, "sqrt()");
            assert_eq!(r, err!(Expr:Call:ArgList:WrongArgCount));

            let r = eval_err!(cxt, "foo_undefined()");
            assert_eq!(r, err!(Expr:UndefinedVariable(s!("foo_undefined"))));
        })
    }

    #[test]
    fn test_anonymous_func_call() {
        with_test_context!(cxt, {
            let v = eval!(cxt, "(fn(n) n*2)(3)");
            assert_eq!(v, int!(6));
        })
    }

    #[test]
    fn test_blocks() {
        test_int_expr!("2 + (let a = 4; a * 2)", 10);

        with_test_context!(cxt, {
            let v = eval!(cxt, " let a = 4km; let b = 6km; a * b ");
            assert_eq!(v, number!(cxt.container, 24 km2));
            assert_eq!(var_value!(cxt.scope, "a"), number!(cxt.container, 4 km));
            assert_eq!(var_value!(cxt.scope, "b"), number!(cxt.container, 6 km));
        })
    }

    #[test]
    fn test_blocks_with_void_items() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "(4km;)"), void!());
            assert_eq!(eval!(cxt, "(;)"), void!());
            assert_eq!(eval!(cxt, "(;;;)"), void!());
            assert_eq!(eval!(cxt, " ( ;4km;;) "), void!());
            assert_eq!(eval!(cxt, "(;;4km)"), number!(cxt.container, 4 km));
            assert_eq!(eval!(cxt, "4km;"), void!());
            assert_eq!(eval!(cxt, ";"), void!());
            assert_eq!(eval!(cxt, ";;;"), void!());
            assert_eq!(eval!(cxt, " ;4km; ;"), void!());
            assert_eq!(eval!(cxt, ";;4km"), number!(cxt.container, 4 km));
        })
    }

    #[test]
    fn test_empty_expr() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, ""), void!());
            assert_eq!(eval!(cxt, " "), void!());
            assert_eq!(eval!(cxt, "()"), void!());
            assert_eq!(eval!(cxt, "( )"), void!());
        })
    }

    #[test]
    fn test_dicts() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "{}"), dict!());
            assert_eq!(eval!(cxt, "{ }"), dict!());
        })
    }

    #[test]
    fn test_dicts_ident_keys() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "{ foo: 12 }"), dict!{ string!("foo") => int!(12) });
        })
    }

    #[test]
    fn test_dicts_square_bracket_keys() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "foo = 17; { [foo]: 12 }"), dict!{ int!(17) => int!(12) });
        })
    }

    #[test]
    fn test_dicts_dot_oper() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "d = { foo: 12 }; d.foo"), int!(12));
        })
    }

    #[test]
    fn test_add_strings() {
        with_test_context!(cxt, {
            let r = eval!(cxt, r#"  "12" + "34"  "#);
            assert_eq!(r, string!("1234"));

            let r = eval!(cxt, r#"  "The value is " + (1km + 200m ~m)  "#);
            assert_eq!(r, string!("The value is 1200 m"));

            let r = eval!(cxt, r#"  12km + " = " + (12km ~m)  "#);
            assert_eq!(r, string!("12 km = 12000 m"));
        })
    }

    #[test]
    fn test_user_func_void_return() {
        with_test_context!(cxt, {
            eval!(cxt, r#"  let nop_func = fn() { "nop"; }  "#);
            let r = eval!(cxt, r#" nop_func() "#);
            assert_eq!(r, void!());
        })
    }

    #[test]
    fn test_comments() {
        with_test_context!(cxt, {
            let r = eval!(cxt, r#" 4*2 // My comment  "#);
            assert_eq!(r, int!(8));

            let r = eval!(cxt, "//");
            assert_eq!(r, void!());

            let r = eval!(cxt, "    \n\
                4*2; // My comment  \n\
                // Other comment    \n\
                //                  \n\
                36                  \n\
                // Last comment     \n\
            ");
            assert_eq!(r, int!(36));
        })
    }

    #[test]
    fn test_equ() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "2km == 2km"), v_true!());
            assert_eq!(eval!(cxt, "2km == 3km"), v_false!());
        })
    }

    #[test]
    fn test_nequ() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "2km != 2km"), v_false!());
            assert_eq!(eval!(cxt, "2km != 3km"), v_true!());
        })
    }

    #[test]
    fn test_inequalities() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "2km >= 2km"), v_true!());
            assert_eq!(eval!(cxt, "2km <= 2km"), v_true!());
            assert_eq!(eval!(cxt, "2km > 2km"), v_false!());
            assert_eq!(eval!(cxt, "2km < 2km"), v_false!());
            assert_eq!(eval!(cxt, "3000m >= 2km"), v_true!());
            assert_eq!(eval!(cxt, "3000m <= 2km"), v_false!());
            assert_eq!(eval!(cxt, "3000m > 2km"), v_true!());
            assert_eq!(eval!(cxt, "3000m < 2km"), v_false!());
        })
    }

    #[test]
    fn test_logical_operations() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "true && true"), v_true!());
            assert_eq!(eval!(cxt, "true && false"), v_false!());
            assert_eq!(eval!(cxt, "false && true"), v_false!());
            assert_eq!(eval!(cxt, "false && false"), v_false!());
            assert_eq!(eval!(cxt, "true || true"), v_true!());
            assert_eq!(eval!(cxt, "true || false"), v_true!());
            assert_eq!(eval!(cxt, "false || true"), v_true!());
            assert_eq!(eval!(cxt, "false || false"), v_false!());

            assert_eq!(eval!(cxt, "false || false && true"), v_false!());
            assert_eq!(eval!(cxt, "false || true && true"), v_true!());
            assert_eq!(eval!(cxt, "false && true || false"), v_false!());

            assert_eq!(eval!(cxt, "!false"), v_true!());
            assert_eq!(eval!(cxt, "!true"), v_false!());

            assert_eq!(eval!(cxt, "3 < 10 && 100 < 10 || 45 == 8"), v_false!());
        })
    }

    #[test]
    fn test_assign_oper() {
        with_test_context!(cxt, {
            eval!(cxt, "let a = 2");
            eval!(cxt, "a += 2");
            assert_eq!(var_value!(cxt.scope, "a"), int!(4));
            eval!(cxt, "a *= 2");
            assert_eq!(var_value!(cxt.scope, "a"), int!(8));
            eval!(cxt, "a -= 2");
            assert_eq!(var_value!(cxt.scope, "a"), int!(6));
            eval!(cxt, "a /= 2");
            assert_eq!(var_value!(cxt.scope, "a"), int!(3));
        })
    }

    #[test]
    fn test_incr_decr_oper() {
        with_test_context!(cxt, {
            eval!(cxt, "let a = 2");

            let before = eval!(cxt, "a++");
            assert_eq!(before, int!(2));
            assert_eq!(var_value!(cxt.scope, "a"), int!(3));

            let before = eval!(cxt, "++a");
            assert_eq!(before, int!(4));
            assert_eq!(var_value!(cxt.scope, "a"), int!(4));

            let before = eval!(cxt, "--a");
            assert_eq!(before, int!(3));
            assert_eq!(var_value!(cxt.scope, "a"), int!(3));

            let before = eval!(cxt, "a--");
            assert_eq!(before, int!(3));
            assert_eq!(var_value!(cxt.scope, "a"), int!(2));

            let r = eval_wrapped!(cxt, "a = 3km; a++");
            assert!(r.is_err());
        })
    }

    #[test]
    fn test_optional_commas() {
        with_test_context!(cxt, {
            assert_eq!(eval!(cxt, "[1, 2,]"), array![ int!(1), int!(2) ]);
            assert_eq!(
                eval!(cxt, "{ a: 1, b: 2,}"),
                dict![ string!("a") => int!(1), string!("b") => int!(2) ]
            );
            assert!(eval!(cxt, "fn foo(a,) a").is_func());
            assert_eq!(eval!(cxt, "foo(10,)"), int!(10));
        });
    }
}

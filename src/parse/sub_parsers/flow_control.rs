//! Parsers for flow-control structures (`for`, `if`, ...).

use super::nom::{
    multispace1,
    pair,
    tuple,
    preceded,
    opt,
    alt,
};

use crate::context::Container;
use crate::eval::{AssignMode, Expr, ExprSpan};

use super::{UParser, assignable_let};
use super::{expression, spaced, word, identifier};
use super::to_expr_span_map;

/// Parses a "if/else" expression.
fn if_condition(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        tuple((
            preceded(
                pair(
                    word("if"),
                    multispace1,
                ),
                expression(container),
            ),
            expression(container),
            opt(preceded(
                spaced(word("else")),
                expression(container),
            )),
        )),
        |(cond, if_block, else_block)| {
            Expr::If(
                Box::new(cond),
                Box::new(if_block),
                else_block.map(Box::new),
            )
        }
    )
}

/// Parses a while loop.
fn while_loop(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        tuple((
            preceded(
                pair(
                    word("while"),
                    multispace1,
                ),
                expression(container),
            ),
            expression(container),
        )),
        |(cond, block)| {
            Expr::While(
                Box::new(cond),
                Box::new(block),
            )
        }
    )
}

/// Parses a for loop ("for VAR in EXPR {...}").
fn for_loop(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        tuple((
            preceded(
                spaced(word("for")),
                assignable_let(container, AssignMode::Set),
            ),
            preceded(
                spaced(word("in")),
                expression(container),
            ),
            expression(container),
        )),
        |(assignable, arr, block)| {
            Expr::For(
                Some(assignable),
                Box::new(arr),
                Box::new(block),
            )
        }
    )
}

/// Parses an anonymous for loop ("for ITERABLE {...}", no index variable).
fn for_anonymous_loop(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        tuple((
            preceded(
                spaced(word("for")),
                expression(container),
            ),
            expression(container),
        )),
        |(nb_times, block)| {
            Expr::For(
                None,
                Box::new(nb_times),
                Box::new(block),
            )
        }
    )
}

/// Parses a loop expression.
fn loop_block(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(
            spaced(word("loop")),
            expression(container),
        ),
        |block| Expr::Loop(Box::new(block))
    )
}

/// Parses a break statement.
fn break_statement(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(
            spaced(word("break")),
            opt(expression(container)),
        ),
        |opt_expr| Expr::Break(opt_expr.map(Box::new))
    )
}

/// Parses a continue statement.
fn continue_statement(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(
            spaced(word("continue")),
            opt(expression(container)),
        ),
        |opt_expr| Expr::Continue(opt_expr.map(Box::new))
    )
}

/// Parses a return statement.
fn return_statement(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(
            spaced(word("return")),
            opt(expression(container)),
        ),
        |opt_expr| Expr::Return(opt_expr.map(Box::new))
    )
}

/// Parses a leave statement.
fn leave_statement(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(
            spaced(word("leave")),
            opt(expression(container)),
        ),
        |opt_expr| Expr::Leave(opt_expr.map(Box::new))
    )
}

/// Parses a throw statement.
fn throw_statement(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        preceded(
            spaced(word("throw")),
            opt(expression(container)),
        ),
        |opt_expr| Expr::Throw(opt_expr.map(Box::new))
    )
}

/// Parses a try-catch block:
fn try_catch(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        pair(
            preceded(
                spaced(word("try")),
                expression(container),
            ),
            opt(preceded(
                spaced(word("catch")),
                pair(
                    opt(spaced(identifier)),
                    expression(container),
                )
            ))
        ),
        |(try_expr, opt_catch)| {
            Expr::TryCatch(
                Box::new(try_expr),
                opt_catch.map(|(id, catch_expr)| (
                    id.map(|id| id.to_string()),
                    Box::new(catch_expr),
                ))
            )
        }
    )
}

/// Parses a flow-control expression.
pub fn flow_control_expression<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    alt((
        closure!(if_condition(container)),
        closure!(loop_block(container)),
        closure!(while_loop(container)),
        closure!(for_loop(container)),
        closure!(for_anonymous_loop(container)),
        closure!(break_statement(container)),
        closure!(continue_statement(container)),
        closure!(return_statement(container)),
        closure!(leave_statement(container)),
        closure!(throw_statement(container)),
        closure!(try_catch(container)),
    ))
}

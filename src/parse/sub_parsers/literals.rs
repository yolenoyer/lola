//! Parsers for literals.

use super::nom::{
    tag,
    multispace0,
    anychar,
    separated_pair,
    map,
    alt,
    Parser,
};

use crate::context::Container;
use crate::value::{Value, IntoValue, NumOperand, IntOperand, StrOperand};

use super::{UParser, UResult, Input};
use super::{
    float_number_distinct_from_int,
    float_number_including_int,
    integer,
    word,
    optional_unit,
    mandatory_unit,
};

/// Parses an integer value
pub fn integer_value(input: Input) -> UResult<'_, Value> {
    map(integer, |n| IntOperand::new(n).into_value())
    (input)
}

/// Parses a whole number, e.g. `4.5 m.s-1`.
pub fn number<'a>(container: &'a Container) -> impl UParser<'a, Value> {
    move |input: Input<'a>| {
        let (i, (value, unit)) = alt((
            separated_pair(
                float_number_distinct_from_int,
                multispace0,
                optional_unit(container)
            ),
            separated_pair(
                float_number_including_int,
                multispace0,
                mandatory_unit(container)
            ),
        ))
        .parse(input)?;
        Ok((i, NumOperand::new(value, &unit).into_value()))
    }
}

/// Parses a string.
pub fn string<'a>(mut input: Input<'a>) -> UResult<'a, Value> {
    input = {
        let r = tag("\"")(input)?;
        r.0
    };
    let mut s = String::new();
    loop {
        let close: UResult<'a, Input<'a>> = tag("\"")(input);
        if let Ok((input, _)) = close {
            return Ok((input, StrOperand::from(s).into_value()));
        }
        let esc: UResult<'a, Input<'a>> = tag("\\\"")(input);
        if let Ok((i, _)) = esc {
            s.push('"');
            input = i;
            continue;
        }
        let r = anychar(input)?;
        input = r.0;
        s.push(r.1);
    }
}

/// Parses a boolean literal.
pub fn boolean(input: Input) -> UResult<'_, Value> {
    alt((
        map(word("true"),  |_| v_true!()),
        map(word("false"), |_| v_false!()),
    ))
    (input)
}

/// Parses a literal: a number or a string.
pub fn literal(container: &Container) -> impl UParser<'_, Value> {
    alt((
        number(container),
        integer_value,
        boolean,
        string,
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::UError;
    use crate::util::test_common::*;

    #[test]
    fn parse_number() {
        with_test_container!(container, {
            macro_rules! parse_value {
                ($s:expr) => ( number(&container)(Input::new($s, 0)).unwrap() );
            }
            let parsed = parse_value!("2 m2.s-1");
            let v = parsed.1;
            assert_eq!(v, number!(container, 2 m:2, s:-1));
        })
    }

    #[test]
    fn fail_parse_number() {
        with_test_container!(container, {
            let mut parse_value = number(&container);
            assert!(parse_value(Input::new("!!2 m2.s-1", 0)).is_err());
        })
    }

    #[test]
    fn parse_bad_number() {
        with_test_container!(container, {
            let mut parse_value = number(&container);
            let r = parse_value(Input::new("2 m2.bovis-1", 0));
            assert!(matches!(r, Err(nom::Err::Failure(UError::UndefinedBasicUnit(_, _)))));
        })
    }

    #[test]
    fn parse_string() {
        let input = Input::new("\"hello string\"", 0);
        let parsed = string(input).unwrap().1;
        assert_eq!(parsed, StrOperand::from("hello string").into_value());

        let input = Input::new("\"\"", 0);
        let parsed = string(input).unwrap().1;
        assert_eq!(parsed, StrOperand::from("").into_value());
    }

    #[test]
    fn parse_string_with_escape_quotes() {
        let input = Input::new("\"hello \\\"string\\\"\"", 0);
        let parsed = string(input).unwrap().1;
        assert_eq!(parsed, StrOperand::from("hello \"string\"").into_value());
    }

    #[test]
    fn parse_literal() {
        with_test_container!(container, {
            let input = Input::new("\"hello string\"", 0);
            let parsed = literal(&container)(input).unwrap().1;
            assert_eq!(parsed, StrOperand::from("hello string").into_value());

            let input = Input::new("2 m2.s-1", 0);
            let parsed = literal(&container)(input).unwrap().1;
            assert_eq!(parsed, number!(&container, 2 m:2, s:-1));

            let input = Input::new("true", 0);
            let parsed = literal(&container)(input).unwrap().1;
            assert_eq!(parsed, v_true!());
        })
    }

    #[test]
    fn parse_boolean() {
        let input = Input::new("true", 0);
        let parsed = boolean(input).unwrap().1;
        assert_eq!(parsed, v_true!());

        let input = Input::new("false", 0);
        let parsed = boolean(input).unwrap().1;
        assert_eq!(parsed, v_false!());
    }
}

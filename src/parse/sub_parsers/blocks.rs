//! Parsers for blocks (sequences of expressions, contained inside brackets or not).

use std::mem;

use super::nom::{
    tag,
    multispace1,
    not_line_ending,
    tuple,
    delimited,
    map,
};

use crate::context::Container;
use crate::eval::{ExprSpan, Expr};

use super::{UParser, UError, Input};
use super::{spaced, expression};
use super::to_expr_span_map;

/// Parses a sequence of expressions separated by semicolons.
///
/// *Note*: I tried to implement it with nom parsers (especially with `separated0()`) but it didn't
/// work as expected, for example with empty input, or input starting by a semicolon.
fn expr_sequence<'a>(container: &'a Container) -> impl UParser<'a, Vec<ExprSpan>> {
    move |mut input: Input<'a>| {
        let mut exprs = vec![];
        let mut last_was_expr = false;
        let mut err_if_no_semicolon = false;
        let mut expression = expression(container);
        let mut semicolon = spaced(tag(";"));

        loop {
            if let Ok((i, _)) = multispace1::<_, UError>(input) {
                input = i;
            }
            if let Ok((i, _)) = tuple::<_, _, UError, _>((
                tag("//"),
                not_line_ending,
            ))(input) {
                input = i;
                continue;
            }
            if let Ok((i, _)) = semicolon(input) {
                last_was_expr = false;
                input = i;
                continue;
            }
            match expression(input) {
                Ok((i, expr_span)) => {
                    if last_was_expr && err_if_no_semicolon {
                        return Err(nom::Err::Error(UError::BadExprSequence(expr_span.span)));
                    }
                    last_was_expr = true;
                    err_if_no_semicolon = expr_span.expr.needs_semicolon();
                    exprs.push(expr_span);
                    input = i;
                    continue;
                },
                Err(nom::Err::Failure(e)) => return Err(nom::Err::Failure(e)),
                _ => (),
            }
            if !last_was_expr {
                exprs.push(expr_void!());
            }
            break Ok((input, exprs));
        }
    }
}

/// Parses a scoped block containing several expressions separated by semi-colons.
fn scoped_block(container: &Container) -> impl UParser<ExprSpan> {
    map(
        to_expr_span_map(
            expr_sequence(container),
            Expr::ScopedBlock
        ),
        |mut expr_span| {
            check_leave_block(&mut expr_span);
            expr_span
        }
    )
}

/// Parses a block containing several expressions separated by semi-colons.
pub fn block(container: &Container) -> impl UParser<ExprSpan> {
    to_expr_span_map(
        expr_sequence(container),
        Expr::Block
    )
}

/// Parses a sub-expression contained into parentheses.
pub fn parentheses_block(container: &Container) -> impl UParser<ExprSpan> {
    delimited(
        spaced(tag("(")),
        block(container),
        spaced(tag(")")),
    )
}

/// Parses a sub-expression contained into curly brackets.
pub fn curly_brackets_block(container: &Container) -> impl UParser<ExprSpan> {
    delimited(
        spaced(tag("{")),
        scoped_block(container),
        spaced(tag("}")),
    )
}

/// Handles a special case: when "{ ...; leave ...; }" is encountered, replace the scoped block by
/// the special `Expr::LeaveBlock`, which exists only for this reason. It allows the `leave`
/// statement to ignore the scoped block "{}", which has no sense if the `leave` statement is at
/// the end.
fn check_leave_block(expr_span: &mut ExprSpan) {
    if let ExprSpan { expr: Expr::ScopedBlock(exprs), .. } = expr_span {
        let mut i = exprs.len();
        if matches!(exprs.last(), Some(ExprSpan { expr: Expr::Val(val), ..}) if val.is_void()) {
            // Allows a semicolon after the `leave` statement:
            i -= 1;
        }
        if i > 0 {
            if let Some(ExprSpan { expr: Expr::Leave(_), .. }) = exprs.get(i - 1) {
                expr_span.expr = Expr::LeaveBlock(mem::take(exprs));
            }
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn block_end_semicolon_is_opt() {
        with_test_context!(cxt, {
            let r = eval_wrapped!(cxt, r#"
                fn foo() {
                    for 3 {
                        nop;
                    }
                    print(1);
                }
                fn bar() {
                    if true {
                        nop;
                    }
                    print(1);
                }
                fn baz() {
                    if true {
                        nop;
                    } else {
                        nop;
                    }
                    print(1);
                }
                a = 2;
            "#);
            assert!(r.is_ok());
        })
    }
}

use nom::error::ErrorKind;

use crate::eval::{AssignMode, AssignPattern, AssignTarget, DictAssignable, Expr, ExprSpan};
use crate::parse::UError;
use crate::parse::sub_parsers::{
    curly_brackets_block,
    flow_control_expression,
    function_definition,
    oper_prior_0,
    parentheses_block,
    spaced,
    variable_expr,
};
use crate::value::{HashableValue, StrOperand};
use crate::{context::Container, eval::Assignable};
use super::{Input, UParser, UResult, identifier};

use super::nom::{
    tag,
    map,
    alt,
    opt,
    tuple,
    preceded,
    terminated,
    multispace0,
    delimited,
    separated_list1,
};
use nom::{
    Err,
    error::ParseError,
};

/// Parses anything assignable.
pub fn assignable(container: &Container, mode: AssignMode) -> impl UParser<'_, Assignable> {
    alt((
        assignable_indexed_expr(container, mode),
        assignable_variable(mode),
        assignable_pattern(container, mode),
    ))
}

/// Parses anything which could be put before an equal sign `=`, for a `let` statement.
pub fn assignable_let(container: &Container, mode: AssignMode) -> impl UParser<'_, Assignable> {
    alt((
        assignable_variable(mode),
        assignable_pattern(container, mode),
    ))
}

/// Parses anything which could be assigned directly, like a variable identifier, or an indexed
/// expression.
pub fn assignable_value(container: &Container, mode: AssignMode) -> impl UParser<'_, Assignable> {
    alt((
        assignable_indexed_expr(container, mode),
        assignable_variable(mode),
    ))
}

/// Parses a destructuring pattern.
pub fn assignable_pattern(container: &Container, mode: AssignMode) -> impl UParser<'_, Assignable> {
    alt((
        assignable_pattern_array(container, mode),
        assignable_pattern_dict(container, mode),
    ))
}

/// Parses an array destructuring pattern.
pub fn assignable_pattern_array<'a>(container: &'a Container, mode: AssignMode) -> impl UParser<'a, Assignable> {
    map(
        delimited(
            spaced(tag("[")),
            terminated(
                separated_list1(
                    spaced(tag(",")),
                    closure!(assignable(container, mode)),
                ),
                opt(spaced(tag(",")))
            ),
            spaced(tag("]")),
        ),
        move |assignables| {
            Assignable::new(
                mode,
                AssignTarget::Pattern(AssignPattern::Array(assignables))
            )
        }
    )
}

/// Parses a dict destructuring pattern.
pub fn assignable_pattern_dict<'a>(
    container: &'a Container,
    mode: AssignMode
) -> impl UParser<'a, Assignable> {
    map(
        delimited(
            spaced(tag("{")),
            terminated(
                separated_list1(
                    spaced(tag(",")),
                    closure!(assignable_pattern_dict_entry(container, mode)),
                ),
                opt(spaced(tag(",")))
            ),
            spaced(tag("}")),
        ),
        move |assignables| {
            Assignable::new(
                mode,
                AssignTarget::Pattern(AssignPattern::Dict(assignables))
            )
        }
    )
}

/// Parses an entry in a dict destructuring pattern.
pub fn assignable_pattern_dict_entry(
    container: &Container,
    mode: AssignMode,
) -> impl UParser<'_, DictAssignable> {
    alt((
        map(
            tuple((
                identifier,
                spaced(tag("as")),
                assignable(container, mode),
            )),
            |(key, _as, assignable)| {
                DictAssignable {
                    assignable,
                    key: HashableValue::Str(StrOperand::from(key.to_string())),
                }
            }
        ),
        map(
            identifier,
            move |key| {
                DictAssignable {
                    assignable: Assignable::new(mode, AssignTarget::Variable(key.to_string())),
                    key: HashableValue::Str(StrOperand::from(key.to_string())),
                }
            }
        ),
    ))
}

/// Parses a variable identifier as an assignable.
pub fn assignable_variable<'a>(mode: AssignMode) -> impl UParser<'a, Assignable> {
    map(identifier, move |id| {
        Assignable::new(mode, AssignTarget::Variable(id.to_string()))
    })
}

/// Parses an expression for which the last operation is an indexation, like `arr[2]`,
/// `myfunc()[3][5]`...
pub fn assignable_indexed_expr<'a>(container: &'a Container, mode: AssignMode) -> impl UParser<'a, Assignable> {
    move |input: Input<'a>| -> UResult<'a, Assignable> {
        let (remain, expr_span) = oper_prior_0(
            container,
            assignable_single_expr(container)
        )(input)?;
        if let ExprSpan { expr: Expr::Index(expr, index), .. } = expr_span {
            let assignable = Assignable::new(mode, AssignTarget::IndexedExpr(expr, index));
            Ok((remain, assignable))
        } else {
            Err(Err::Error(UError::from_error_kind(input, ErrorKind::Fail)))
        }
    }
}

/// Replaces the function [`super::expressions::single_expr()`], by removing elements forbidden in
/// an assignable expression.
pub fn assignable_single_expr<'a>(container: &'a Container) -> impl UParser<'a, ExprSpan> {
    preceded(
        multispace0,
        alt((
            closure!(flow_control_expression(container)),
            closure!(function_definition(container)),
            variable_expr,
            closure!(parentheses_block(container)),
            closure!(curly_brackets_block(container)),
        )),
    )
}

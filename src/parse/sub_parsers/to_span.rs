//! Special parser combinators which can transform the [`Expr`] result of a parser into a
//! [`ExprSpan`] result.

use super::nom::map;

use crate::eval::{ExprSpan, Expr};
use crate::parse::Span;
use super::{UParser, Input};

/// Wraps an `Expr` parser to transform the `Expr` result to a `ExprSpan` result (containing source
/// location information).
pub fn to_expr_span<'a>(mut parser: impl UParser<'a, Expr>) -> impl UParser<'a, ExprSpan> {
    move |input: Input<'a>| {
        let (remaining, expr) = parser(input)?;
        let mut expr_span = ExprSpan::new(expr);
        expr_span.set_span(Span::from_inputs(input, remaining));
        Ok((remaining, expr_span))
    }
}

/// Wraps an `Expr` parser to transform the `Expr` result to a `ExprSpan` result (containing source
/// location information).
pub fn with_span<'a, O>(mut parser: impl UParser<'a, O>) -> impl UParser<'a, (Span, O)> {
    move |input: Input<'a>| {
        let (remaining, output) = parser(input)?;
        let span = Span::from_inputs(input, remaining);
        Ok((remaining, (span, output)))
    }
}

/// Shortcut: combines `to_expr_span()` with a nom `map()` combinator.
pub fn to_expr_span_map<'a, V, F>(parser: impl UParser<'a, V>, mapper: F) -> impl UParser<'a, ExprSpan>
where
    F: Fn(V) -> Expr
{
    to_expr_span(map(parser, mapper))
}

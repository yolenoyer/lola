//! Parsers for function definitions and function calls.

use std::rc::Rc;

use super::nom::{
    tag,
    pair,
    tuple,
    preceded,
    delimited,
    separated_list0,
    separated_list1,
    map,
    map_res,
    alt,
    opt,
};

use crate::context::Container;
use crate::eval::{
    ArgChecker,
    arg_checker as arg,
    ExprSpan,
    Expr,
    function::FunctionArg,
};
use crate::parse::{Span, Input};
use crate::util::NoDebug;

use super::{is_keyword, opt_comma};
use super::{UParser, UError};
use super::{expression, curly_brackets_block};
use super::{spaced, identifier, word, var_type};
use super::{to_expr_span, to_expr_span_map};

/// Parses a function call parenthese block.
pub fn call_arguments<'a>(container: &'a Container) -> impl UParser<(Vec<ExprSpan>, Span)> {
    move |input: Input<'a>| {
        let (remain_input, output) = tuple((
            spaced(tag("(")),
            separated_list0(
                spaced(tag(",")),
                expression(container),
            ),
            opt_comma(tag(")")),
        ))(input)?;
        Ok((remain_input, (output.1, Span::from_inputs(input, remain_input))))
    }
}

/// Parses an argument definition.
fn argument_definition(container: &Container) -> impl UParser<FunctionArg> {
    map(
        pair(
            identifier,
            opt_var_type(container),
        ),
        |(id, opt_vt)| FunctionArg::new(
            id.to_string(),
            opt_vt,
        )
    )
}

fn opt_var_type(container: &Container) -> impl UParser<Option<Rc<dyn ArgChecker>>> {
    map(
        opt(preceded(
            spaced(tag(":")),
            pair(
                opt(spaced(tag("?"))),
                separated_list1(
                    spaced(tag("|")),
                    var_type(container),
                )
            )
        )),
        |opt| {
            opt.map(|(question, var_types)| {
                macro_rules! rc {
                    ($v:expr) => { Rc::new($v) }
                }

                let mut checkers = var_types.into_iter().map(|vt| {
                    let checker: Rc<dyn ArgChecker> = rc!(arg::has_type(vt));
                    checker
                });
                let first_checker = checkers.next().unwrap();
                let mut checker = checkers.fold(first_checker, |acc, checker| {
                    rc!(arg::dyn_or(acc, checker))
                });
                if question.is_some() {
                    checker = rc!(arg::dyn_or(rc!(arg::is_void), checker));
                }
                checker
            })
        }
    )
}

/// Parses a function definition.
pub fn function_definition<'a>(container: &'a Container) -> impl UParser<ExprSpan> {
    to_expr_span(map_res(
        pair(
            preceded(
                spaced(word("fn")),
                tuple((
                    opt(identifier),
                    delimited(
                        spaced(tag("(")),
                        spaced(separated_list0(
                            spaced(tag(",")),
                            closure!(argument_definition(container)),
                        )),
                        opt_comma(tag(")")),
                    ),
                    opt_var_type(container),
                )),
            ),
            alt((
                curly_brackets_block(container),
                to_expr_span_map(
                    expression(container),
                    |expr_span| Expr::ScopedBlock(vec![expr_span]),
                ),
            ))
        ),
        |((opt_name, args, opt_ret_type), expr_span)| {
            match args.iter().find(|arg| is_keyword(arg.name())) {
                // TODO: this error is working, but is not shown (it is replaced by "Bad expression
                // sequence"):
                Some(arg) => Err(nom::Err::Failure(UError::ForbiddenVarName(arg.name().clone(), Span::default(/*TODO*/)))),
                None => Ok(Expr::FuncDef(
                    opt_name.map(|name| name.to_string()),
                    args,
                    NoDebug(opt_ret_type),
                    Box::new(expr_span)
                ))
            }
        }
    ))
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_user_func_define() {
        with_test_context!(cxt, {
            let r = eval_wrapped!(cxt, "let f = fn(a, b) { a + b }");
            assert!(r.is_ok());
        })
    }

    #[test]
    fn test_user_func_call() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a, b) { a + b }");
            let r = eval!(cxt, "f(2, 3)");
            assert_eq!(r, int!(5));
        })
    }

    #[test]
    fn test_user_func_arg_count() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a, b) { a + b }");
            let r = eval_err!(cxt, "f(2, 3, 4)");
            assert_eq!(r, err!(Expr:Call:ArgList:WrongArgCount));
        })
    }

    #[test]
    fn test_user_func_arg_type() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a: string) a");
            let r = eval_err!(cxt, "f(2)");
            assert_eq!(r, err!(Expr:Call:ArgList:Arg:MismatchTypes));
        })
    }

    #[test]
    fn test_user_func_arg_number_type() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a: number(length)) a");
            assert!(eval_wrapped!(cxt, "f(2m)").is_ok());
            let r = eval_err!(cxt, "f(2)");
            assert_eq!(r, err!(Expr:Call:ArgList:Arg:MismatchTypes));

            eval!(cxt, "f = fn(a: number(length/time)) a");
            assert!(eval_wrapped!(cxt, "f(2m)").is_err());
            assert!(eval_wrapped!(cxt, "f(2m/s)").is_ok());

            eval!(cxt, "f = fn(a: number(length.time-2)) a");
            assert!(eval_wrapped!(cxt, "f(2m/s)").is_err());
            assert!(eval_wrapped!(cxt, "f(2m/s2)").is_ok());

            eval!(cxt, "f = fn(a: {length.time-2}) a");
            assert!(eval_wrapped!(cxt, "f(2m/s)").is_err());
            assert!(eval_wrapped!(cxt, "f(2m/s2)").is_ok());
        })
    }

    #[test]
    fn test_user_func_run_error() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a, b) { a + b }");
            let r = eval_err!(cxt, "f(2h, 3km)");
            assert_eq!(r, err!(Expr:Operation:MismatchUnits));
        })
    }

    #[test]
    fn test_user_func_single_expr() {
        with_test_context!(cxt, {
            eval!(cxt, "let f = fn(a, b) a + b; let c = 78");
            let r = eval!(cxt, "f(2, 3)");
            assert_eq!(r, int!(5));
        })
    }

    #[test]
    fn test_named_function() {
        with_test_context!(cxt, {
            eval!(cxt, "fn f(a, b) a + b");
            let r = eval!(cxt, "f(2, 3)");
            assert_eq!(r, int!(5));
        })
    }
}

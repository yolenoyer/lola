//! Parser for a variable type (like in a function argument definition for example).

use super::nom::{
    tag,
    preceded,
    delimited,
    alt,
    map,
    opt,
};

use crate::context::Container;
use crate::eval::VarType;

use super::UParser;
use super::{word, spaced, compound_quantity};

/// Parses a variable type.
pub fn var_type(_container: &Container) -> impl UParser<VarType> {
    alt((
        map(word("void"), |_| VarType::Void),
        map(
            preceded(
                word("number"),
                opt(delimited(
                    spaced(tag("(")),
                    compound_quantity(),
                    spaced(tag(")")),
                ))
            ),
            VarType::Num
        ),
        map(
            delimited(
                spaced(tag("{")),
                compound_quantity(),
                spaced(tag("}")),
            ),
            |cq| VarType::Num(Some(cq))
        ),
        map(word("int"),      |_| VarType::Int),
        map(word("boolean"),  |_| VarType::Boolean),
        map(word("string"),   |_| VarType::Str),
        map(word("function"), |_| VarType::Func),
        map(word("array"),    |_| VarType::Array),
        map(word("dict"),     |_| VarType::Dict),
    ))
}

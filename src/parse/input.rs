//! Implements the [`Input`] struct.

use std::ops::{Range, RangeFrom, RangeFull, RangeTo};
use std::str::{CharIndices, Chars};
use std::fmt;

use nom;
use nom::{
    IResult,
    Offset,
    Slice,
    InputTake,
    InputIter,
    InputLength,
    Compare,
    InputTakeAtPosition,
    Needed,
};
use nom::error::{ParseError, ErrorKind};

/// Represents an input for the nom parser.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Input<'a> {
    pub text: &'a str,
    pub offset: usize,
    pub line: usize,
    pub column: usize,
    pub parsed_input_index: usize,
}

impl<'a> Input<'a> {
    pub fn new(text: &'a str, parsed_input_index: usize) -> Self {
        Self {
            text,
            offset: 0,
            line: 1,
            column: 1,
            parsed_input_index,
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.text.len()
    }

    /// Checks if the input is empty.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Returns the distance in bytes with another `Input` instance.
    ///
    /// Will panic if `after` is not after self.
    pub fn dist(&self, after: Input<'_>) -> usize {
        after.text.as_ptr() as usize - self.text.as_ptr() as usize
    }
}

impl<'a> fmt::Display for Input<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.text)
    }
}

//############################################################
//                         Offset
//############################################################

impl<'a> Offset for Input<'a> {
  fn offset(&self, second: &Self) -> usize {
    let fst = self.text.as_ptr();
    let snd = second.text.as_ptr();

    snd as usize - fst as usize
  }
}

//############################################################
//                         Slice
//############################################################

macro_rules! impl_fn_slice {
    ( $ty:ty ) => {
        fn slice(&self, range: $ty) -> Self {
            let slice = &self.text[range];
            let slice_start = slice.as_ptr() as usize - self.text.as_ptr() as usize;
            let before_slice = &self.text[0..slice_start];

            let mut line = self.line;
            let mut column = self.column;
            let mut offset = self.offset;

            for c in before_slice.chars() {
                if c == '\n' {
                    line += 1;
                    column = 0;
                }
                column += 1;
                offset += 1;
            }

            Self {
                text: slice,
                line,
                column,
                offset,
                parsed_input_index: self.parsed_input_index,
            }
        }
    };
}

macro_rules! slice_range_impl {
    ( [ $for_type:ident ], $ty:ty ) => {
        impl<'a, $for_type> Slice<$ty> for [$for_type] {
            impl_fn_slice!($ty);
        }
    };
    ( $for_type:ty, $ty:ty ) => {
        impl<'a> Slice<$ty> for $for_type {
            impl_fn_slice!($ty);
        }
    };
}

macro_rules! slice_ranges_impl {
    ( [ $for_type:ident ] ) => {
        slice_range_impl! {[$for_type], Range<usize>}
        slice_range_impl! {[$for_type], RangeTo<usize>}
        slice_range_impl! {[$for_type], RangeFrom<usize>}
        slice_range_impl! {[$for_type], RangeFull}
    };
    ( $for_type:ty ) => {
        slice_range_impl! {$for_type, Range<usize>}
        slice_range_impl! {$for_type, RangeTo<usize>}
        slice_range_impl! {$for_type, RangeFrom<usize>}
        slice_range_impl! {$for_type, RangeFull}
    };
}

slice_ranges_impl! {Input<'a>}

//############################################################
//                         InputTake
//############################################################

impl<'a> InputTake for Input<'a> {
    #[inline]
    fn take(&self, count: usize) -> Self {
        self.slice(..count)
    }

    // return byte index
    #[inline]
    fn take_split(&self, count: usize) -> (Self, Self) {
        ( self.slice(count..), self.slice(..count) )
    }
}

//############################################################
//                         InputIter
//############################################################

impl<'a> InputIter for Input<'a> {
    type Item = char;
    type Iter = CharIndices<'a>;
    type IterElem = Chars<'a>;

    #[inline]
    fn iter_indices(&self) -> Self::Iter {
        self.text.char_indices()
    }

    #[inline]
    fn iter_elements(&self) -> Self::IterElem {
        self.text.chars()
    }
    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        for (o, c) in self.text.char_indices() {
            if predicate(c) {
                return Some(o);
            }
        }
        None
    }

    #[inline]
    fn slice_index(&self, count: usize) -> Result<usize, Needed> {
        let mut cnt = 0;
        for (index, _) in self.text.char_indices() {
            if cnt == count {
                return Ok(index);
            }
            cnt += 1;
        }
        if cnt == count {
            return Ok(self.len());
        }
        Err(Needed::Unknown)
    }
}

//############################################################
//                         InputLength
//############################################################

impl<'a> InputLength for Input<'a> {
  #[inline]
  fn input_len(&self) -> usize {
    self.len() // TODO: not utf8 len?
  }
}

//############################################################
//                         Compare
//############################################################

impl<'a> Compare<&str> for Input<'a> {
    #[inline(always)]
    fn compare(&self, t: &str) -> nom::CompareResult {
        self.text.compare(t)
    }

    #[inline(always)]
    fn compare_no_case(&self, t: &str) -> nom::CompareResult {
        self.text.compare_no_case(t)
    }
}

//############################################################
//                         InputTakeAtPosition
//############################################################

impl<'a> InputTakeAtPosition for Input<'a> {
    type Item = char;

    fn split_at_position<P, E: ParseError<Self>>(&self, predicate: P) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.text.find(predicate) {
            Some(i) => Ok((self.slice(i..), self.slice(..i))),
            None => Err(nom::Err::Incomplete(nom::Needed::new(1))),
        }
    }

    fn split_at_position1<P, E: ParseError<Self>>(
        &self,
        predicate: P,
        e: ErrorKind,
    ) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.text.find(predicate) {
            Some(0) => Err(nom::Err::Error(E::from_error_kind(*self, e))),
            Some(i) => Ok((self.slice(i..), self.slice(..i))),
            None => Err(nom::Err::Incomplete(nom::Needed::new(1))),
        }
    }

    fn split_at_position_complete<P, E: ParseError<Self>>(
        &self,
        predicate: P,
    ) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.text.find(predicate) {
            Some(i) => Ok((self.slice(i..), self.slice(..i))),
            None => Ok(self.take_split(self.input_len())),
        }
    }

    fn split_at_position1_complete<P, E: ParseError<Self>>(
        &self,
        predicate: P,
        e: ErrorKind,
    ) -> IResult<Self, Self, E>
    where
        P: Fn(Self::Item) -> bool,
    {
        match self.text.find(predicate) {
            Some(0) => Err(nom::Err::Error(E::from_error_kind(*self, e))),
            Some(i) => Ok((self.slice(i..), self.slice(..i))),
            None => {
                if self.is_empty() {
                    Err(nom::Err::Error(E::from_error_kind(*self, e)))
                } else {
                    Ok(self.take_split(self.input_len()))
                }
            }
        }
    }
}

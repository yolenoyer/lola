//! Basic types used for nom parsers.

use std::fmt;

use nom::error::{ParseError, ErrorKind, FromExternalError};
use nom::IResult;

use super::{Span, Input};

/// Alias used for all parse results.
pub type UResult<'a, V> = IResult<Input<'a>, V, UError>;

/// Trait alias for all parsers.
pub trait UParser<'a, V>: FnMut(Input<'a>) -> UResult<'a, V> {}

impl<'a, V, F> UParser<'a, V> for F
    where F: FnMut(Input<'a>) -> UResult<'a, V>
{}

/// Custom error for parsers.
#[derive(Debug, PartialEq)]
pub enum UError {
    UndefinedBasicUnit(String, Span),
    UnterminatedExpr(String),
    ForbiddenVarName(String, Span),
    BadExprSequence(Span),
    NomError(ErrorKind),
    ExternalError(ErrorKind),
    CannotAssign(Span),
    InvalidDictIndex, // TODO: span
}

impl fmt::Display for UError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            UError::UndefinedBasicUnit(u, span) => write!(f, "{}: Undefined basic unit: '{}'", span, u),
            UError::UnterminatedExpr(s)         => write!(f, "Unterminated expression: '{}'", s),
            UError::ForbiddenVarName(n, span)   => write!(f, "{}: Forbidden variable name: '{}'", span, n),
            UError::BadExprSequence(span)       => write!(f, "{}: Bad expression sequence", span),
            UError::NomError(_)                 => write!(f, "Unable to parse the expression"),
            UError::ExternalError(_)            => write!(f, "External error"),
            UError::CannotAssign(span)          => write!(f, "{}: Unable to assign to this expression", span),
            UError::InvalidDictIndex            => write!(f, "Invalid dict index"),
        }
    }
}

impl ParseError<&str> for UError
{
    fn from_error_kind(_: &str, kind: ErrorKind) -> Self {
        UError::NomError(kind)
    }

    fn append(_: &str, _: ErrorKind, other: Self) -> Self {
        other
    }
}

impl<'a> ParseError<Input<'a>> for UError
{
    fn from_error_kind(_: Input<'a>, kind: ErrorKind) -> Self {
        UError::NomError(kind)
    }

    fn append(_: Input<'a>, _: ErrorKind, other: Self) -> Self {
        other
    }
}

impl<I, E> FromExternalError<I, E> for UError {
    fn from_external_error(_input: I, kind: ErrorKind, _e: E) -> Self {
        UError::ExternalError(kind)
    }
}

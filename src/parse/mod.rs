//! The lola parser.

mod types;
pub mod sub_parsers;
mod input;
mod span;
mod parsed_input;

use std::path::PathBuf;

use crate::context::Container;
use crate::eval::ExprSpan;
use sub_parsers::block;
#[doc(hidden)]
pub use types::UError;
pub use span::*;
pub use input::*;
pub use parsed_input::*;

/// A list of all keywords in the language. This is used to forbid the creation of variables having
/// one of these names.
const KEYWORDS: &[&str] = &[
    "let",
    "if",
    "else",
    "while",
    "loop",
    "for",
    "in",
    "break",
    "continue",
    "return",
    "leave",
    "fn",
    "true",
    "false",
    "throw",
    "try",
    "catch",
];

/// Checks if the given string is a language keyword.
fn is_keyword(s: &str) -> bool {
    KEYWORDS.contains(&s)
}

/// Parses a whole expression or sequence of expressions, with an addtional error if some input
/// remains unconsumed after parsing, and by returning the parse result instead of a parser
/// functor.
pub fn parse_expr(input: &str, container: &Container, source: Option<(String, Option<PathBuf>)>) -> Result<ExprSpan, nom::Err<UError>> {
    let index = container.push_parsed_input(input, source);

    let input = Input::new(input, index);
    let (input, output) = block(container)(input)?;
    if !input.text.is_empty() {
        Err(nom::Err::Error(UError::UnterminatedExpr(input.to_string())))
    } else {
        Ok(output)
    }
}

/// Displays a parse error.
pub fn display_error(err: &nom::Err<UError>) -> String {
    match err {
        nom::Err::Error(uerror) => {
            uerror.to_string()
        },
        nom::Err::Failure(uerror) => {
            uerror.to_string()
        },
        _ => "Unable to parse the expression".to_string(),
    }
}

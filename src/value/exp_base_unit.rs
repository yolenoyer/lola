//! Implements the [`ExpBaseUnit`] struct.

use std::rc::{Weak, Rc};
use std::cell::RefCell;
use std::fmt;
use core::ops::Neg;

use getset::Getters;
use rug::{Float, float::Round};
use rug::ops::{MulAssignRound, NegAssign};

use crate::value::{BaseUnit, Quantity};

/// Represents a base unit (e.g. meters, seconds) raised to a given power.
/// It is the base component of the [`Unit`] structure.
///
/// [`Unit`]: crate::value::Unit
#[derive(Clone, Getters)]
pub struct ExpBaseUnit {
    #[getset(get = "pub")]
    base_unit: Weak<BaseUnit>,

    #[getset(get = "pub")]
    exp: Float,
}

impl fmt::Debug for ExpBaseUnit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let bu = self.base_unit.upgrade().unwrap();
        write!(f, "{}({})", bu.name(), self.exp)
    }
}

impl ExpBaseUnit {
    /// Creates a new [`ExpBaseUnit`] instance, to be added to a [`Unit`] instance.
    ///
    /// [`Unit`]: crate::value::Unit
    pub fn new(base_unit: &Rc<BaseUnit>, exp: Float) -> Self {
        Self {
            base_unit: Rc::downgrade(base_unit),
            exp,
        }
    }

    /// Get the exponent as a mutable reference.
    pub fn exp_mut(&mut self) -> &mut Float {
        &mut self.exp
    }

    /// Creates a new [`ExpBaseUnit`] instance by cloning `self` but replacing its exponent.
    pub fn with_exp(&self, exp: Float) -> ExpBaseUnit {
        Self {
            base_unit: self.base_unit.clone(),
            exp,
        }
    }

    /// Creates a new [`ExpBaseUnit`] instance by cloning `self` excepted that the new
    /// exponent is multiplied by `n`.
    pub fn mul_exp(&self, mut n: Float) -> ExpBaseUnit {
        n.mul_assign_round(&self.exp, Round::Up);
        self.with_exp(n)
    }

    /// Creates a new [`ExpBaseUnit`] instance by cloning `self` excepted that the new
    /// exponent is negated.
    pub fn neg(&self) -> ExpBaseUnit {
        self.with_exp(self.exp.clone().neg())
    }

    /// Creates a new [`ExpBaseUnit`] instance by cloning `self` excepted that the new
    /// exponent is negated if `dir` is `-1`.
    pub fn neg_dir(&self, dir: i32) -> ExpBaseUnit {
        let mut exp = self.exp.clone();
        if dir == -1 {
            exp.neg_assign();
        }
        self.with_exp(exp)
    }

    /// Helper to access the upgraded version of the weak reference to the base unit.
    pub fn strong_base_unit(&self) -> Rc<BaseUnit> {
        self.base_unit.upgrade().unwrap()
    }

    /// Helper to access the weak reference of the unit's quantity.
    pub fn weak_quantity(&self) -> Weak<RefCell<Quantity>> {
        Rc::downgrade(&self.strong_base_unit().strong_quantity())
    }
}

impl PartialEq for ExpBaseUnit {
    fn eq(&self, other: &Self) -> bool {
        self.exp == other.exp && self.base_unit.ptr_eq(&other.base_unit)
    }
}

impl Eq for ExpBaseUnit {}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! new_ebu {
        ($cont:expr, $u:expr, $exp:expr) => {
            ( ExpBaseUnit::new(&base_unit!($cont, $u), float_parsed!($exp)) )
        };
    }

    #[test]
    fn ebu_mul() {
        with_test_container!(container, {
            let ebu = new_ebu!(container, "m", 2);
            let ebu = ebu.mul_exp(float_parsed!(-1));
            assert_eq!(ebu.exp, float_parsed!(-2));
        })
    }

    #[test]
    fn ebu_bu_is_well_linked() {
        with_test_container!(container, {
            let bu = &base_unit!(container, "km");
            let ebu = ExpBaseUnit::new(bu, float_parsed!(2));
            assert!(Rc::ptr_eq(bu, &ebu.strong_base_unit()));
        })
    }

    #[test]
    fn ebu_equality() {
        with_test_container!(container, {
            let ebu1 = new_ebu!(container, "km", 2);
            assert!(ebu1 == ebu1);

            let ebu2 = new_ebu!(container, "km", 2);
            assert!(ebu1 == ebu2);

            let ebu3 = new_ebu!(container, "m", 2);
            assert!(ebu1 != ebu3);

            let ebu4 = new_ebu!(container, "km", 3);
            assert!(ebu1 != ebu4);
        })
    }
}

/// Gets a string representation of a [`Value`] with a configurable style.
///
/// [`Value`]: crate::value::Value
pub trait Format {
    /// Returns a formatted version of the given object.
    /// If `as_expr` is set to true, then the output must be parsable as a lola expression.
    /// NOTE: this last sentence is not totally correct, indeed for now the number values are
    /// truncated with the ellipsis character (`…`), which will not be parsable by lola.
    fn format(&self, config: &FormatConfig, as_expr: bool) -> String;

    /// The same as [`format()`], but handles indentation.
    fn format_indented(&self, config: &FormatConfig, as_expr: bool, indent: usize) -> String {
        return format!("{}{}", config.get_ident_string(indent), self.format(config, as_expr));
    }
}

/// The style to use when converting values to string. Actually only used for number formatting.
/// The [`Container`] holds an instance of this struct, and it can be configured in live thanks to
/// the RT function `config()`.
///
/// [`Container`]: crate::context::Container
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FormatConfig {
    /// Max number of significant digits to show, before truncating the number.
    pub significant_digits: Option<i32>,
    /// String to append when a number has been truncated.
    pub ellipsis: String,
    /// Format numbers with underscores, like in Rust. The given [`usize`] tells by which size
    /// the digits have to be grouped.
    pub underscores: Option<usize>,
    /// Allows arrays and dicts to be displayed in multiple line format.
    pub multi_line: bool,
    /// Max width before an array or dict is displayed in multiple line format.
    pub max_width: usize,
    /// Indentation of values when an array or a dict is displayed in multiple line format.
    pub indent: usize,
}

impl FormatConfig {
    /// Retrieves the indent string for a given indent.
    #[inline]
    pub fn get_ident_string(&self, indent: usize) -> String {
        " ".repeat(self.indent * indent)
    }
}

impl Default for FormatConfig {
    fn default() -> Self {
        Self {
            significant_digits: Some(7),
            ellipsis: "…".to_string(),
            underscores: Some(3),
            multi_line: true,
            max_width: 60,
            indent: 2,
        }
    }
}

//! Implements the [`VoidOperand`] struct.

use std::fmt;

use crate::value::{
    Value,
    Operand,
    IntoValue,
    OperationResult,
    Format,
    FormatConfig,
};

/// A void operand.
#[derive(Debug, Clone, PartialEq, Default, Hash)]
pub struct VoidOperand ();

impl VoidOperand {
    pub fn new() -> VoidOperand {
        VoidOperand::default()
    }
}

impl Operand for VoidOperand {
    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        match rhs {
            Value::Void(_) => Ok(v_true!()),
            _ => Ok(v_false!()),
        }
    }
}

impl fmt::Display for VoidOperand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(void)")
    }
}

impl Format for VoidOperand {
    fn format(&self, _config: &FormatConfig, _as_expr: bool) -> String {
        "(void)".to_string()
    }
}

impl IntoValue for VoidOperand {
    fn into_value(self) -> Value {
        Value::Void(self)
    }
}

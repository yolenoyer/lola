//! Implements the [`BooleanOperand`] struct.

use std::fmt;

use crate::value::{
    Value,
    Operand,
    IntoValue,
    OperationResult,
    Format,
    FormatConfig,
};

/// A boolean operand.
#[derive(Debug, Clone, PartialEq, Eq, Default, Hash)]
pub struct BooleanOperand ( pub bool );

impl BooleanOperand {
    /// Returns the underlying data.
    #[inline]
    pub fn val(&self) -> bool {
        self.0
    }

    /// Checks if the boolean is true.
    #[inline]
    pub fn is_true(&self) -> bool {
        self.0
    }

    /// Checks if the boolean is false.
    #[inline]
    pub fn is_false(&self) -> bool {
        !self.0
    }

    /// Returns the negative operand of this value.
    #[inline]
    pub fn negate(&self) -> BooleanOperand {
        BooleanOperand ( !self.0 )
    }

    /// Helps to implement the trait [`Operand`] without repeating the same code multiple times.
    fn bool_oper(&self, rhs: &Value, func: impl Fn(&BooleanOperand) -> bool) -> OperationResult {
        match rhs {
            Value::Boolean(rhs) => Ok(boolean!(func(rhs))),
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }
}

impl Operand for BooleanOperand {
    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        self.bool_oper(rhs, |rhs| self.0 == rhs.0)
            .or_else(|_| Ok(v_false!()))
    }

    /// Performs the `&&` operation.
    fn and(&self, rhs: &Value) -> OperationResult {
        self.bool_oper(rhs, |rhs| self.0 && rhs.0)
    }

    /// Performs the `||` operation.
    fn or(&self, rhs: &Value) -> OperationResult {
        self.bool_oper(rhs, |rhs| self.0 || rhs.0)
    }

    /// Performs the unary `!` operation.
    fn not(&self) -> OperationResult {
        Ok(boolean!(!self.0))
    }
}

impl fmt::Display for BooleanOperand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Format for BooleanOperand {
    fn format(&self, _config: &FormatConfig, _as_expr: bool) -> String {
        format!("{}", self.0)
    }
}

impl From<bool> for BooleanOperand {
    fn from(v: bool) -> BooleanOperand {
        BooleanOperand (v)
    }
}

impl IntoValue for BooleanOperand {
    fn into_value(self) -> Value {
        Value::Boolean(self)
    }
}

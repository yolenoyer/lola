use std::{
    cell::RefCell,
    collections::{HashMap, hash_map::Entry},
    convert::TryFrom,
    error::Error,
    fmt,
    ops::Deref,
    rc::Rc,
};

use itertools::Itertools;

use crate::{
    eval::ExprError,
    value::{
        Format,
        FormatConfig,
        IntoValue,
        OperationError,
        OperationResult,
        Value,
        ValueIterator,
        VoidOperand,
    },
};

use super::{ArrayOperand, BooleanOperand, IntOperand, Operand, StrOperand};

pub type DictHashMap = HashMap<HashableValue, Value>;

/// A dict operand (key/value pairs).
#[derive(Debug, Clone, PartialEq, Default)]
pub struct DictOperand(Rc<RefCell<DictHashMap>>);

impl DictOperand {
    pub fn new() -> Self {
        Self::default()
    }

    /// Tries to insert a new value in the dict. The key value must be hashable (either a string,
    /// an int or a bool).
    pub fn try_insert(&mut self, key: Value, value: Value) -> DictResult {
        let hash_value = HashableValue::try_from(key)?;
        let mut dict = self.0.borrow_mut();
        dict.insert(hash_value, value);
        Ok(())
    }

    /// Finds all the string keys which start by the given prefix.
    pub fn keys_starting_with(&self, prefix: &str) -> Vec<String> {
        let dict = self.0.borrow();
        dict.keys()
            .filter_map(|k| {
                if let HashableValue::Str(s) = k {
                    let s = s.val_ref();
                    if s.starts_with(prefix) {
                        return Some(s.to_owned())
                    }
                }
                None
            })
            .collect::<Vec<_>>()
    }

    /// Returns the inner element.
    #[inline]
    pub fn inner(&self) -> &Rc<RefCell<DictHashMap>> {
        &self.0
    }

    /// Returns a `Vec` of array values, each of them having two elements: the key name, then its
    /// value.
    pub fn get_key_value_pairs(&self) -> Vec<Value> {
        let dict = self.0.borrow();
        dict.iter()
            .map(|(key, value)| {
                ArrayOperand::from([
                    key.to_owned().into_value(),
                    value.clone()
                ]).into_value()
            })
            .collect::<Vec<_>>()
    }
}

impl Operand for DictOperand {
    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        match rhs.get_dict() {
            Some(rhs_dict) => {
                let self_dict = self.0.borrow();
                let rhs_dict = rhs_dict.0.borrow();
                Ok(boolean!(*self_dict == *rhs_dict))
            },
            None => Ok(v_false!())
        }
    }

    /// Performs the `+` operation (dict merge).
    fn add(&self, rhs: &Value) -> OperationResult {
        match rhs.get_dict() {
            Some(other_dict) => {
                let mut new_dict = self.0.borrow().clone();
                let other_dict = other_dict.0.borrow().clone();
                new_dict.extend(other_dict.into_iter());
                Ok(DictOperand::from(new_dict).into_value())
            },
            None => Err(err!(Operation:MismatchTypes))
        }
    }

    /// Performs the `+=` operation (dict merge).
    fn set_add(&mut self, rhs: &Value) -> OperationResult {
        match rhs.get_dict() {
            Some(other_dict) => {
                let mut dict = self.0.borrow_mut();
                let other_dict = other_dict.0.borrow().clone();
                dict.extend(other_dict.into_iter());
                Ok(self.clone().into_value())
            },
            None => Err(err!(Operation:MismatchTypes))
        }
    }

    /// Performs the `[]` operation.
    fn index(&self, key: &Value) -> OperationResult {
        let hash_value = HashableValue::try_from(key.clone())?;
        match self.0.borrow().get(&hash_value) {
            Some(value) => Ok(value.clone()),
            None => Err(err!(Operation:Dict:DictKeyNotFound(hash_value))),
        }
    }

    /// Calls the given closure by giving to it a mutable reference to the dict value indexed by
    /// the given value (value must be a [`hashable value`]).
    ///
    /// [`hashable value`]: HashableValue
    fn get_indexed_mut(
        &self,
        key: &Value,
        f: &dyn Fn(&mut Value) -> OperationResult
    ) -> Result<OperationResult, OperationError>
    {
        let hash_value = HashableValue::try_from(key.clone())?;
        let mut dict = self.0.borrow_mut();
        match dict.entry(hash_value) {
            Entry::Occupied(mut entry) => {
                let value_mut = entry.get_mut();
                Ok(f(value_mut))
            }
            Entry::Vacant(entry) => {
                let value_mut = entry.insert(Value::Void(VoidOperand::new()));
                Ok(f(value_mut))
            }
        }
    }

    /// Returns an iterator over the dict (key/value pairs).
    #[allow(clippy::needless_collect)]
    fn iterate(&self) -> Result<ValueIterator, OperationError> {
        let key_values = self.get_key_value_pairs();

        Ok(ValueIterator::new(Box::new(key_values.into_iter())))
    }
}

impl Deref for DictOperand {
    type Target = Rc<RefCell<DictHashMap>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl fmt::Display for DictOperand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let values = self.0.borrow().iter()
            .map(|(k, v)| format!("{}: {}", k.to_string(), v.to_string()))
            .collect::<Vec<_>>()
            .join(", ");
        write!(f, "{{ {} }}", values)
    }
}

impl From<DictHashMap> for DictOperand {
    fn from(hm: DictHashMap) -> DictOperand {
        Self(Rc::new(RefCell::new(hm)))
    }
}

impl Format for DictOperand {
    fn format(&self, config: &FormatConfig, as_expr: bool) -> String {
        self.format_indented(config, as_expr, 0)
    }

    fn format_indented(&self, format_config: &FormatConfig, as_expr: bool, indent: usize) -> String {
        const SEPAR: &str = ", ";
        const COLON: &str = ": ";
        const OPEN_SIGN: &str = "{";
        const CLOSE_SIGN: &str = "}";

        macro_rules! tab_size {
            ($indent:expr) => { $indent * format_config.indent };
        }

        let dict = self.0.borrow();

        let mut values_size = dict
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .fold(0, |acc, (k, v)| acc + k.len() + COLON.len() + v.len());
        values_size += 4 + (dict.len().max(1) - 1) * SEPAR.len() + tab_size!(indent);

        let indent_string = format_config.get_ident_string(indent);

        if !format_config.multi_line || values_size <= format_config.max_width {
            let dict = dict
                .iter()
                .map(|(k, v)| {
                    let k = k.format(format_config, as_expr);
                    let v = v.format(format_config, as_expr);
                    format!("{}{}{}", k, COLON, v)
                })
                .join(SEPAR);
            format!("{}{} {} {}", indent_string, OPEN_SIGN, dict, CLOSE_SIGN)
        } else {
            let dict = dict
                .iter()
                .map(|(k, v)| {
                    let k = k.format_indented(format_config, as_expr, indent + 1);
                    let v = v.format_indented(format_config, as_expr, indent + 1);
                    format!("{}{}{}", k, COLON, v.trim_start())
                })
                .join(",\n");
            format!(
                "{}{}\n{}\n{}{}",
                indent_string,
                OPEN_SIGN,
                dict,
                indent_string,
                CLOSE_SIGN
            )
        }
    }
}

/// A subset of [`Value`], containing all the variants which can be hashed.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum HashableValue {
    Int(IntOperand),
    Boolean(BooleanOperand),
    Str(StrOperand),
}

impl HashableValue {
    /// Retrieves the underlying [`Operand`] instance, depending on the type of the value.
    pub fn as_operand_value(&self) -> &dyn Operand {
        match self {
            Self::Int(int_operand)         => int_operand,
            Self::Boolean(boolean_operand) => boolean_operand,
            Self::Str(str_operand)         => str_operand,
        }
    }

    /// Checks if the given value can be converted to a hashable value.
    pub fn can_convert_from(value: &Value) -> bool {
        matches!(
            value,
            Value::Int(_) | Value::Boolean(_) | Value::Str(_)
        )
    }

    /// Retrieves the underlying [`StrOperand`] instance, if the value is a string.
    pub fn get_str(&self) -> Option<&StrOperand> {
        if let Self::Str(s) = self {
            Some(s)
        } else {
            None
        }
    }
}

impl IntoValue for DictOperand {
    fn into_value(self) -> Value {
        Value::Dict(self)
    }
}

impl TryFrom<Value> for HashableValue {
    type Error = DictError;

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        let hashable_value = match value {
            Value::Int(v) => Self::Int(v),
            Value::Boolean(v) => Self::Boolean(v),
            Value::Str(v) => Self::Str(v),
            _ => return Err(err!(Dict:InvalidDictIndex)),
        };

        Ok(hashable_value)
    }
}

impl<S> From<S> for HashableValue
where
    S: Into<String>,
{
    fn from(s: S) -> HashableValue {
        Self::Str(StrOperand::from(s.into()))
    }
}

impl fmt::Display for HashableValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_operand_value().to_string())
    }
}

impl Format for HashableValue {
    fn format(&self, config: &FormatConfig, as_expr: bool) -> String {
        self.as_operand_value().format(config, as_expr)
    }

    fn format_indented(&self, config: &FormatConfig, as_expr: bool, ident: usize) -> String {
        self.as_operand_value().format_indented(config, as_expr, ident)
    }
}

impl IntoValue for HashableValue {
    fn into_value(self) -> Value {
        match self {
            Self::Int(v) => Value::Int(v),
            Self::Boolean(v) => Value::Boolean(v),
            Self::Str(v) => Value::Str(v),
        }
    }
}

pub type DictResult<T = ()> = Result<T, DictError>;

#[derive(Debug, PartialEq, Clone)]
pub enum DictError {
    /// The given value cannot be used as a dict index.
    InvalidDictIndex,
    /// Dict key was not found.
    DictKeyNotFound(HashableValue),
}

impl Error for DictError {}

impl fmt::Display for DictError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::InvalidDictIndex => write!(f, "This value cannot be used as a dict index"),
            Self::DictKeyNotFound(key) => write!(f, "Dict key not found: '{}'", key),
        }
    }
}

impl From<DictError> for OperationError {
    fn from(err: DictError) -> OperationError {
        OperationError::Dict(err)
    }
}

impl From<DictError> for ExprError {
    fn from(err: DictError) -> ExprError {
        ExprError::Dict(err)
    }
}

#[cfg(test)]
mod tests {
    use crate::value::ArrayOperand;

    use super::*;

    #[test]
    fn test_dict_operand_base() {
        let dict = DictOperand::new();

        let key = Value::Int(7.into());
        dict.get_indexed_mut(&key, &|v: &mut Value| {
            *v = Value::Str("hello".into());
            Ok(v.clone())
        }).unwrap().unwrap();
        assert_eq!(dict.index(&key).unwrap(), Value::Str("hello".into()));

        let key = Value::Int(19.into());
        assert_eq!(dict.index(&key), Err(err!(Operation:Dict:DictKeyNotFound(
            HashableValue::Int(IntOperand::from(19))
        ))));

        let key = Value::Array(ArrayOperand::default());
        assert_eq!(dict.index(&key), Err(err!(Operation:Dict:InvalidDictIndex)));
    }

    #[test]
    fn test_dict_eq() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                d1 = { foo: 1, bar: 2 };
                d2 = { foo: 1, bar: 2 };
                d3 = { foo: 1, bar: 45 };
                d4 = { foo: 1, zoo: 2 };
                d5 = { foo: 1 };
            "#);
            assert_eq!(eval!(cxt, "d1 == d2"), v_true!());
            assert_eq!(eval!(cxt, "d1 != d2"), v_false!());
            assert_eq!(eval!(cxt, "d1 == d3"), v_false!());
            assert_eq!(eval!(cxt, "d1 == d4"), v_false!());
            assert_eq!(eval!(cxt, "d1 == d5"), v_false!());
            assert_eq!(eval!(cxt, "d1 == 123"), v_false!());
        });
    }

    #[test]
    fn test_dict_add() {
        with_test_context!(cxt, {
            eval!(cxt, r#"
                d1 = { foo: 1, bar: 2 };
                d2 = { foo: 10, zoo: 45 };
                d3 = { foo: 10, zoo: 45, bar: 2 };
            "#);
            assert_eq!(eval!(cxt, "d1 + d2 == d3"), v_true!());
            assert_eq!(eval!(cxt, "d1"), dict!{
                string!("foo") => int!(1),
                string!("bar") => int!(2),
            });

            eval!(cxt, "d1 += d2");
            assert_eq!(eval!(cxt, "d1"), dict!{
                string!("foo") => int!(10),
                string!("bar") => int!(2),
                string!("zoo") => int!(45),
            });
        });
    }
}

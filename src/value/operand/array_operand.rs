//! Implements the [`ArrayOperand`] struct.

use std::{cell::RefCell, fmt, rc::Rc};

use itertools::Itertools;

use crate::value::{
    Value,
    Operand,
    IntoValue,
    OperationResult,
    OperationError,
    ValueIterator,
    Format,
    FormatConfig,
};

/// An array operand.
#[derive(Debug, Clone, PartialEq, Default)]
pub struct ArrayOperand ( Rc<RefCell<Vec<Value>>> );

impl ArrayOperand {
    /// Retuns the length of the array.
    #[inline]
    pub fn len(&self) -> usize {
        self.0.borrow().len()
    }

    /// Checks if the array is empty.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Gets a reference to the array values.
    #[inline]
    pub fn values(&self) -> Rc<RefCell<Vec<Value>>> {
        self.0.clone()
    }

    /// Concatenates this array with another array.
    pub fn concat(&self, rhs: &Value) -> ArrayOperand {
        let mut new_array = self.0.borrow().clone();
        match rhs.get_array() {
            Some(arr) => new_array.extend(arr.0.borrow().iter().cloned()),
            None      => new_array.push(rhs.clone()),
        };
        ArrayOperand::from(new_array)
    }
}

impl Operand for ArrayOperand {
    /// Performs the `[]` operation.
    fn index(&self, index: &Value) -> OperationResult {
        match index.get_int() {
            Some(n) => {
                let n = n.val();
                if n < 0 || n as usize >= self.len() {
                    Err(err!(Operation:IndexOutOfRange))
                } else {
                    Ok(self.0.borrow()[n as usize].clone())
                }
            },
            None => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Performs the `+` operation (array concatenation).
    fn add(&self, rhs: &Value) -> OperationResult {
        Ok(self.concat(rhs).into_value())
    }

    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        match rhs {
            Value::Array(rhs) => Ok(boolean!(self.0 == rhs.0)),
            _ => Ok(v_false!())
        }
    }

    /// Performs the `+=` operation.
    fn set_add(&mut self, rhs: &Value) -> OperationResult {
        let mut self_array = self.0.borrow_mut();
        match rhs.get_array() {
            Some(arr) => self_array.extend(arr.0.borrow().iter().cloned()),
            None      => self_array.push(rhs.clone()),
        };
        Ok(self.clone().into_value())
    }

    /// Returns an iterator over the value.
    fn iterate(&self) -> Result<ValueIterator, OperationError> {
        let values = self.0.borrow().clone();
        Ok(ValueIterator::new(Box::new(values.into_iter())))
    }

    /// Calls the given closure by giving to it a mutable reference to the array element indexed by
    /// the given value (value must be `int`).
    fn get_indexed_mut(
        &self,
        index: &Value,
        f: &dyn Fn(&mut Value) -> OperationResult
    ) -> Result<OperationResult, OperationError>
    {
        match index.get_int() {
            Some(n) => {
                let n = n.val();
                if n < 0 || n as usize >= self.len() {
                    Err(err!(Operation:IndexOutOfRange))
                } else {
                    let val = &mut self.0.borrow_mut()[n as usize];
                    Ok(f(val))
                }
            },
            None => Err(err!(Operation:MismatchTypes)),
        }
    }
}

impl fmt::Display for ArrayOperand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let values = self.0.borrow().iter()
            .map(|v| v.to_string())
            .collect::<Vec<_>>()
            .join(", ");
        write!(f, "[ {} ]", values)
    }
}

impl Format for ArrayOperand {
    fn format(&self, config: &FormatConfig, as_expr: bool) -> String {
        self.format_indented(config, as_expr, 0)
    }

    fn format_indented(&self, format_config: &FormatConfig, as_expr: bool, indent: usize) -> String {
        const SEPAR: &str = ", ";
        const OPEN_SIGN: &str = "[";
        const CLOSE_SIGN: &str = "]";

        macro_rules! tab_size {
            ($indent:expr) => { $indent * format_config.indent };
        }

        let values = self.0.borrow();

        let mut values_size = values
            .iter()
            .map(|v| v.to_string())
            .fold(0, |acc, v| acc + v.len());
        values_size += 4 + (values.len().max(1) - 1) * SEPAR.len() + tab_size!(indent);

        let indent_string = format_config.get_ident_string(indent);

        if !format_config.multi_line || values_size <= format_config.max_width {
            let values = values
                .iter()
                .map(|v| v.format(format_config, as_expr))
                .join(SEPAR);
            format!("{}{} {} {}", indent_string, OPEN_SIGN, values, CLOSE_SIGN)
        } else {
            let values = values
                .iter()
                .map(|v| v.format_indented(format_config, as_expr, indent + 1))
                .join(",\n");
            format!(
                "{}{}\n{}\n{}{}",
                indent_string,
                OPEN_SIGN,
                values,
                indent_string,
                CLOSE_SIGN
            )
        }
    }
}

impl IntoValue for ArrayOperand {
    fn into_value(self) -> Value {
        Value::Array(self)
    }
}

impl<V> From<V> for ArrayOperand
where
    V: Into<Vec<Value>>
{
    fn from(values: V) -> ArrayOperand {
        ArrayOperand ( Rc::new(RefCell::new(values.into())) )
    }
}

#[cfg(test)]
mod tests {
    use crate::util::test_common::*;

    #[test]
    fn array_concatenation() {
        with_test_context!(cxt, {
            let c = eval!(cxt, "[1,2] + [3,4]");
            assert_eq!(c, array![
                int!(1),
                int!(2),
                int!(3),
                int!(4),
            ]);

            let c = eval!(cxt, "[1,2] + []");
            assert_eq!(c, array![
                int!(1),
                int!(2),
            ]);

            let c = eval!(cxt, "[1,2] + true");
            assert_eq!(c, array![
                int!(1),
                int!(2),
                v_true!(),
            ]);
        })
    }

    #[test]
    fn array_indexing() {
        with_test_context!(cxt, {
            let v = eval!(cxt, "a = [333, 555, 777]; a[1]");
            assert_eq!(v, int!(555));
        })
    }

    #[test]
    fn array_equality() {
        with_test_context!(cxt, {
            let t = eval!(cxt, "[1, 2.0] == [1, 4/2]");
            assert_eq!(t, v_true!());
            let t = eval!(cxt, "[1, 2] != [1, 4*2]");
            assert_eq!(t, v_true!());
            let t = eval!(cxt, "[1, 2] != [1, 2, 3]");
            assert_eq!(t, v_true!());
            let t = eval!(cxt, "[1, 2] != 1");
            assert_eq!(t, v_true!());
        })
    }

    #[test]
    fn array_format() {
        with_test_context!(cxt, {
            eval!(cxt, r#"config("max-width", 10)"#);
            let arr = eval!(cxt, "[1, 2]");
            assert_eq!(
                arr.format(cxt.container.format_config(), true),
                "[ 1, 2 ]"
            );
            let arr = eval!(cxt, "[1, 2, 3, 4]");
            assert_eq!(
                arr.format(cxt.container.format_config(), true),
                "[\n  1,\n  2,\n  3,\n  4\n]"
            );
            let arr = eval!(cxt, "[1, 2, [6, 7, 8, 9], 4]");
            assert_eq!(
                arr.format(cxt.container.format_config(), true),
                "[\n  1,\n  2,\n  [\n    6,\n    7,\n    8,\n    9\n  ],\n  4\n]"
            );
            eval!(cxt, r#"config("indent", 1)"#);
            let arr = eval!(cxt, "[1, 2, [6, 7, 8, 9], 4]");
            assert_eq!(
                arr.format(cxt.container.format_config(), true),
                "[\n 1,\n 2,\n [\n  6,\n  7,\n  8,\n  9\n ],\n 4\n]"
            );
            eval!(cxt, r#"config("indent", 0)"#);
            let arr = eval!(cxt, "[1, 2, [6, 7, 8, 9], 4]");
            eval!(cxt, r#"config("multi-line", false)"#);
            assert_eq!(
                arr.format(cxt.container.format_config(), true),
                "[ 1, 2, [ 6, 7, 8, 9 ], 4 ]",
            );
        });
    }
}

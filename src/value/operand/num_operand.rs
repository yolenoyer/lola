//! Implements the [`NumOperand`] struct.

use std::convert::TryFrom;
use std::fmt;

use getset::Getters;
use rug::{Float, float::Round};
use rug::ops::{Pow, MulAssignRound, AddAssignRound, DivAssignRound, RemAssignRound};

use crate::value::{
    Value,
    OperationError,
    Unit,
    UnitMergingStrategy,
    OperationResult,
    IntoOperationResult,
    ValueMergeStrategy,
    Operand,
    IntoValue,
    ToStrOperand,
    IntOperand,
    Format,
    FormatConfig,
};
use crate::util::format_number;
use crate::numbers::approx_eq;

/// A quantified numeric operand, e.g. `2.5 km/s`.
#[derive(Debug, Clone, PartialEq, Getters)]
pub struct NumOperand {
    #[getset(get = "pub")]
    num: Float,

    #[getset(get = "pub")]
    unit: Unit,
}

/// The result of an operation between two [`NumOperand`] instances.
pub type NumOperationResult = Result<NumOperand, OperationError>;

/// It is nice to be able to easily convert a [`NumOperationResult`] into the more generic type
/// [`OperationResult`].
///
impl IntoOperationResult for NumOperationResult {
    fn into_operation_result(self) -> OperationResult {
        self.map(|num_operand| num_operand.into_value())
    }
}

impl NumOperand {
    /// Creates a new [`NumOperand`] instance.
    pub fn new(num: Float, unit: &Unit) -> NumOperand {
        NumOperand {
            num,
            unit: unit.clone(),
        }
    }

    /// Tries to convert the number to an usize.
    pub fn to_usize(&self) -> Result<usize, OperationError> {
        match self.num.to_i32_saturating() {
            Some(num) if num >= 0 => Ok(num as usize),
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Tries to convert the number to an i32.
    pub fn to_i32(&self) -> Result<i32, OperationError> {
        match self.num.to_i32_saturating() {
            Some(num) => Ok(num),
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Creates a new [`NumOperand`] instance by transforming the numeric value, but keeping the same
    /// unit.
    pub fn map<F>(&self, func: F) -> NumOperand
        where F: Fn(&Float) -> Float
    {
        NumOperand::new(func(&self.num), &self.unit)
    }

    /// Checks if the two numbers are equivalent, e.g., 1000m is equivalent to 1km.
    pub fn equiv(&self, rhs: &NumOperand) -> bool {
        if let Ok(rhs) = rhs.convert_num_to_unit(&self.unit) {
            approx_eq(&self.num, &rhs.num)
        } else {
            false
        }
    }

    /// Checks if the two numbers are equal (same unit, same number approximatively).
    pub fn equal(&self, rhs: &NumOperand) -> bool {
        self.unit == rhs.unit && approx_eq(&self.num, &rhs.num)
    }

    /// Checks if self is lower than the rhs.
    pub fn lt_num(&self, rhs: &NumOperand) -> bool {
        if let Ok(rhs) = rhs.convert_num_to_unit(&self.unit) {
            self.num < rhs.num
        } else {
            false
        }
    }

    /// Checks if self is greater than the rhs.
    pub fn gt_num(&self, rhs: &NumOperand) -> bool {
        if let Ok(rhs) = rhs.convert_num_to_unit(&self.unit) {
            self.num > rhs.num
        } else {
            false
        }
    }

    /// Checks if self is lower than or equal to the rhs.
    pub fn lte_num(&self, rhs: &NumOperand) -> bool {
        if let Ok(rhs) = rhs.convert_num_to_unit(&self.unit) {
            self.num <= rhs.num
        } else {
            false
        }
    }

    /// Checks if self is greater thant or equal to the rhs.
    pub fn gte_num(&self, rhs: &NumOperand) -> bool {
        if let Ok(rhs) = rhs.convert_num_to_unit(&self.unit) {
            self.num >= rhs.num
        } else {
            false
        }
    }

    /// Creates a new [`NumOperand`] instance by converting the number to new quantity-equivalent unit
    /// (e.g: `m.s-1` to `km.h-1`).
    pub fn to_converted_float(&self, unit: &Unit) -> Option<Float> {
        if self.unit.quantity_equiv(unit) {
            let mut float = self.num.clone();
            float.mul_assign_round(self.unit.weight(), Round::Nearest);
            float.div_assign_round(unit.weight(), Round::Up);
            Some(float)
        } else {
            None
        }
    }

    /// Creates a new [`NumOperand`] instance by converting the number to new quantity-equivalent unit
    /// (e.g: `m.s-1` to `km.h-1`), with rounding strategy given as an argument.
    pub fn to_converted_float_round(&self, unit: &Unit, round: Round) -> Option<Float> {
        if self.unit.quantity_equiv(unit) {
            let mut float = self.num.clone();
            float.mul_assign_round(self.unit.weight(), round);
            float.div_assign_round(unit.weight(), round);
            Some(float)
        } else {
            None
        }
    }

    /// Converts the float number of an operand (if possible), so that it is expressed with the
    /// same unit as self. Rounding strategy is available as an argument.
    fn to_compatible_float_round(&self, n: &NumOperand, round: Round) -> Option<Float> {
        n.to_converted_float_round(&self.unit, round)
    }

    /// Creates a new [`NumOperand`] instance by converting the number to new quantity-equivalent unit
    /// (e.g: `m.s-1` to `km.h-1`).
    pub fn convert_num_to_unit(&self, unit: &Unit) -> NumOperationResult {
        match self.to_converted_float(unit) {
            Some(float) => Ok(NumOperand {
                num: float,
                unit: unit.clone(),
            }),
            None => Err(err!(Operation:MismatchUnits))
        }
    }

    /// Creates a new [`NumOperand`] instance by keeping the value, but changing the unit without any
    /// conversion.
    /// (e.g: `4.5 km/s` to `4.5 h`).
    pub fn raw_convert_num_to_unit(&self, unit: &Unit) -> NumOperationResult {
        Ok(NumOperand {
            num: self.num.clone(),
            unit: unit.clone(),
        })
    }

    /// Creates a new [`NumOperand`] instance which is the result of the addition or the subtraction
    /// with another number.
    ///
    /// If `dir` is equal to 1 then it will perform an addition. If `dir` is equal to -1 then it
    /// will perform a subtraction.
    fn add_num_dir(&self, rhs: &NumOperand, dir: i32) -> NumOperationResult {
        match rhs.to_converted_float(&self.unit) {
            Some(mut float) => {
                float.mul_assign_round(dir, Round::Up);
                float.add_assign_round(&self.num, Round::Up);
                Ok(NumOperand {
                    num: float,
                    unit: self.unit.clone(),
                })
            },
            None => Err(err!(Operation:MismatchUnits))
        }
    }

    /// Creates a new [`NumOperand`] instance which is the result of the addition with another number.
    pub fn add_num(&self, rhs: &NumOperand) -> NumOperationResult {
        self.add_num_dir(rhs, 1)
    }

    /// Creates a new [`NumOperand`] instance which is the result of the subtraction with another
    /// number.
    pub fn sub_num(&self, rhs: &NumOperand) -> NumOperationResult {
        self.add_num_dir(rhs, -1)
    }

    /// Creates a new [`NumOperand`] instance which is the result of the multiplication or the division
    /// with another number.
    ///
    /// If `dir` is equal to `1` then it will perform a multiplication. If `dir` is equal to `-1`
    /// then it will perform a division.
    fn mul_num_dir(&self, rhs: &NumOperand, dir: i32, strategy: ValueMergeStrategy) -> NumOperationResult {
        let ret = |real_rhs: &NumOperand, strategy| {
            let mut num = self.num.clone();
            match dir {
                1  => num.mul_assign_round(&real_rhs.num, Round::Up),
                -1 => num.div_assign_round(&real_rhs.num, Round::Up),
                _ => unreachable!(),
            };
            Ok(NumOperand {
                num,
                unit: self.unit.mul_dir(&real_rhs.unit, dir, strategy),
            })
        };

        match strategy {
            ValueMergeStrategy::Append => {
                ret(rhs, UnitMergingStrategy::Append)
            },
            ValueMergeStrategy::MergeUnits => {
                ret(rhs, UnitMergingStrategy::Merge)
            },
            ValueMergeStrategy::MergeQuantities => {
                let new_rhs_unit = rhs.unit.adapted_quantities(&self.unit);
                let new_rhs = rhs.convert_num_to_unit(&new_rhs_unit).unwrap();
                ret(&new_rhs, UnitMergingStrategy::Merge)
            },
        }
    }

    /// Creates a new [`NumOperand`] instance which is the result of the multiplication with another
    /// number.
    ///
    /// See [`ValueMergeStrategy`] for information about the `strategy` parameter.
    pub fn mul_num(&self, rhs: &NumOperand, strategy: ValueMergeStrategy) -> NumOperationResult {
        self.mul_num_dir(rhs, 1, strategy)
    }

    /// Creates a new [`NumOperand`] instance which is the result of the division with another number.
    ///
    /// See [`ValueMergeStrategy`] for information about the `strategy` parameter.
    pub fn div_num(&self, rhs: &NumOperand, strategy: ValueMergeStrategy) -> NumOperationResult {
        if rhs.num.is_zero() {
            Err(err!(Operation:DivideByZero))
        } else {
            self.mul_num_dir(rhs, -1, strategy)
        }
    }

    /// Creates a new number which is raised to the given power. The power number must have an empty
    /// unit (trying to calculate `3 ^ 2km` is non-sense).
    pub fn pow_num(&self, rhs: &NumOperand) -> NumOperationResult {
        if !rhs.unit.is_empty() {
            return Err(err!(Operation:MismatchUnits));
        }
        Ok(NumOperand {
            num: self.num.clone().pow(&rhs.num),
            unit: self.unit.pow(rhs.num.clone()),
        })
    }

    /// Creates a new [`NumOperand`] instance which is the result of the modulo (non-negative
    /// remainder) of another number.
    fn modulo(&self, rhs: &NumOperand) -> NumOperationResult {
        let rhs_num = self.to_compatible_float_round(rhs, Round::Down)
            .ok_or(err!(Operation:MismatchUnits))?;

        let mut r = self.num.clone();
        r.rem_assign_round(rhs_num, Round::Up);

        Ok(NumOperand {
            num: r,
            unit: self.unit.clone(),
        })
    }

    /// Helps to implement the trait [`Operand`] without repeating the same code multiple times.
    fn num_oper(rhs: &Value, func: impl Fn(&NumOperand) -> NumOperationResult) -> OperationResult {
        match rhs {
            Value::Num(rhs) => func(rhs).into_operation_result(),
            Value::Int(rhs) => {
                let new_rhs = NumOperand::from(rhs).into_value();
                Self::num_oper(&new_rhs, func)
            },
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Helps to implement the trait [`Operand`] without repeating the same code multiple times.
    fn num_oper_operand(rhs: &Value, func: impl Fn(&NumOperand) -> NumOperationResult) -> NumOperationResult {
        match rhs {
            Value::Num(rhs) => func(rhs),
            Value::Int(rhs) => {
                let new_rhs = NumOperand::from(rhs).into_value();
                Self::num_oper_operand(&new_rhs, func)
            },
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }

    /// Helps to implement the trait [`Operand`] without repeating the same code multiple times.
    fn num_compare(&self, rhs: &Value, func: impl Fn(&NumOperand) -> bool) -> OperationResult {
        match rhs {
            Value::Num(rhs) => {
                if self.unit.quantity_equiv(&rhs.unit) {
                    Ok(boolean!(func(rhs)))
                } else {
                    Err(err!(Operation:MismatchUnits))
                }
            }
            Value::Int(rhs) => {
                let new_rhs = NumOperand::from(rhs).into_value();
                self.num_compare(&new_rhs, func)
            },
            _ => Err(err!(Operation:MismatchTypes)),
        }
    }
}

impl Operand for NumOperand {
    /// Performs the `+` operation.
    fn add(&self, rhs: &Value) -> OperationResult {
        if rhs.is_str() {
            // Delegate string concatenation to StrOperand:
            let lhs = self.to_str_operand().into_value();
            lhs.add(rhs)
        } else {
            Self::num_oper(rhs, |nv| self.add_num(nv))
        }
    }

    /// Performs the `-` operation.
    fn sub(&self, rhs: &Value) -> OperationResult {
        Self::num_oper(rhs, |nv| self.sub_num(nv))
    }

    /// Performs the `*` operation.
    fn mul(&self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        Self::num_oper(rhs, |nv| self.mul_num(nv, strategy))
    }

    /// Performs the `/` operation.
    fn div(&self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        Self::num_oper(rhs, |nv| self.div_num(nv, strategy))
    }

    /// Performs the `%` operation.
    fn modulo(&self, rhs: &Value) -> OperationResult {
        Self::num_oper(rhs, |nv| self.modulo(nv))
    }

    /// Performs the `^` operation.
    fn pow(&self, rhs: &Value) -> OperationResult {
        Self::num_oper(rhs, |nv| self.pow_num(nv))
    }

    /// Performs the `+=` operation.
    fn set_add(&mut self, rhs: &Value) -> OperationResult {
        let new_num = Self::num_oper_operand(rhs, |rhs| self.add_num(rhs))?;
        *self = new_num.clone();
        Ok(new_num.into_value())
    }

    /// Performs the `-=` operation.
    fn set_sub(&mut self, rhs: &Value) -> OperationResult {
        let new_num = Self::num_oper_operand(rhs, |rhs| self.sub_num(rhs))?;
        *self = new_num.clone();
        Ok(new_num.into_value())
    }

    /// Performs the `*=` operation.
    fn set_mul(&mut self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        let new_num = Self::num_oper_operand(rhs, |rhs| self.mul_num(rhs, strategy))?;
        *self = new_num.clone();
        Ok(new_num.into_value())
    }

    /// Performs the `/=` operation.
    fn set_div(&mut self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        let new_num = Self::num_oper_operand(rhs, |rhs| self.div_num(rhs, strategy))?;
        *self = new_num.clone();
        Ok(new_num.into_value())
    }

    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.equiv(nv))
            .or_else(|_| Ok(v_false!()))
    }

    /// Performs the `<` operation.
    fn cmp_lt(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.lt_num(nv))
    }

    /// Performs the `>` operation.
    fn cmp_gt(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.gt_num(nv))
    }

    /// Performs the `<=` operation.
    fn cmp_lte(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.lte_num(nv))
    }

    /// Performs the `>=` operation.
    fn cmp_gte(&self, rhs: &Value) -> OperationResult {
        self.num_compare(rhs, |nv| self.gte_num(nv))
    }

    /// Performs the `~` operation.
    fn convert_to_unit(&self, unit: &Unit) -> OperationResult {
        self.convert_num_to_unit(unit).into_operation_result()
    }

    /// Performs the `~!` operation.
    fn raw_convert_to_unit(&self, unit: &Unit) -> OperationResult {
        self.raw_convert_num_to_unit(unit).into_operation_result()
    }
}

impl Default for NumOperand {
    fn default() -> Self {
        Self {
            num: float_parsed!(0),
            unit: Unit::default(),
        }
    }
}

impl fmt::Display for NumOperand {
    /// Displays a formatted version of a number.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", format_number(&self.num, None))?;
        let unit = self.unit.to_string();
        if !unit.starts_with('°') && !self.unit.is_empty() {
            write!(f, " ")?;
        }
        write!(f, "{}", self.unit)
    }
}

impl Format for NumOperand {
    fn format(&self, config: &FormatConfig, _as_expr: bool) -> String {
        let mut output = format_number(&self.num, Some(config));
        let unit = self.unit.to_string();
        if !unit.starts_with('°') && !self.unit.is_empty() {
            output.push(' ');
        }
        output.push_str(&format!("{}", self.unit));
        output
    }
}

impl From<Float> for NumOperand {
    fn from(num: Float) -> NumOperand {
        NumOperand {
            num,
            unit: Unit::default(),
        }
    }
}

impl From<usize> for NumOperand {
    fn from(num: usize) -> NumOperand {
        NumOperand::from(float!(num))
    }
}

impl From<i64> for NumOperand {
    fn from(num: i64) -> NumOperand {
        NumOperand::from(float!(num))
    }
}

impl From<&IntOperand> for NumOperand {
    fn from(int: &IntOperand) -> NumOperand {
        NumOperand::from(float!(int.val()))
    }
}

impl TryFrom<&NumOperand> for i64 {
    type Error = ();

    fn try_from(num: &NumOperand) -> Result<Self, Self::Error> {
        if let Some(integer) = num.num.to_integer() {
            if let Some(i) = integer.to_i64() {
                return Ok(i);
            }
        }
        Err(())
    }
}

impl IntoValue for NumOperand {
    fn into_value(self) -> Value {
        Value::Num(self)
    }
}

#[cfg(test)]
mod tests {
    use crate::util::test_common::*;
    use ValueMergeStrategy::{Append, MergeUnits, MergeQuantities};

    #[test]
    fn number_mul_append() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km).mul(&val!(2 s), Append).unwrap();
            assert_eq!(v, val!(10 km.s));

            let v = val!(5 km/s).mul(&val!(2 km/s), Append).unwrap();
            assert_eq!(v, val!(10 km:1, s:-1, km:1, s:-1));
        })
    }

    #[test]
    fn number_mul_merge_units() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km).mul(&val!(2 s), MergeUnits).unwrap();
            assert_eq!(v, val!(10 km.s));

            let v = val!(5 km/s).mul(&val!(2 km/s), MergeUnits).unwrap();
            assert_eq!(v, val!(10 km:2, s:-2));

            let v = val!(5 km/s).mul(&val!(2 km/h), MergeUnits).unwrap();
            assert_eq!(v, val!(10 km:2, s:-1, h:-1));
        })
    }

    #[test]
    fn number_mul_merge_quantities() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km).mul(&val!(2 s), MergeQuantities).unwrap();
            assert_eq!(v, val!(10 km.s));

            let v = val!(5 km/s).mul(&val!(2 km/s), MergeQuantities).unwrap();
            assert_eq!(v, val!(10 km:2, s:-2));

            let v = val!(5 km/s).mul(&val!(7200 km/h), MergeQuantities).unwrap();
            assert!(v.equal(&val!(10 km:2, s:-2)));
        })
    }

    #[test]
    fn number_div_append() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km).div(&val!(2), Append).unwrap();
            assert_eq!(v, val!(2.5 km));

            let v = val!(5 km/s).div(&val!(2 km), Append).unwrap();
            assert_eq!(v, val!(2.5 km:1, s:-1, km:-1));
        })
    }

    #[test]
    fn number_div_merge_units() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km/s).div(&val!(2 km), MergeUnits).unwrap();
            assert_eq!(v, val!(2.5 s:-1));

            let v = val!(5 m/s).div(&val!(2 km), MergeUnits).unwrap();
            assert_eq!(v, val!(2.5 m:1, s:-1, km:-1));
        })
    }

    #[test]
    fn number_div_merge_quantities() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km/s).div(&val!(2 km), MergeQuantities).unwrap();
            assert_eq!(v, val!(2.5 s:-1));

            let v = val!(5000 m/s).div(&val!(2 km), MergeQuantities).unwrap();
            assert_eq!(v, val!(2.5 s:-1));
        })
    }

    #[test]
    fn number_divide_by_zero() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let r = val!(7 km).div(&val!(0 s), Append);
            assert_eq!(r, Err(err!(Operation:DivideByZero)));
        })
    }

    #[test]
    fn number_basic_add() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km).add(&val!(2 km)).unwrap();
            assert_eq!(v, val!(7 km));
        })
    }

    #[test]
    fn number_basic_sub() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(5 km).sub(&val!(7 km)).unwrap();
            assert_eq!(v, val!(-2 km));
        })
    }

    #[test]
    fn number_add_with_conversion() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(1000 m).add(&val!(1.5 km)).unwrap();
            assert_eq!(v, val!(2500 m));
        })
    }

    #[test]
    fn number_add_with_incompatible_units() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(1000 m).add(&val!(1 h));
            assert!(v == Err(err!(Operation:MismatchUnits)));
        })
    }

    #[test]
    fn number_pow() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            let v = val!(2 km).pow(&val!(2)).unwrap();
            assert_eq!(v, val!(4 km2));

            let r = val!(2 km).pow(&val!(2 km));
            assert_eq!(r, Err(err!(Operation:MismatchUnits)));

            let v = val!(4 km2).pow(&val!(0.5)).unwrap();
            assert_eq!(v, val!(2 km));

            let v = val!(16 km/h).pow(&val!(-1)).unwrap();
            assert_eq!(v, val!(0.0625 km:-1, h:1));

            let v = val!(16 km/h).pow(&val!(0)).unwrap();
            assert_eq!(v, val!(1));
        })
    }

    #[test]
    fn number_equality() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (number!(container, $($tt)*)) }

            assert!(val!(1000 km) == val!(1000 km));
            assert!(val!(1000 m) != val!(1 km));
        })
    }

    #[test]
    fn number_equiv() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (num_operand!(container, $($tt)*)) }

            assert!(val!(1000 m).equiv(&val!(1 km)));
        })
    }

    #[test]
    fn number_equiv_with_derived_units() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (num_operand!(container, $($tt)*)) }

            assert!(val!(15 N).equiv(&val!(15 kg:1, m:1, s:-2)));
            assert!(val!(1 N).equiv(&val!(3600000 g:1, m:1, min:-2)));
        })
    }

    #[test]
    fn number_lt() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (num_operand!(container, $($tt)*)) }

            let lhs = val!(2000 m);
            let rhs = val!(1 km);
            assert!(!lhs.lt_num(&rhs));
            assert!(rhs.lt_num(&lhs));
        })
    }

    #[test]
    fn number_gt() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (num_operand!(container, $($tt)*)) }

            let lhs = val!(2000 m);
            let rhs = val!(1 km);
            assert!(lhs.gt_num(&rhs));
            assert!(!rhs.gt_num(&lhs));
        })
    }

    #[test]
    fn number_lte() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (num_operand!(container, $($tt)*)) }

            let lhs = val!(2000 m);
            let rhs = val!(1 km);
            assert!(!lhs.lte_num(&rhs));
            assert!(rhs.lte_num(&lhs));

            let lhs = val!(1000 m);
            let rhs = val!(1 km);
            assert!(lhs.lte_num(&rhs));
            assert!(rhs.lte_num(&lhs));
        })
    }

    #[test]
    fn number_gte() {
        with_test_container!(container, {
            macro_rules! val { ($($tt:tt)*) => (num_operand!(container, $($tt)*)) }

            let lhs = val!(2000 m);
            let rhs = val!(1 km);
            assert!(lhs.gte_num(&rhs));
            assert!(!rhs.gte_num(&lhs));

            let lhs = val!(1000 m);
            let rhs = val!(1 km);
            assert!(lhs.gte_num(&rhs));
            assert!(rhs.gte_num(&lhs));
        })
    }

    // #[test]
    // fn number_display() {
    //     with_test_container!(container, {
    //         let n = float_parsed!(30856775810000000);
    //         let n = float!(&n * &float!(&n * &n));
    //         let v = NumOperand::new(n, &unit!(container, km)).into_value();
    //         assert_eq!(format!("{}", v), "2.9379989446927950283284513941E+49 km");

    //         let n = float_parsed!(1000000000000000000);
    //         let n = float!(&n * &float!(&n * &n));
    //         let v = NumOperand::new(n, &unit!(container, km)).into_value();
    //         assert_eq!(format!("{}", v), "1E+54 km");
    //     })
    // }

    #[test]
    fn add_string() {
        with_test_container!(container, {
            let n = num_operand!(container, 4 km);
            let s = string!("...");
            assert_eq!(n.add(&s).unwrap(), string!("4 km..."));
        })
    }
}

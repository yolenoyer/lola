//! Implements the [`StrOperand`] struct.

use std::fmt;

use crate::value::{
    IntoValue,
    Operand,
    OperationError,
    OperationResult,
    Value,
    ValueIterator,
    Format,
    FormatConfig,
};

/// A string operand.
#[derive(Debug, Clone, PartialEq, Eq, Default, Hash)]
pub struct StrOperand ( String );

impl StrOperand {
    /// Returns a reference to the underlying string.
    pub fn val_ref(&self) -> &String {
        &self.0
    }

    /// Retuns the length of the underlying string.
    #[inline]
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Checks if the string is empty.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Return the UTF-8 length of the underlying string.
    pub fn utf8_len(&self) -> usize {
        self.0.chars().count()
    }

    /// Concatenate this string value with another value.
    pub fn concat(&self, rhs: &Value) -> StrOperand {
        let rhs = rhs.as_operand_value().to_string();
        StrOperand::from(self.0.clone() + &rhs)
    }
}

impl Operand for StrOperand {
    /// Performs the `+` operation (string concatenation).
    fn add(&self, rhs: &Value) -> OperationResult {
        Ok(self.concat(rhs).into_value())
    }

    /// Performs the `==` operation.
    fn equ(&self, rhs: &Value) -> OperationResult {
        match rhs {
            Value::Str(rhs) => Ok(boolean!(self.0 == rhs.0)),
            _ => Ok(v_false!())
        }
    }

    /// Performs the `+=` operation.
    fn set_add(&mut self, rhs: &Value) -> OperationResult {
        let rhs = rhs.as_operand_value().to_string();
        self.0.push_str(&rhs);
        Ok(self.clone().into_value())
    }

    /// Performs the `[]` operation.
    fn index(&self, index: &Value) -> OperationResult {
        let num = index.get_num().ok_or(err!(Operation:MismatchTypes))?;
        if !num.unit().is_empty() {
            return Err(err!(Operation:MismatchUnits));
        }
        let num = num.to_i32()?;
        if num < 0 {
            return Err(err!(Operation:IndexOutOfRange));
        }
        match self.0.chars().nth(num as usize) {
            Some(c) => Ok(string!(format!("{}", c))),
            None => Err(err!(Operation:IndexOutOfRange)),
        }
    }

    /// Returns an iterator over the value.
    #[allow(clippy::needless_collect)]
    fn iterate(&self) -> Result<ValueIterator, OperationError> {
        let values = self.0
            .chars()
            .map(|c| Value::from(StrOperand::from(c)))
            .collect::<Vec<_>>()
        ;
        Ok(ValueIterator::new(Box::new(values.into_iter())))
    }
}

impl fmt::Display for StrOperand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Format for StrOperand {
    fn format(&self, _config: &FormatConfig, as_expr: bool) -> String {
        if as_expr {
            let mut output = String::with_capacity(self.0.len() + 2);
            output.push('"');
            for c in self.0.chars() {
                if c == '"' {
                    output.push('\\');
                }
                output.push(c);
            }
            output.push('"');
            output
        } else {
            self.0.to_string()
        }
    }
}

impl From<&str> for StrOperand {
    fn from(s: &str) -> StrOperand {
        StrOperand ( s.to_string() )
    }
}

impl From<String> for StrOperand {
    fn from(s: String) -> StrOperand {
        StrOperand ( s )
    }
}

impl From<char> for StrOperand {
    fn from(c: char) -> StrOperand {
        StrOperand ( c.to_string() )
    }
}

impl IntoValue for StrOperand {
    fn into_value(self) -> Value {
        Value::Str(self)
    }
}

/// Convert something into an [`StrOperand`].
///
/// [`StrOperand`]: crate::value::StrOperand
pub trait ToStrOperand {
    fn to_str_operand(&self) -> StrOperand;
}

impl<V: Operand> ToStrOperand for V {
    fn to_str_operand(&self) -> StrOperand {
        StrOperand::from(self.to_string())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn concat_strings() {
        with_test_container!(container, {
            let s1 = str_operand!("hello ");
            let s2 = string!("world");
            assert_eq!(s1.concat(&s2), str_operand!("hello world"));

            let s = str_operand!("Value is ");
            let v = number!(container, 2 km);
            assert_eq!(s.concat(&v), str_operand!("Value is 2 km"));
        })
    }

    #[test]
    fn index_strings() {
        with_test_container!(container, {
            let s = string!("hello");
            let n = number!(container, 4);
            assert_eq!(s.index(&n), Ok(string!("o")));

            let s = string!("hello");
            let n = number!(container, 5);
            assert_eq!(s.index(&n), Err(err!(Operation:IndexOutOfRange)));
        })
    }

    #[test]
    fn iterate_strings() {
        with_test_container!(container, {
            let s = string!("AB");
            assert_eq!(
                s.iterate().unwrap().0.collect::<Vec<_>>(),
                [ string!("A"), string!("B") ]
            );

            let s = string!("");
            assert_eq!(
                s.iterate().unwrap().0.collect::<Vec<_>>(),
                []
            );

            let s = string!("éà");
            assert_eq!(
                s.iterate().unwrap().0.collect::<Vec<_>>(),
                [ string!("é"), string!("à") ]
            );
        })
    }
}

//! Implements the [`CompoundQuantity`] struct.

use std::collections::HashMap;
use std::fmt;

use rug::Float;

use crate::numbers::format_number;
use crate::util::numbers::approx_eq;
use crate::value::{BaseUnit, Unit};

/// Represents a compound quantity, with an exponent for each base quantity.
/// This is useful to compare units together, to check if they represent the same "concept".
///
/// E.g, `km·h-1` and `m·s-1` will have the same [`CompoundQuantity`]: `{ "length": 1, "time": -1 }`
#[derive(Debug, PartialEq, Clone, Default)]
pub struct CompoundQuantity ( HashMap<String, Float> );

/// Creates the [`CompoundQuantity`] instance corresponding to the given base unit.
impl From<&BaseUnit> for CompoundQuantity {
    fn from(bu: &BaseUnit) -> CompoundQuantity {
        match bu.equivalence() {
            Some(unit) => CompoundQuantity::from(unit),
            None => {
                let mut m = HashMap::new();
                let quantity_name = bu.strong_quantity().borrow().name().to_string();
                m.insert(quantity_name, float!(1.0));
                CompoundQuantity (m)
            }
        }
    }
}

/// Creates the [`CompoundQuantity`] instance corresponding to the given unit.
impl From<&Unit> for CompoundQuantity {
    fn from(u: &Unit) -> CompoundQuantity {
        let mut m = HashMap::new();
        for ebu in u.exp_base_units() {
            let bu_compound_quantity = CompoundQuantity::from(ebu.strong_base_unit().as_ref());
            for (q_name, exp) in bu_compound_quantity.0.into_iter() {
                *m.entry(q_name).or_insert_with(|| float_parsed!(0)) += ebu.exp() * exp;
            }
        }
        m.retain(|_, v: &mut Float| !v.is_zero());
        CompoundQuantity (m)
    }
}

/// Creates a [`CompoundQuantity`] instance from a `HashMap`.
impl From<HashMap<String, Float>> for CompoundQuantity {
    fn from(m: HashMap<String, Float>) -> CompoundQuantity {
        CompoundQuantity (m)
    }
}

impl fmt::Display for CompoundQuantity {
    /// Displays a formatted version of a compound quantity.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = self.0.iter()
            .map(|(name, exp)| {
                let mut s = name.to_string();
                if !approx_eq(exp, &float!(1.0)) {
                    s.push_str(&format_number(exp, None));
                }
                s
            })
            .collect::<Vec<_>>()
            .join(".");
        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use maplit::hashmap;

    #[test]
    fn unit_to_compound_quantity() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u = u!(km:1, s:-1, m:1);
            assert_eq!(
                CompoundQuantity::from(&u),
                CompoundQuantity::from(hashmap!{
                    s!("length") => float_parsed!(2),
                    s!("time") => float_parsed!(-1),
                })
            );
        })
    }

    #[test]
    fn derived_unit_to_compound_quantity() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u = u!(N:1, s:-1, m:1);
            assert_eq!(
                CompoundQuantity::from(&u),
                CompoundQuantity::from(hashmap!{
                    s!("mass") => float_parsed!(1),
                    s!("length") => float_parsed!(2),
                    s!("time") => float_parsed!(-3),
                })
            );
        })
    }

    #[test]
    fn display_compound_unit() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let cq = CompoundQuantity::from(&u!(km:1, s:-1, m:1));
            assert!(
                &cq.to_string() == "length2.time-1" ||
                &cq.to_string() == "time-1.length2"
            );
        })
    }
}

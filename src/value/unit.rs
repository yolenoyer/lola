//! Implements the [`Unit`] struct.

use std::cell::RefCell;
use std::rc::Weak;
use std::fmt;

use getset::Getters;
use rug::{Float, float::Round};
use rug::ops::{Pow, AddAssignRound, SubAssignRound};

use crate::util::numbers::approx_eq;
use crate::value::{ExpBaseUnit, Quantity, CompoundQuantity};
use crate::numbers::format_number;

/// Describes the strategy to use when two units are multiplied or divided.
#[derive(Debug)]
pub enum UnitMergingStrategy {
    /// Append the new base units at the end, even if they already exist in the first unit.
    ///
    /// E.g: `m.s * s` will become `m.s.s`.
    Append,

    /// Merge common base units together.
    ///
    /// E.g: `m.s * s` will become `m.s2`.
    Merge,
}



/// A compound unit, e.g. `km.s-1` (aka `km/s`).
#[derive(Debug, Clone, PartialEq, Getters, Default)]
pub struct Unit {
    #[getset(get = "pub")]
    exp_base_units: Vec<ExpBaseUnit>,
}

impl Unit {
    /// Checks if the unit is empty.
    pub fn is_empty(&self) -> bool {
        self.exp_base_units.is_empty()
    }

    // Warning: this code is working but is not used anymore (should be removed one day). See ##1##
    // references in the code for other parts of the code which is related.
    //
    // /// Check if two units are equivalent.
    // ///
    // /// E.g:
    // ///   - `m.s`   is equivalent to `s.m`
    // ///   - `m.m.s` is equivalent to `s.m2`
    // ///   - `m.s-1` is equivalent to `m.s2.s-3`
    // ///   - `m.s-1` is *not* equivalent to `km.h-1` (see [quantity_equiv()] for such equivalences)
    // ///
    // /// [quantity_equiv()]: struct.Unit.html#method.quantity_equiv
    // pub fn equiv(&self, other: &Unit) -> bool {
    //     let self_hash_map = self.to_hash_map();
    //     let other_hash_map = other.to_hash_map();
    //     self_hash_map == other_hash_map
    // }

    /// Checks if two units are equivalent, considering the quantities, not the base units.
    ///
    /// E.g:
    ///   - `m.s-1` is quantity-equivalent to `km.h-1` because both of them are `length.time-1`
    pub fn quantity_equiv(&self, other: &Unit) -> bool {
        self.is_compound_quantity(&CompoundQuantity::from(other))
    }

    /// Checks if the unit has the given compound quantity.
    ///
    /// E.g, it will return `true` if the unit is `km/s` and the given compound quantity is
    /// `length/time`.
    pub fn is_compound_quantity(&self, compound_quantity: &CompoundQuantity) -> bool {
        &CompoundQuantity::from(self) == compound_quantity
    }

    /// Finds the first [`ExpBaseUnit`] with the given "base unit" name in this [`Unit`].
    ///
    /// E.g: with a unit `m.s.g.s-2`, searching for `s` will return the exp base unit corresponding
    /// to seconds (the 2nd base unit), with exponent `1`.
    ///
    /// [`Unit`]: crate::value::Unit
    fn find_ebu_mut(&mut self, name: &str) -> Option<&mut ExpBaseUnit> {
        self.exp_base_units.iter_mut()
            .find(|ebu| ebu.strong_base_unit().name() == name)
    }

    /// Finds the first [`ExpBaseUnit`] representing the given quantity in this [`Unit`].
    ///
    /// E.g: with a unit `m/s2`, searching for the quantity `time` will return the exp base unit
    /// corresponding to seconds (the 2nd base unit), with exponent `-2`.
    ///
    /// [`Unit`]: crate::value::Unit
    fn find_ebu_by_quantity(&self, quantity: &Weak<RefCell<Quantity>>) -> Option<&ExpBaseUnit> {
        self.exp_base_units.iter()
            .find(|ebu| quantity.ptr_eq(ebu.strong_base_unit().weak_quantity()))
    }

    /// Creates a new [`Unit`] instance which is the result of the multiplication or the division of
    /// the current unit by the unit given as an argument.
    ///
    /// If `dir` is equal to `1` then it will perform a multiplication. If `dir` is equal to `-1`
    /// then it will perform a division.
    ///
    /// See enum [`UnitMergingStrategy`] (`Append` or `Merge`) for details about how the operation
    /// is performed.
    ///
    /// [`Unit`]: crate::value::Unit
    pub fn mul_dir(&self, other: &Unit, dir: i32, strategy: UnitMergingStrategy) -> Unit {
        match strategy {
            UnitMergingStrategy::Append => {
                Unit::from(
                    self.exp_base_units.iter()
                    .cloned()
                    .chain(
                        other.exp_base_units.iter()
                        .map(|ebu| ebu.neg_dir(dir))
                    )
                    .collect::<Vec<_>>()
                )
            },
            UnitMergingStrategy::Merge => {
                let mut new_unit = self.clone();
                for other_ebu in &other.exp_base_units {
                    let other_unit = other_ebu.strong_base_unit();
                    let other_name = &other_unit.name();
                    if let Some(ebu) = new_unit.find_ebu_mut(other_name) {
                        match dir {
                            -1 => { ebu.exp_mut().sub_assign_round(other_ebu.exp(), Round::Up); },
                            1  => { ebu.exp_mut().add_assign_round(other_ebu.exp(), Round::Up); },
                            _  => unreachable!(),
                        }
                    } else {
                        new_unit.exp_base_units.push(other_ebu.neg_dir(dir));
                    }
                }
                new_unit.exp_base_units.retain(|ebu| !ebu.exp().is_zero());
                new_unit
            },
        }
    }

    /// Creates a new [`Unit`] instance which is the result of the multiplication of the current unit
    /// by the unit given as an argument.
    ///
    /// See enum [`UnitMergingStrategy`] (`Append` or `Merge`) for details about how the operation
    /// is performed.
    ///
    /// [`Unit`]: crate::value::Unit
    pub fn mul(&self, other: &Unit, strategy: UnitMergingStrategy) -> Unit {
        self.mul_dir(other, 1, strategy)
    }

    /// Creates a new [`Unit`] instance which is the result of the division of the current unit by
    /// the unit given as an argument.
    ///
    /// See enum [`UnitMergingStrategy`] (`Append` or `Merge`) for details about how the operation
    /// is performed.
    ///
    /// [`Unit`]: crate::value::Unit
    pub fn div(&self, other: &Unit, strategy: UnitMergingStrategy) -> Unit {
        self.mul_dir(other, -1, strategy)
    }

    /// Creates a new [`Unit`] instance which is the current unit raised to the power of `n`.
    ///
    /// [`Unit`]: crate::value::Unit
    pub fn pow(&self, n: Float) -> Unit {
        if n == 0.0 {
            Unit { exp_base_units: vec![] }
        } else {
            Unit::from(
                self.exp_base_units.iter()
                .map(|ebu| ebu.mul_exp(n.clone()))
                .collect::<Vec<_>>()
            )
        }
    }

    // Warning: this code is working but is not used anymore (should be removed one day). See ##1##
    // references in the code for other parts of the code which is related.
    //
    // /// Create a hash map with base units as keys and exponents as value. If some units used are
    // /// derived units, this can be a recursive process.
    // ///
    // /// E.g:
    // ///   - `km.h-1` will return the following hash map: `{ "km": 1, "h": -1 }`
    // ///   - `N.m` will return the following hash map: `{ "kg": 1, "m": 2, "s": -2 }`
    // pub fn to_hash_map(&self) -> HashMap<String, Float> {
    //     println!("to_hash_map");
    //     let mut m = HashMap::new();
    //     for ebu in &self.exp_base_units {
    //         let bu_map = ebu.strong_base_unit().to_hash_map();
    //         for (name, exp) in bu_map.into_iter() {
    //             *m.entry(name).or_insert_with(|| float_parsed!(0)) += ebu.exp() * exp;
    //         }
    //     }
    //     m.retain(|_, v: &mut Float| !v.is_zero());
    //     m
    // }

    /// Creates a new [`Unit`] instance, which will have its base units modified if possible, by
    /// adapting them to the ones found in the `from` unit.
    ///
    /// E.g, if `self` is `g/m2` and the `from` unit is `km/h`, the function will detect that the
    /// two units have a common quantity (`length`), and will adapt the unit of `self` to match
    /// the `from` unit's signature; then it will replace `m2` by `km2` to finally obtain the new
    /// unit `g/km2`.
    ///
    /// This function is used internally to multiply or divide values together, by using the
    /// `ValueMergeStrategy::MergeQuantities` strategy.
    ///
    /// [`Unit`]: crate::value::Unit
    pub fn adapted_quantities(&self, from: &Unit) -> Unit {
        let new_ebus = self.exp_base_units.iter()
            .map(|ebu| {
                let q = ebu.weak_quantity();
                if let Some(from_ebu) = from.find_ebu_by_quantity(&q) {
                    from_ebu.with_exp(ebu.exp().clone())
                } else {
                    ebu.clone()
                }
            })
            .collect::<Vec<_>>();
        Unit::from(new_ebus)
    }

    /// Calculates the whole weight of the unit, which can be compared to other
    /// quantity-equivalent units.
    ///
    /// E.g, because time base unit is seconds and length base unit is meters, if the unit is
    /// `km/h` then the weight will be `1000 / 3600` = `2.7777...`.
    pub fn weight(&self) -> Float {
        self.exp_base_units.iter()
            .map(|ebu| float!(ebu.strong_base_unit().weight().pow(ebu.exp())))
            .fold(float!(1), |acc, n| acc * n)
    }
}

impl From<Vec<ExpBaseUnit>> for Unit {
    /// Creates a new unit from a vector a exponentiated base units.
    fn from(exp_base_units: Vec<ExpBaseUnit>) -> Self {
        Self { exp_base_units }
    }
}

impl fmt::Display for Unit {
    /// Displays a formatted version of a unit.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = self.exp_base_units.iter()
            .map(|ebu| {
                let mut s = ebu.strong_base_unit().name().to_string();
                if !approx_eq(ebu.exp(), &float!(1.0)) {
                    s.push_str(&format_number(ebu.exp(), None));
                }
                s
            })
            .collect::<Vec<_>>()
            .join("·");
        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::rc::Rc;

    #[test]
    fn unit_equality() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u1 = u!(km:1, s:-1);
            let u2 = u!(s:-1, km:1);
            assert!(u1 != u2);

            let u1 = u!(km:1, s:-1);
            let u2 = u!(km:1, s:-1);
            assert!(u1 == u2);
        })
    }

    #[test]
    fn unit_weight() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u = u!(km:1, s:-1);
            assert_eq!(u.weight(), float_parsed!(1000));

            let u = u!(km:1, h:1);
            assert_eq!(u.weight(), float_parsed!(3600000));
        })
    }

    // Warning: these tests test code which is not used anymore (should be removed one day). See ##1##
    // references in the code for other parts of the code which is related.
    //
    // #[test]
    // fn unit_equiv() {
    //     with_test_container!(container, {
    //         macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

    //         let u1 = u!(km:1, s:-1);
    //         let u2 = u!(s:-1, km:1);
    //         assert!(u1.equiv(&u2));
    //     })
    // }

    // #[test]
    // fn unit_not_equiv() {
    //     with_test_container!(container, {
    //         macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

    //         let u1 = u!(km:1, s:-1);
    //         let u2 = u!(s:-1, km:2);
    //         assert!(!u1.equiv(&u2));

    //         let u1 = u!(km:1, s:-1);
    //         let u2 = u!(km:1, s:-1, m:1);
    //         assert!(!u1.equiv(&u2));
    //     })
    // }

    // #[test]
    // fn unit_self_equiv() {
    //     with_test_container!(container, {
    //         macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

    //         let u1 = u!(km:1, s:-1);
    //         assert!(u1.equiv(&u1));
    //     })
    // }

    // #[test]
    // fn unit_to_hashmap() {
    //     with_test_container!(container, {
    //         macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

    //         let u = u!(km:1, s:-1);
    //         assert_eq!(
    //             u.to_hash_map(),
    //             hashmap!{
    //                 s!("km") => float_parsed!(1),
    //                 s!("s") => float_parsed!(-1),
    //             }
    //         );
    //     })
    // }

    #[test]
    fn unit_quantity_equiv() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u1 = u!(m:1, h:-1);
            let u2 = u!(s:-1, km:1);
            assert!(u1.quantity_equiv(&u2));
        })
    }

    #[test]
    fn unit_quantity_not_equiv() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u1 = u!(km:1, s:-1);
            let u2 = u!(s:-1, km:2);
            assert!(!u1.quantity_equiv(&u2));

            let u1 = u!(km:1, s:-1);
            let u2 = u!(km:1, s:-1, m:1);
            assert!(!u1.quantity_equiv(&u2));
        })
    }

    #[test]
    fn unit_quantity_self_equiv() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u1 = u!(km:1, s:-1);
            assert!(u1.quantity_equiv(&u1));
        })
    }

    #[test]
    fn unit_equiv_with_derived_units() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u1 = u!(N);
            let u2 = u!(kg:1, m:1, s:-2);
            // assert!(u1.equiv(&u2)); // see ##1## to know why it is commented
            assert!(u1.quantity_equiv(&u2));

            let u2 = u!(kg:1, m:1);
            // assert!(!u1.equiv(&u2)); // see ##1## to know why it is commented
            assert!(!u1.quantity_equiv(&u2));

            let u2 = u!(g:1, mi:1, s:-2);
            // assert!(!u1.equiv(&u2)); // see ##1## to know why it is commented
            assert!(u1.quantity_equiv(&u2));

            let u1 = u!(J:1, N:-1);
            let u2 = u!(m:1);
            // assert!(u1.equiv(&u2)); // see ##1## to know why it is commented
            assert!(u1.quantity_equiv(&u2));
        })
    }

    #[test]
    fn unit_string_format() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u = u!(km:1, s:-1);
            assert_eq!(u.to_string(), "km·s-1");
            let u = u!();
            assert_eq!(u.to_string(), "");
        })
    }

    #[test]
    fn unit_mul() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u1 = u!(km:1);
            let u2 = u!(s:1);
            let u = u1.mul(&u2, UnitMergingStrategy::Merge);
            assert_eq!(u, u!(km:1, s:1));

            let u1 = u!(km:1, s:-1);
            let u2 = u!(s:1);
            let u = u1.mul(&u2, UnitMergingStrategy::Merge);
            assert_eq!(u, u!(km:1));
        })
    }

    #[test]
    fn unit_pow() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u = u!(km).pow(float_parsed!(2));
            assert_eq!(u, u!(km2));

            let u = u!(km:1, s:-2).pow(float_parsed!(2));
            assert_eq!(u, u!(km:2, s:-4));

            let u = u!(km:1, s:-2).pow(float_parsed!(0));
            assert_eq!(u, u!());
        })
    }

    #[test]
    fn unit_find_ebu_mut() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let mut u = u!(km/s);
            let ebu = u.find_ebu_mut("s").unwrap();
            assert_eq!(ebu.exp(), &float_parsed!(-1));
            assert_eq!(ebu.base_unit().upgrade().unwrap().name(), "s");
        })
    }

    #[test]
    fn unit_find_ebu_by_quantity() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            let u = u!(km/s);
            let q = Rc::downgrade(&quantity!(container, "time"));
            let ebu = u.find_ebu_by_quantity(&q).unwrap();
            assert_eq!(ebu.exp(), &float_parsed!(-1));
            assert_eq!(ebu.base_unit().upgrade().unwrap().name(), "s");
        })
    }

    #[test]
    fn unit_adapted() {
        with_test_container!(container, {
            macro_rules! u { ($($tt:tt)*) => (unit!(container, $($tt)*)) }

            macro_rules! test_adapted {
                (
                    ( $($to_adapt:tt)* ),
                    ( $($from:tt)* ),
                    ( $($res:tt)* )
                ) => {
                    let u_to_adapt = u!($($to_adapt)*);
                    let u_adapt_from = u!($($from)*);
                    let adapted = u_to_adapt.adapted_quantities(&u_adapt_from);
                    assert_eq!(adapted, u!($($res)*));
                };
            }

            test_adapted!(
                (m:2, s:-3),
                (m:1, h:-1),
                (m:2, h:-3)
            );

            test_adapted!(
                (m:3, s:-1, kg:2),
                (km:1, h:-1),
                (km:3, h:-1, kg:2)
            );
        })
    }
}

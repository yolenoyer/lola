//! Implements the [`ValueIterator`] struct.

use crate::value::Value;

/// This struct can be returned by some operands via the method [`Operand::iterate()`]. If an
/// operand implements the `iterate()` method, then it will be automatically available for use as a
/// `for` loop iterator.
///
/// E.g., as [`IntOperand`] implements the `iterate()` method, it can for used in a `for` loop like
/// this:
///
/// ```none
/// for n in 12 {
///     print("loop", n);
/// }
/// ```
///
/// Here are the kinds of operand which implement `iterate()`:
///
///  * [`IntOperand`] (iterate over the numbers from 0 to n-1)
///  * [`ArrayOperand`] (iterate over the elements of the array)
///
/// [`Operand::iterate()`]: crate::value::Operand::iterate()
/// [`IntOperand`]: crate::value::IntOperand
/// [`ArrayOperand`]: crate::value::ArrayOperand
pub struct ValueIterator(
    pub Box<dyn Iterator<Item = Value>>
);

impl ValueIterator {
    pub fn new(iter: Box<dyn Iterator<Item = Value>>) -> Self {
        Self(iter)
    }
}

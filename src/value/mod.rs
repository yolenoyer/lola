//! Contains all the code which handles values: units, quantities, operations on values...

pub mod base_unit;
pub mod exp_base_unit;
pub mod quantity;
pub mod compound_quantity;
pub mod unit;
pub mod operand;
pub mod value_iterator;
pub mod format;

pub use base_unit::*;
pub use exp_base_unit::*;
pub use quantity::*;
pub use compound_quantity::*;
pub use unit::*;
pub use operand::*;
pub use value_iterator::*;
pub use format::*;

use std::fmt;
use std::convert::TryFrom;

use crate::eval::VarType;

/// Represents a value, which can be a [`NumOperand`] (number), a [`IntOperand`] (integer),
/// a [`StrOperand`] (string), a [`BooleanOperand`] (boolean), a [`FuncOperand`] (function), an
/// [`ArrayOperand`] (array)...
///
/// An important trait is [`Operand`], which makes the the bridge between the generic enum
/// [`Value`] and concrete types like [`NumOperand`] and [`StrOperand`].
#[derive(Clone, PartialEq)]
pub enum Value {
    Void(VoidOperand),
    Num(NumOperand),
    Int(IntOperand),
    Boolean(BooleanOperand),
    Str(StrOperand),
    Func(FuncOperand),
    Array(ArrayOperand),
    Dict(DictOperand),
}

/// Defines errors which can occur when performing an operation concerning one or multiple value(s).
#[derive(Debug, PartialEq)]
pub enum OperationError {
    /// Used when an operation is not available for the used value type.
    Unavailable,
    /// Mismatch types.
    MismatchTypes,
    /// Mismatch units.
    MismatchUnits,
    /// Mismatch value (like division by zero, but more generic):
    MismatchValue,
    /// Division by zero.
    DivideByZero,
    /// Index out of range.
    IndexOutOfRange,
    /// Value out of bounds.
    ValueOutOfBounds,
    /// Not iterable.
    NotIterable,
    /// The given value cannot be used as a dict index.
    Dict(DictError),
}

impl fmt::Display for OperationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Unavailable       => write!(f, "Operation: Unavailable operation for the given value types"),
            Self::MismatchTypes     => write!(f, "Operation: Mismatch types"),
            Self::MismatchUnits     => write!(f, "Operation: Mismatch units"),
            Self::MismatchValue     => write!(f, "Operation: Mismatch value"),
            Self::DivideByZero      => write!(f, "Operation: Division by zero"),
            Self::IndexOutOfRange   => write!(f, "Operation: Index out of range"),
            Self::ValueOutOfBounds  => write!(f, "Operation: Value out of bounds"),
            Self::NotIterable       => write!(f, "Operation: Value cannot be iterated"),
            Self::Dict(err)         => write!(f, "Operation: {}", err),
        }
    }
}

/// The result of an operation between values.
pub type OperationResult = Result<Value, OperationError>;

/// Trait which can convert a typed operation result to a generic operation result.
pub trait IntoOperationResult {
    fn into_operation_result(self) -> OperationResult;
}

impl Value {
    /// Return `true` if the value is void.
    pub fn is_void(&self) -> bool {
        matches!(&self, Self::Void(_))
    }

    /// Return `true` if the value is a number.
    pub fn is_num(&self) -> bool {
        matches!(&self, Self::Num(_))
    }

    /// Return `true` if the value is an integer.
    pub fn is_int(&self) -> bool {
        matches!(&self, Self::Int(_))
    }

    /// Return `true` if the value is a bool.
    pub fn is_boolean(&self) -> bool {
        matches!(&self, Self::Boolean(_))
    }

    /// Return `true` if the value is a string.
    pub fn is_str(&self) -> bool {
        matches!(&self, Self::Str(_))
    }

    /// Return `true` if the value is a function.
    pub fn is_func(&self) -> bool {
        matches!(&self, Self::Func(_))
    }

    /// Return `true` if the value is an array.
    pub fn is_array(&self) -> bool {
        matches!(&self, Self::Array(_))
    }

    /// Return `true` if the value is a dict.
    pub fn is_dict(&self) -> bool {
        matches!(&self, Self::Dict(_))
    }

    /// Retrieves the underlying [`NumOperand`] instance, if the value is a number.
    pub fn get_num(&self) -> Option<&NumOperand> {
        if let Value::Num(n) = self {
            Some(n)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`NumOperand`] instance. This will panic if the value is
    /// not a number.
    pub fn unwrap_num(&self) -> &NumOperand {
        self.get_num().unwrap()
    }

    /// Retrieves the underlying [`IntOperand`] instance, if the value is an integer.
    pub fn get_int(&self) -> Option<&IntOperand> {
        if let Value::Int(n) = self {
            Some(n)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`IntOperand`] instance. This will panic if the value is
    /// not an integer.
    pub fn unwrap_int(&self) -> &IntOperand {
        self.get_int().unwrap()
    }

    /// Tries to get an integer value, then to convert it to the given template type.
    pub fn extract_int<T>(&self) -> Result<T, OperationError>
    where
        T: TryFrom<i64>
    {
        let int_operand = self.get_int().ok_or(err!(Operation:MismatchTypes))?;
        T::try_from(int_operand.val()).map_err(|_| err!(Operation:ValueOutOfBounds))
    }

    /// Retrieves the underlying [`BooleanOperand`] instance, if the value is a boolean.
    pub fn get_boolean(&self) -> Option<&BooleanOperand> {
        if let Value::Boolean(b) = self {
            Some(b)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`BooleanOperand`] instance. This will panic if the value is
    /// not a boolean.
    pub fn unwrap_boolean(&self) -> &BooleanOperand {
        self.get_boolean().unwrap()
    }

    /// Retrieves the underlying [`StrOperand`] instance, if the value is a string.
    pub fn get_str(&self) -> Option<&StrOperand> {
        if let Value::Str(s) = self {
            Some(s)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`StrOperand`] instance. This will panic if the value is
    /// not a string.
    pub fn unwrap_str(&self) -> &StrOperand {
        self.get_str().unwrap()
    }

    /// Retrieves the underlying [`FuncOperand`] instance, if the value is a function.
    pub fn get_func(&self) -> Option<&FuncOperand> {
        if let Value::Func(s) = self {
            Some(s)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`FuncOperand`] instance. This will panic if the value is
    /// not a function.
    pub fn unwrap_func(&self) -> &FuncOperand {
        self.get_func().unwrap()
    }

    /// Retrieves the underlying [`ArrayOperand`] instance, if the value is an array.
    pub fn get_array(&self) -> Option<&ArrayOperand> {
        if let Value::Array(a) = self {
            Some(a)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`ArrayOperand`] instance. This will panic if the value is
    /// not an array.
    pub fn unwrap_array(&self) -> &ArrayOperand {
        self.get_array().unwrap()
    }

    /// Retrieves the underlying [`DictOperand`] instance, if the value is a dict.
    pub fn get_dict(&self) -> Option<&DictOperand> {
        if let Value::Dict(a) = self {
            Some(a)
        } else {
            None
        }
    }

    /// Retrieves and unwraps the underlying [`DictOperand`] instance. This will panic if the value is
    /// not a dict.
    pub fn unwrap_dict(&self) -> &DictOperand {
        self.get_dict().unwrap()
    }

    /// Checks if the value has the given type.
    pub fn has_type(&self, var_type: &VarType) -> bool {
        match &var_type {
            VarType::Void    => self.is_void(),
            VarType::Num(cq) => match self.get_num() {
                Some(n) => match cq {
                    Some(cq) => n.unit().is_compound_quantity(cq),
                    None => true,
                },
                None => false,
            },
            VarType::Int     => self.is_int(),
            VarType::Boolean => self.is_boolean(),
            VarType::Str     => self.is_str(),
            VarType::Func    => self.is_func(),
            VarType::Array   => self.is_array(),
            VarType::Dict    => self.is_dict(),
        }
    }

    /// Returns the type of the value.
    pub fn get_type(&self) -> VarType {
        match self {
            Self::Void(_)          => VarType::Void,
            Self::Num(num_operand) => {
                let cq = CompoundQuantity::from(num_operand.unit());
                VarType::Num(Some(cq))
            },
            Self::Int(_)           => VarType::Int,
            Self::Boolean(_)       => VarType::Boolean,
            Self::Str(_)           => VarType::Str,
            Self::Func(_)          => VarType::Func,
            Self::Array(_)         => VarType::Array,
            Self::Dict(_)          => VarType::Dict,
        }
    }

    /// Retrieves the underlying [`Operand`] instance, depending on the type of the value.
    pub fn as_operand_value(&self) -> &dyn Operand {
        match self {
            Self::Void(void_operand)       => void_operand,
            Self::Num(num_operand)         => num_operand,
            Self::Int(int_operand)         => int_operand,
            Self::Boolean(boolean_operand) => boolean_operand,
            Self::Str(str_operand)         => str_operand,
            Self::Func(func_operand)       => func_operand,
            Self::Array(array_operand)     => array_operand,
            Self::Dict(dict_operand)       => dict_operand,
        }
    }

    /// Retrieves the underlying [`Operand`] instance, depending on the type of the value.
    pub fn as_operand_value_mut(&mut self) -> &mut dyn Operand {
        match self {
            Self::Void(void_operand)       => void_operand,
            Self::Num(num_operand)         => num_operand,
            Self::Int(int_operand)         => int_operand,
            Self::Boolean(boolean_operand) => boolean_operand,
            Self::Str(str_operand)         => str_operand,
            Self::Func(func_operand)       => func_operand,
            Self::Array(array_operand)     => array_operand,
            Self::Dict(dict_operand)       => dict_operand,
        }
    }

    /// Checks equality between two values. If the values are numbers, uses approximative equality.
    pub fn equal(&self, rhs: &Value) -> bool {
        match (self, rhs) {
            (Self::Num(lhs), Self::Num(rhs)) => lhs.equal(rhs),
            _ => self == rhs,
        }
    }

    /// Tries to multiply two values. Only available for [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn mul(&self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        self.as_operand_value().mul(rhs, strategy)
    }

    /// Tries to divide two values. Only available for [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn div(&self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        self.as_operand_value().div(rhs, strategy)
    }

    /// Tries to perform the modulo (non-negative remainder) of two values. Only available for
    /// [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn modulo(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().modulo(rhs)
    }

    /// Tries to add two values. Available for [numbers] or [strings].
    ///
    /// [numbers]: crate::value::NumOperand
    /// [strings]: crate::value::StrOperand
    pub fn add(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().add(rhs)
    }

    /// Tries to subtract two values. Only available for [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn sub(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().sub(rhs)
    }

    /// Tries to raise a value to the power of another one. Only available for [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn pow(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().pow(rhs)
    }

    /// Tries to compare two values (equality).
    pub fn equ(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().equ(rhs)
    }

    /// Tries to compare two values (inequality).
    pub fn nequ(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().nequ(rhs)
    }

    /// Tries to compare two values (lower than).
    pub fn cmp_lt(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().cmp_lt(rhs)
    }

    /// Tries to compare two values (greater than).
    pub fn cmp_gt(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().cmp_gt(rhs)
    }

    /// Tries to compare two values (lower than or equal).
    pub fn cmp_lte(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().cmp_lte(rhs)
    }

    /// Tries to compare two values (greater than or equal).
    pub fn cmp_gte(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().cmp_gte(rhs)
    }

    /// Tries to convert a number to the given unit. Only available for [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn convert_to_unit(&self, unit: &Unit) -> OperationResult {
        self.as_operand_value().convert_to_unit(unit)
    }

    /// Raw-converts a number to the given unit. Only available for [numbers].
    ///
    /// [numbers]: crate::value::NumOperand
    pub fn raw_convert_to_unit(&self, unit: &Unit) -> OperationResult {
        self.as_operand_value().raw_convert_to_unit(unit)
    }

    /// Tries to perform the `&&` operation.
    pub fn and(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().and(rhs)
    }

    /// Tries to perform the `||` operation.
    pub fn or(&self, rhs: &Value) -> OperationResult {
        self.as_operand_value().or(rhs)
    }

    /// Tries to perform the unary `!` operation.
    pub fn not(&self) -> OperationResult {
        self.as_operand_value().not()
    }

    /// Tries to perform the postfix `++` operation.
    pub fn postfix_incr(&mut self) -> OperationResult {
        self.as_operand_value_mut().postfix_incr()
    }

    /// Tries to perform the postfix `--` operation.
    pub fn postfix_decr(&mut self) -> OperationResult {
        self.as_operand_value_mut().postfix_decr()
    }

    /// Tries to perform the prefix `++` operation.
    pub fn prefix_incr(&mut self) -> OperationResult {
        self.as_operand_value_mut().prefix_incr()
    }

    /// Tries to perform the prefix `--` operation.
    pub fn prefix_decr(&mut self) -> OperationResult {
        self.as_operand_value_mut().prefix_decr()
    }

    /// Tries to perform the `*=` operation.
    pub fn set_mul(&mut self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        self.as_operand_value_mut().set_mul(rhs, strategy)
    }

    /// Tries to perform the `/=` operation.
    pub fn set_div(&mut self, rhs: &Value, strategy: ValueMergeStrategy) -> OperationResult {
        self.as_operand_value_mut().set_div(rhs, strategy)
    }

    /// Tries to perform the `+=` operation.
    pub fn set_add(&mut self, rhs: &Value) -> OperationResult {
        self.as_operand_value_mut().set_add(rhs)
    }

    /// Tries to perform the `-=` operation.
    pub fn set_sub(&mut self, rhs: &Value) -> OperationResult {
        self.as_operand_value_mut().set_sub(rhs)
    }

    /// Tries to index the value. Available for [`ArrayOperand`] (arrays).
    ///
    /// [`ArrayOperand`]: crate::value::ArrayOperand
    pub fn index(&self, index: &Value) -> OperationResult {
        self.as_operand_value().index(index)
    }

    /// Tries to iterate over the values of the operand.
    pub fn iterate(&self) -> Result<ValueIterator, OperationError> {
        self.as_operand_value().iterate()
    }
}

/// A trait which can be implemented to convert any data into a [`Value`] instance.
///
/// [`Value`]: crate::value::Value
pub trait IntoValue {
    fn into_value(self) -> Value;
}

/// Describes the strategy to use when two numbers are multiplied or divided.
#[derive(Debug, Copy, Clone)]
pub enum ValueMergeStrategy {
    /// Append the new base units at the end, even if they already exist in the first unit.
    ///
    /// E.g: `1 m.s * 1 s` will become `1 m.s.s`.
    Append,
    /// Merge common base units together.
    ///
    /// E.g: `1 m.s * 1 s` will become `1 m.s2`, but `1 m.s * 1 km` will not merge meters and
    /// kilometers, then it will become `1 m.s.km`.
    MergeUnits,
    /// (Default)
    ///
    /// Merge common quantity together, eventually by converting the second value if
    /// needed.
    ///
    /// E.g: `1 m.s * 1 km` will merge meters and kilometers, then it will become `1000 m2.s`.
    MergeQuantities,
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_operand_value().to_string())
    }
}

impl Format for Value {
    fn format(&self, config: &FormatConfig, as_expr: bool) -> String {
        self.as_operand_value().format(config, as_expr)
    }

    fn format_indented(&self, config: &FormatConfig, as_expr: bool, ident: usize) -> String {
        self.as_operand_value().format_indented(config, as_expr, ident)
    }
}

impl fmt::Debug for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.as_operand_value())
    }
}

impl<T> From<T> for Value
where
    T: IntoValue,
{
    fn from(t: T) -> Value {
        t.into_value()
    }
}

#[cfg(test)]
mod tests {
    use crate::util::test_common::*;
    use maplit::hashmap;

    #[test]
    fn value_type() {
        with_test_container!(container, {
            let n = number!(container, 15 km);
            let cq = CompoundQuantity::from(hashmap!{
                s!("length") => float_parsed!(1),
            });
            assert!(n.has_type(&VarType::Num(Some(cq))));
            let cq = CompoundQuantity::from(hashmap!{
                s!("length") => float_parsed!(2),
            });
            assert!(!n.has_type(&VarType::Num(Some(cq))));
            assert!(!n.has_type(&VarType::Str));

            let s = string!("hi");
            assert!(!s.has_type(&VarType::Num(None)));
            assert!(s.has_type(&VarType::Str));
        })
    }
}

//! Handles the mechanism which allows to import other lola modules via the RT function `import()`.
//!
//! The [`Container`] contains a [`DefaultImporter`] which can be overloaded by a custom one.
//!
//! [`Container`]: crate::context::Container
//!
//! # Example of importer overload
//!
//!
//! ```ignore
//! //! To make this example compile, the feature "import_function" must be activated.
//!
//! use std::path::PathBuf;
//!
//! use lola::{
//!     context::{Container, ContextRef, DefaultImporter, Importer, Scope},
//!     library::prefill_context,
//!     parse::{ParsedInputSource, parse_expr},
//! };
//!
//! /// An importer which provides two pseudo-modules "foo" and "bar", in addition to the modules
//! /// provided by the default importer.
//! #[derive(Debug)]
//! struct MyImporter {
//!     default_importer: DefaultImporter,
//! }
//!
//! impl MyImporter {
//!     fn new() -> Self {
//!         Self { default_importer: DefaultImporter::new() }
//!     }
//! }
//!
//! impl Importer for MyImporter {
//!     fn get_code(&self, path: &str, caller_source: Option<ParsedInputSource>)
//!         -> Result<(String, Option<PathBuf>), String>
//!     {
//!         match path {
//!             // Pseudo-module "foo", containing the definition of the function "my_foo()":
//!             "foo" => Ok((r#"fn my_foo() { print("FOO!") }"#.to_string(), None)),
//!             // Pseudo-module "bar", containing the definition of the function "my_bar()":
//!             "bar" => Ok((r#"fn my_bar() { print("BAR!") }"#.to_string(), None)),
//!             // If the path is not handled, delegates to the default importer which will look into
//!             // the file system:
//!             _ => self.default_importer.get_code(path, caller_source)
//!         }
//!     }
//! }
//!
//! fn main() {
//!     // Create a new context, by giving the custom importer:
//!     let my_importer = MyImporter::new();
//!     let mut container = Container::new(Container::default_writer(), my_importer);
//!     let mut scope = Scope::new();
//!     let mut cxt = ContextRef::new(&mut container, &mut scope);
//!     prefill_context(&mut cxt);
//!
//!     // Now you can import the module "foo" or the module "bar":
//!     let code = r#"
//!         import("foo");
//!         my_foo();
//!     "#;
//!
//!     let expr = parse_expr(code, cxt.container, None).unwrap();
//!     expr.eval(&mut cxt).unwrap();
//! }
//! ```

use std::{fs, env};
use std::fmt::Debug;
use std::path::{PathBuf, Path, Component};

use directories::ProjectDirs;

use crate::parse::ParsedInputSource;

/// An trait that is able to resolve the location of a module, in a way or another, and return the
/// code to import.
///
/// See the [module] page for a full example of how to use it.
///
/// [module]: crate::context::importer
pub trait Importer: Debug {
    fn get_code(&self, path: &str, caller_source: Option<ParsedInputSource>) -> Result<(String, Option<PathBuf>), String>;
}

/// Default importer: resolve imports from the filesystem.
///
/// If the path starts with `./`, then search the module into the directory where the caller stays
/// (directory of the source file if the import is in a file, or current directory for inline
/// commands); if the path does not starts with `./`, then search the module in each of the
/// configured import paths.
///
/// By security, if a caller asks for a path containing `..`, then an error will occur.
#[derive(Debug)]
pub struct DefaultImporter {
    import_paths: Vec<PathBuf>,
}

impl DefaultImporter {
    /// Creates a `DefaultImporter` with a default single import path (on Linux, this directory
    /// will be `$HOME/.local/share/lola`).
    pub fn new() -> Self {
        let mut import_paths = vec![];

        if let Some(project_dirs) = ProjectDirs::from("", "", "lola") {
            import_paths.push(project_dirs.data_dir().to_owned());
        }

        Self {
            import_paths,
        }
    }

    /// Adds an import path.
    pub fn add_import_path(&mut self, path: PathBuf) {
        self.import_paths.push(path);
    }

    /// Finds the whole path to a module.
    fn find_module(&self, path: &str, caller_source: Option<ParsedInputSource>) -> Result<Option<PathBuf>, String> {
        let filename = Path::new(path);
        if filename.components().any(|component| matches!(component, Component::ParentDir)) {
            return Err("\"..\" is forbidden in module paths".to_string());
        }

        let filename = PathBuf::from(format!("{}.lola", path));

        let module = if let Ok(filename) = filename.strip_prefix("./") {
            self.find_relative_module(filename, caller_source)
        } else {
            self.find_library_module(filename)
        };

        Ok(module)
    }

    /// Finds a relative module.
    fn find_relative_module(&self, filename: &Path, caller_source: Option<ParsedInputSource>) -> Option<PathBuf> {
        caller_source
            .and_then(|caller_source| match caller_source {
                ParsedInputSource::Module(_, fullpath) => {
                    fullpath.and_then(|fullpath| fullpath.parent().map(|p| p.to_owned()))
                },
                ParsedInputSource::Inline(_) => {
                    env::current_dir().ok()
                }
            })
            .map(|base_dir| [ base_dir.as_path(), filename ].iter().collect())
    }

    /// Finds a module in the import paths.
    fn find_library_module(&self, filename: PathBuf) -> Option<PathBuf> {
        self.import_paths.iter()
            .filter_map(|import_path| Some((
                import_path.canonicalize().ok()?,
                [ import_path, &filename ].iter().collect::<PathBuf>().canonicalize().ok()?,
            )))
            .filter(|(import_path, file)| file.starts_with(import_path))
            .map(|(_, file)| file)
            .next()
    }
}

impl Default for DefaultImporter {
    fn default() -> Self {
        Self::new()
    }
}

impl Importer for DefaultImporter {
    fn get_code(&self, path: &str, caller_source: Option<ParsedInputSource>) -> Result<(String, Option<PathBuf>), String> {
        match self.find_module(path, caller_source)? {
            Some(file) => {
                let code = fs::read_to_string(&file)
                    .map_err(|_| format!("Unable to read the module \"{}\"", path))?;
                Ok((code, Some(file)))
            },
            None => {
                Err(format!("Unable to find the module '{}'", path))
            }
        }
    }
}

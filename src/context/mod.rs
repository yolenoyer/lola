//! Defines the structures that keep track of the context of a lola session:
//!
//!  * [`Container`] holds the definitions of the units, quantities, as well as the output buffer
//!    used for example by the runtime `print()` function;
//!  * [`Scope`] holds all the variables, function, etc... that have been created, including the
//!    native functions;
//!  * [`ContextRef`] can be used to hold a pair of references to the two above strutures.

pub mod scope;
pub mod container;
pub mod importer;

#[doc(inline)]
pub use scope::Scope;
#[doc(inline)]
pub use container::Container;
#[doc(inline)]
pub use importer::{
    Importer,
    DefaultImporter,
};

/// Simplifies argument passing by grouping together references to a container and a scope.
///
/// # Creating a pre-filled context (many RT functions and units provided)
///
/// ```
/// # use lola::{
/// #     context::{Container, ContextRef, Scope},
/// #     library::create_prefilled_context,
/// # };
/// #
/// let (mut container, mut scope) = create_prefilled_context();
/// let mut cxt = ContextRef::new(&mut container, &mut scope);
/// ```
///
/// # Creating an empty context
///
/// ```
/// # use lola::context::{Container, ContextRef, Scope};
/// #
/// let mut container = Container::new(
///     Container::default_writer(),
///     Container::default_importer()
/// );
/// let mut scope = Scope::new();
/// let mut cxt = ContextRef::new(&mut container, &mut scope);
/// // By calling `prefill_context()`, you get the same result as the example above:
/// // prefill_context(&mut cxt);
/// ```
#[derive(Debug)]
pub struct ContextRef<'a> {
    pub container: &'a mut Container,
    pub scope: &'a mut Scope,
}

impl<'a> ContextRef<'a> {
    /// Creates a new [`ContextRef`] instance.
    pub fn new(container: &'a mut Container, scope: &'a mut Scope) -> Self {
        Self {
            container,
            scope,
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn find_base_unit() {
        with_test_container!(container, {
            assert!(container.strong_base_unit("m").is_some());
            assert!(container.strong_base_unit("foo").is_none());
        })
    }

    #[test]
    fn find_quantity() {
        with_test_container!(container, {
            assert!(container.strong_quantity("length").is_some());
            assert!(container.strong_quantity("foo").is_none());
        })
    }

    #[test]
    fn create_unit() {
        with_test_container!(container, {
            let u = container.create_unit(&[ ("km", float_parsed!(1)), ("s", float_parsed!(2)) ]);
            assert!(u.is_ok());
        })
    }

    #[test]
    fn create_unit_with_bad_base_unit() {
        with_test_container!(container, {
            let u = container.create_unit(&[ ("km", float_parsed!(1)), ("foo", float_parsed!(2)) ]);
            assert!(u.is_err());
        })
    }

    #[test]
    fn derived_unit_weight() {
        with_test_container!(container, {
            let newton = container.strong_base_unit("N").unwrap();
            let weight = newton.weight();
            assert_eq!(weight, &(1000.0 as f64));
        })
    }

    #[test]
    fn global_var_accessible_in_local_scope() {
        with_test_context!(cxt, {
            eval!(cxt, "let v = 10");
            let r = eval!(cxt, "{v * 2}");
            assert_eq!(r, int!(20));
        })
    }

    #[test]
    fn local_var_not_accessible_in_global_scope() {
        with_test_context!(cxt, {
            let err = eval_err!(cxt, "{let v = 10}; v * 2");
            assert_eq!(err, err!(Expr:UndefinedVariable(s!("v"))));
        })
    }

    #[test]
    fn local_var_accessible_in_current_scope() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "7 + {let v = 10; v * 2}");
            assert_eq!(r, int!(27));
        })
    }

    #[test]
    fn local_var_accessible_in_sub_scope() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "7 + {let v = 10; {let w = v * 2; w + 10}}");
            assert_eq!(r, int!(37));
        })
    }

    #[test]
    fn global_scope_and_parentheses() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "(let a = 5); a");
            assert_eq!(r, int!(5));
            let r = eval!(cxt, "(let b = 5); b");
            assert_eq!(r, int!(5));
        })
    }

    #[test]
    fn global_scope_and_curly_brackets() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "let a = 5; { a + 2 }");
            assert_eq!(r, int!(7));
        })
    }

    #[test]
    fn scope_with_let_keyword() {
        with_test_context!(cxt, {
            let r = eval!(cxt, "let a = 5; { let a = 10 }; a");
            assert_eq!(r, int!(5));
            let r = eval!(cxt, "let b = 5; { b = 10 }; b");
            assert_eq!(r, int!(10));
        })
    }

    #[test]
    fn variable_already_exists() {
        with_test_context!(cxt, {
            eval!(cxt, "let a = 5");
            let r = eval_err!(cxt, "let a = 8");
            assert_eq!(r, err!(Expr:VarAlreadyExists(s!("a"))));
            let r = eval!(cxt, "{ let a = 8 }");
            assert_eq!(r, int!(8));
        })
    }

    #[test]
    #[ignore]
    fn test_iter_vars_starting_with() {
        with_test_scope!(scope, {
            scope.define_var(s!("foo1"), v_true!()).unwrap();
            scope.define_var(s!("foo2"), v_true!()).unwrap();
            scope.define_var(s!("bar1"), v_true!()).unwrap();
            scope.push();
            scope.define_var(s!("foo1"), v_true!()).unwrap();
            scope.define_var(s!("foo3"), v_true!()).unwrap();
            let mut varnames = scope.vars_starting_with("foo");
            varnames.sort();
            assert_eq!(varnames, vec![s!("foo1"), s!("foo1"), s!("foo2"), s!("foo3")]);
        })
    }
}
